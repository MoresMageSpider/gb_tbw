<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
Route::get('/home', function () {
    return redirect('/');
}); 
//Route::get('appointments', 'API\AppointmentsController@index');
/*Route::get('/', function () {
    return view('home');
});*/
Route::get('/', 'HomeController@index')->name('home'); 

Route::post('/tinymce/upload', 'TinymceController@store')->name('tinymce_upload'); 

//Contact 
Route::post('/contacts', 'ContactsController@store')->name('contacts_post'); 
Auth::routes(['register' => false, 'reset' => false]);
Route::post('/news/{id}', 'NewsController@get')->name('get_news'); 
//Login
//Route::get('/signin', 'AuthController@index'); 
Route::post('signin', 'AuthController@postSignin')->name('signin'); 
Route::post('password/send', 'AuthController@password')->name('forget_post'); 
//Signup 
Route::post('signup', 'AuthController@postSignup')->name('signup'); 
Route::post('/ecpay/response', 'ECPayController@ecpay_response')->name('page.success'); 
Route::group([
    'namespace' => 'Account', 'middleware' => ['fronts']
], function () { 
	Route::get('/reservations', 'ReservationsController@index')->name('page.reservations'); 
	Route::post('/reservations', 'ReservationsController@store')->name('reservationsPost');  	 	
}); 		
Route::group([
    'namespace' => 'Account'
], function () { 
	Route::get('/invoice/{reservation_number}', 'ReservationsController@invoice')->name('invoice'); 	
	Route::get('/success/{reservation_number}', 'ReservationsController@success')->name('page.success');
	Route::post('/success/{reservation_number}', 'ReservationsController@success')->name('page.success');
}); 			
Route::group([
    'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'admin', 'auth:admin', 'roles'], 'roles' => ['guide', 'customer_service', 'admin']
], function () { 	
	Route::resource('appointments', 'AppointmentsController');
	Route::DELETE('/reservations/{id}', 'AppointmentsController@cancel')->name('reservations_cancel'); 
	Route::get('appointments-export', 'AppointmentsController@export')->name('appointments_export')->where('name', '[A-Za-z]+');
});

Route::group([
    'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'admin', 'auth:admin', 'roles'], 'roles' => ['customer_service', 'admin']
], function () { 	
	Route::resource('members', 'MembersController');
	Route::resource('contacts', 'ContactsController');	
	Route::resource('question-types', 'QuestionTypesController');
});

Route::group([
    'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web', 'admin', 'auth:admin', 'roles'], 'roles' => 'admin'
], function () { 
	//Default
	Route::get('/dashboard', 'AdminController@index')->name('admin'); 
	Route::resource('roles', 'RolesController');
	Route::resource('permissions', 'PermissionsController');	
	Route::resource('users', 'UsersController');
	Route::post('/password/reset/{id}', 'UsersController@resetPassword');
	
	//Change Password
	Route::resource('change-password', 'ChangePasswordController');
	Route::get('/change-password','ChangePasswordController@showChangePassword');
	Route::post('/change-password','ChangePasswordController@changePassword')->name('changePassword');
	//Home
	Route::get('/home', 'HomeController@index')->name('admin_home'); 
	//Settings
	Route::resource('settings', 'SettingsController');
	Route::get('entrance/settings', 'SettingsController@entrance')->name('entrance_settings'); 
	Route::post('entrance/settings', 'SettingsController@entrance_post');	
	Route::get('general/settings', 'SettingsController@general')->name('general_settings'); 
	Route::post('general/settings', 'SettingsController@general_post');	
	//Page
	Route::resource('pages', 'PagesController');
	Route::get('/page/{id}/edit', 'PagesController@page_edit')->name('page_edit'); 
	Route::get('/home/{id}/edit', 'PagesController@home_edit')->name('home_edit'); 

	Route::delete('users/bulk/delete', 'UsersController@destroyBulk')->name('users_bulk_delete');	
	Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

	//Product
	Route::resource('products', 'ProductsController');
	Route::post('/products/reposition', 'ProductsController@reposition')->name('products_reposition'); 
	//News
	Route::resource('news', 'NewsController');
	Route::post('/news/reposition', 'NewsController@reposition')->name('news_reposition'); 
	//Beers
	Route::resource('beers', 'BeersController');
	Route::post('/beers/reposition', 'BeersController@reposition')->name('beers_reposition'); 
	
	Route::resource('fresh-beers', 'FreshBeersController');
	Route::post('/freshbeers/reposition', 'FreshBeersController@reposition')->name('freshbeers_reposition'); 

	/*Route::resource('appointments', 'AppointmentsController');
	Route::DELETE('/reservations/{id}', 'AppointmentsController@cancel')->name('reservations_cancel'); 
	Route::get('appointments-export', 'AppointmentsController@export')->name('appointments_export')->where('name', '[A-Za-z]+');*/

	Route::resource('translations', 'TranslationsController');
	Route::get('/translations/json/generate', 'TranslationsController@json_generate')->name('json_generate'); 

	Route::resource('phone-verification-code', 'PhoneVerificationCodeController');	
	
	Route::get('calendar', 'CalendarController@index')->name('appointments_calendar');
	//Route::get('timeslot/{date}', 'ManageAppointmentsController@edit')->name('appointments_date');
	Route::get('periods/{month}', 'CalendarPeriodsController@month')->name('timeslot_month'); 
	Route::POST('periods', 'CalendarPeriodsController@store');
	Route::POST('periods/month/{previous_month_id}', 'CalendarPeriodsController@store_previous_month');
	Route::PATCH('periods/{id}', 'CalendarPeriodsController@update');
}); 	
Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');  
  //Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  //Route::post('/register', 'AdminAuth\RegisterController@register');

  //Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  //Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  //Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  //Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
