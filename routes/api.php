<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('admin/appointments', 'API\AppointmentsController@index_admin')->name('appointments_admin_api');
Route::get('appointments', 'API\AppointmentsController@index')->name('appointments_api');
Route::get('appointments/form/{date}', 'API\AppointmentsController@appointment_form')->name('appointments_form');
Route::post('/otp/send', 'API\VerificationController@otp')->name('verification_code_send'); 
Route::post('periods_form', 'API\AppointmentsController@periods_form'); 
Route::post('periods/update', 'API\AppointmentsController@periods_update'); 
Route::post('periods_date', 'API\AppointmentsController@periods_date'); 
//Route::post('/ecpay/response', 'API\AppointmentsController@ecpay_response')->name('ecpay_response'); 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
