<?php

namespace App\Providers;
use File;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {        
        //
        Schema::defaultStringLength(191);
        $guide_menus = [];
        if (File::exists(base_path('resources/laravel-admin/menus_guide.json'))) {
            $guide_menus = json_decode(File::get(base_path('resources/laravel-admin/menus_guide.json')));
            view()->share('laravelGuideMenus', $guide_menus);
        }
		if (File::exists(base_path('resources/laravel-admin/menus_guide.json'))) {
            $service_menus = json_decode(File::get(base_path('resources/laravel-admin/menus_service.json')));
            view()->share('laravelServiceMenus', $service_menus);
        }
    }
}
