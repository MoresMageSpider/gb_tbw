<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\MorphMany;  
class CalendarDates extends Model
{
    use LogsActivity;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar_dates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'max_appointments'];

    protected $appends = ['class', 'booked'];

    public function getBookedAttribute(){
        if(isset($this->calendar_dates_id)){
            $calendar_dates_id = $this->calendar_dates_id;
        }else{
            $calendar_dates_id = $this->id;
        }
        return CalendarDatePeriods::find($calendar_dates_id)->appointments()->sum('total_people');
    }
    public function getClassAttribute(){ 
        if(isset($this->calendar_dates_id)){
            $calendar_dates_id = $this->calendar_dates_id;
        }else{
            $calendar_dates_id = $this->id;
        }
        $period_date = CalendarDates::find($calendar_dates_id);
        $booked = $period_date->appointments()->sum('total_people');
        $total_date_period = CalendarDatePeriods::where('calendar_dates_id', $calendar_dates_id)->where('status', 1)->count(); 
        $total = $period_date->max_appointments * $total_date_period;
        $class = 'disable';
        if($booked >= $total){
            $class = 'full';
        }elseif($booked <= $total && $period_date->date >= \Carbon\Carbon::today()->addDays(2)){
            $class = 'available';
        }
        return $class;
    }
    /**
     * Get the calendar_date_periods for the blog post.
     */
    public function periods()
    {
        return $this->hasMany('App\CalendarDatePeriods');
    }

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    /**
     * The customer may have many bookings.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function appointments(): MorphMany
    {
        return $this->morphMany('App\Appointment', 'bookable');
    }
}
