<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphTo;
class Appointment extends Model
{
    use LogsActivity;
    use SoftDeletes;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_type', 'user_id', 'bookable_id', 'bookable_type', 'period_type', 'period_id', 'name', 'phone', 'email', 'appointment_date', 'appointment_time', 'reservation_number', 'group_name', 'adult', 'handicap', 'underage', 'total_people', 'appointment_note', 'payment_amount', 'payment_status', 'service_notes', 'cancelled_at', 'payment_at'];

    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function getPdf($type = 'stream')
    {
        $pdf = app('dompdf.wrapper')->loadView('invoice-pdf', ['appointment' => $this]);
        $pdf_name = $this->reservation_number.'.pdf';
        if ($type == 'stream') {
            return $pdf->stream($pdf_name);
        }

        if ($type == 'download') {
            return $pdf->download($pdf_name);
        }
    }

    /**
     * Get the Appointment Calendar Date.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function bookable(): MorphTo
    {
        return $this->morphTo('App\CalendarDates', 'bookable_type', 'bookable_id');
    } 

    /**
     * Get the booking customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function period(): MorphTo
    {
        return $this->morphTo('App\CalendarDatePeriods', 'period_type', 'period_id');
    }
    /**
     * Get the user that owns the reservation.
     */
    public function user(): MorphTo
    {
        return $this->morphTo('App\User', 'user_type', 'user_id'); 
    }

    public static function getReservationNumber($date){
        $today_appointments = Appointment::withTrashed()->whereDate('appointment_date', $date)->count();
        $serial_number = $today_appointments + 1; 
        return 'GB'.date('Ymd').sprintf("%'03d", $serial_number);
    }
}
