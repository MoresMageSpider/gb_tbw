<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\MorphMany;  
use App\CalendarDates;
use App\CalendarDatePeriods;
class CalendarDatePeriods extends Model
{
    use LogsActivity;
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar_date_periods';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['calendar_dates_id', 'period', 'status', 'sort_order'];

    protected $appends = ['booked', 'available', 'is_future'];

    public function getBookedAttribute(){
        return CalendarDatePeriods::find($this->id)->appointments()->whereNull('cancelled_at')->sum('total_people');
    }  
    public function getAvailableAttribute(){        
        $period = CalendarDatePeriods::find($this->id);
        $booked = CalendarDatePeriods::find($this->id)->appointments()->whereNull('cancelled_at')->sum('total_people');
        $max_appointments = CalendarDates::find($period->calendar_dates_id)->max_appointments;
        $available = $max_appointments - $booked;
        if($available < 0){
            $available = 0;
        }
        return $available;
    }
    public function getIsFutureAttribute(){
        $period = CalendarDatePeriods::find($this->id);
        $calendar_date = CalendarDates::find($period->calendar_dates_id);
        $date_time = $calendar_date->date.' '.$period->period;
        if($date_time >= \Carbon\Carbon::now()){
            return true;    
        }
        return false;
    }
    /**
     * Get the post that owns the comment.
     */
    public function date()
    {
        return $this->belongsTo('App\CalendarDates');
    }

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    /**
     * The customer may have many bookings.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function appointments(): MorphMany
    {
        return $this->morphMany('App\Appointment', 'period');
    }
    public static function getBooked($id){     
        return CalendarDatePeriods::find($id)->appointments()->sum('total_people');
    }
    public static function getPeriodClass($id, $calendar_dates_id){     
        $max_appointments = CalendarDates::find($calendar_dates_id)->max_appointments;
        $period = CalendarDatePeriods::find($id);
        $booked = CalendarDatePeriods::find($id)->appointments()->sum('total_people');
        $available = $max_appointments - $booked;
        $class = 'disableEvent';
        if($period->status == 0){
            $class = 'disableEvent';
        }elseif($available <= 0){
            $class = 'fullEvent';
        }elseif($booked > 0){
            $class = 'reservedEvent';
        }elseif($period->status == 1){
            $class = 'openEvent';
        }
        return $class;
    }    
}
