<?php

declare(strict_types=1);

namespace App;

use Spatie\Sluggable\SlugOptions;
use Rinvex\Support\Traits\HasSlug;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Rinvex\Cacheable\CacheableEloquent;
use Rinvex\Support\Traits\HasTranslations;
use Rinvex\Support\Traits\ValidatingTrait;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Rinvex\Pages\Models\Page.
 *
 * @property int                 $id
 * @property string              $uri
 * @property string              $slug
 * @property string              $route
 * @property string              $domain
 * @property string              $middleware
 * @property array               $title
 * @property array               $subtitle
 * @property array               $excerpt
 * @property array               $content
 * @property string              $view
 * @property bool                $is_active
 * @property int                 $sort_order
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page ordered($direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereMiddleware($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rinvex\Pages\Models\Page whereView($value)
 * @mixin \Eloquent
 */
class Page extends Model implements Sortable
{
    use HasSlug;
    use SortableTrait;
    use HasTranslations;
    use ValidatingTrait;
    use CacheableEloquent;
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'parent_id',
        'uri',
        'slug',
        'title',
        'route',
        'subtitle',
        'domain',
        'middleware',
        'excerpt',
        'content',
        'image',
        'view',
        'is_active',
        'sort_order',
        'include_in_navigation',
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'parent_id' => 'string',
        'uri' => 'string',
        'slug' => 'string',
        'route' => 'string',
        'domain' => 'string',
        'middleware' => 'string',
        'view' => 'string',
        'is_active' => 'boolean',
        'sort_order' => 'integer',
        'deleted_at' => 'datetime',
    ];

    protected $appends = ['parent_page', 'parent_page_route', 'more_route', 'previous_route', 'next_route'];

    public function getParentPageAttribute()
    {
        if($this->parent_id > 0){
            return Page::find($this->parent_id)->title;
        }
        return '';
    }
    public function getParentPageRouteAttribute()
    {
        if($this->parent_id > 0){
            return Page::find($this->parent_id)->route;
        }
        return '';
    }
    public function getMoreRouteAttribute()
    {
        if(is_null($this->parent_id) || $this->parent_id == ''){
            $child_page = Page::where('parent_id', $this->id)->orderBy('sort_order', 'ASC')->first();  
            if(!empty($child_page)){
                return $child_page->route;
            }
        }
        return '';
    }
    public function getPreviousRouteAttribute()
    { 
        if($this->id == 15){
            return 'home';
        }
        $query = Page::where('include_in_navigation', 1)->where('is_active', 1)->where('sort_order', '<', $this->sort_order);
        if($this->parent_id > 0){ 
            $query->where('parent_id', $this->parent_id);
        }elseif(is_null($this->parent_id)){ 
            $query->whereNull('parent_id');
        }
        $child_page = $query->orderBy('sort_order', 'DESC')->first();  
        if(!empty($child_page)){
            return $child_page->route;
        }else{
            $query = Page::where('include_in_navigation', 1)->where('is_active', 1);
            if($this->parent_id > 0){ 
                $query->where('parent_id', $this->parent_id);
            }elseif(is_null($this->parent_id)){ 
                $query->whereNull('parent_id');
            }
            $child_page = $query->orderBy('sort_order', 'DESC')->first();  
            if(!empty($child_page)){
                return $child_page->route;
            }
        }
        return '';
    }
    public function getNextRouteAttribute()
    {
        if($this->id == 20){
            return 'home';
        }
        $query = Page::where('include_in_navigation', 1)->where('is_active', 1)->where('sort_order', '>', $this->sort_order);
        if($this->parent_id > 0){ 
            $query->where('parent_id', $this->parent_id);
        }elseif(is_null($this->parent_id)){ 
            $query->whereNull('parent_id');
        }
        $child_page = $query->orderBy('sort_order', 'ASC')->first();  
        if(!empty($child_page)){
            return $child_page->route;
        }else{
            $query = Page::where('include_in_navigation', 1)->where('is_active', 1);
            if($this->parent_id > 0){ 
                $query->where('parent_id', $this->parent_id);
            }elseif(is_null($this->parent_id)){ 
                $query->whereNull('parent_id');
            }
            $child_page = $query->orderBy('sort_order', 'ASC')->first();  
            if(!empty($child_page)){
                return $child_page->route;
            }
        }
        return ''; 
    } 
    /**
     * {@inheritdoc}
     */
    protected $observables = [
        'validating',
        'validated',
    ];

    /**
     * {@inheritdoc}
     */
    public $translatable = [
        'title',
        'subtitle',
        'excerpt',
        'content',
    ];


    /**
     * {@inheritdoc}
     */
    public $sortable = [
        'order_column_name' => 'sort_order',
    ];

    /**
     * The default rules that the model will validate against.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Whether the model should throw a
     * ValidationException if it fails validation.
     *
     * @var bool
     */
    protected $throwValidationExceptions = true;

    public function parent()
    {
        return $this->belongsTo('App\Page', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Page', 'parent_id');
    }
    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('rinvex.pages.tables.pages'));
        $this->setRules([
            'uri' => 'required|regex:/^([0-9a-z\/_-]+)$/|max:150|unique:'.config('rinvex.pages.tables.pages').',uri,NULL,id,domain,'.($this->domain ?? 'null'),
            'slug' => 'required|alpha_dash|max:150|unique:'.config('rinvex.pages.tables.pages').',slug,NULL,id,domain,'.($this->domain ?? 'null'),
            'route' => 'required|regex:/^([0-9a-z\._-]+)$/|max:150|unique:'.config('rinvex.pages.tables.pages').',route,NULL,id,domain,'.($this->domain ?? 'null'),
            'domain' => 'nullable|string|max:150',
            'middleware' => 'nullable|string|max:150',
            'title' => 'required|string|max:150',
            'subtitle' => 'nullable|string|max:150',
            'excerpt' => 'nullable|string|max:10000',
            'content' => 'nullable|string|max:10000000',
            'view' => 'required|string|max:150',
            'is_active' => 'sometimes|boolean',
            'sort_order' => 'nullable|integer|max:10000000',
        ]);
    }

    /**
     * Get the options for generating the slug.
     *
     * @return \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
                          ->doNotGenerateSlugsOnUpdate()
                          ->generateSlugsFrom('title')
                          ->saveSlugsTo('slug');
    }

    /**
     * Activate the page.
     *
     * @return $this
     */
    public function activate()
    {
        $this->update(['is_active' => true]);

        return $this;
    }

    /**
     * Deactivate the page.
     *
     * @return $this
     */
    public function deactivate()
    {
        $this->update(['is_active' => false]);

        return $this;
    }
}
