<?php

namespace App\Exports;

  

use App\Appointment;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
class AppointmentExport implements FromCollection, WithHeadings
{    

        /**

    * @return \Illuminate\Support\Collection

    */

    public function collection()
    {
        $keyword = "";
        $date = "";
        $period = "";
        $query = Appointment::select('reservation_number', 'appointment_date', 'appointment_time', 'name', 'phone', 'total_people', 'adult', 'handicap', 'underage', 'payment_amount', 'cancelled_at', 'payment_at');
        if(isset($_GET['search']) && $_GET['search'] != ''){
            $keyword = $_GET['search'];
            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', "%$keyword%")
                    ->orWhere('phone', 'LIKE', "%$keyword%") 
                    ->orWhere('reservation_number', 'LIKE', "%$keyword%");
            });
        }
        if(isset($_GET['date']) && $_GET['date'] != ''){
            $date = $_GET['date'];
            $query->whereDate('appointment_date', '=', $date);
        }
        if(isset($_GET['period']) && $_GET['period'] != ''){
            $period = $_GET['period'];
            $query->whereTime('appointment_time', '=', $period);
        }
        $appointments =  $query->latest()->get();

        /*$appointments = Appointment::select('reservation_number', 'appointment_date', 'appointment_time', 'name', 'phone', 'total_people', 'adult', 'handicap', 'underage', 'payment_amount', 'cancelled_at', 'payment_at')->get();*/
        if(!empty($appointments)){
        	foreach ($appointments as $key => $appointment) {
        		$appointments[$key]->appointment_date = date('m/d/Y', strtotime($appointment->appointment_date));
        		$appointments[$key]->appointment_time = date('A h:i', strtotime($appointment->appointment_date));
        		$appointments[$key]->adult = $appointment->adult.'/'.$appointment->handicap.'/'.$appointment->underage;
        		$appointments[$key]->payment_amount = number_format($appointment->payment_amount);
        		
        		if(!is_null($appointment->cancelled_at)){
        			$appointments[$key]->payment_at = date('m/d/Y', strtotime($appointment->cancelled_at));
        		}elseif(!is_null($appointment->payment_at)){
        			$appointments[$key]->payment_at = date('m/d/Y', strtotime($appointment->payment_at));
        		}
                $appointments[$key]->cancelled_at = (!is_null($appointment->cancelled_at))?"已取消":"付款完成";        		
        		unset($appointments[$key]->handicap);
        		unset($appointments[$key]->underage);
        	} 
        }
        return $appointments;
    }

    public function headings(): array
    {
        return [
            '預約單號',
            '預約日期',
            '預約時段',
            '預約人姓名',
            '手機號碼',
            '預約總人數',
            '成人/身障/未成年',
            '付款金額',
            '預約狀態',
            '建立日期'
        ];
    }
}

