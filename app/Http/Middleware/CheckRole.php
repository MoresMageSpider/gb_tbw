<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */    
    public function handle($request, Closure $next)
    {         
        // Get the required roles from the route 
        $roles = $this->getRequiredRoleForRoute($request->route());   
        // Check if a role is required for the route, and
        // if so, ensure that the user has that role. 
        if(is_array($roles)){
            foreach ($roles as $role) {
                if ($request->user()->hasRole($role) || !$role) {
                    return $next($request);
                }
            }
        }else{
            if ($request->user()->hasRole($roles) || !$roles) {
                return $next($request);
            }    
        }        
        if (Auth::check() && Auth::guard('admin')->user()->hasRole('admin')) {
            // Do admin stuff here
            return redirect('/admin/dashboard');
        }elseif (Auth::check() && Auth::guard('admin')->user()->hasRole('guide')) {
            // Do admin stuff here
            return redirect('/admin/appointments');
        }elseif (Auth::check() && Auth::guard('admin')->user()->hasRole('customer_service')) {
            // Do admin stuff here
            return redirect('/admin/appointments');
        }else {
            // Do nothing
            return redirect('reservations');
        }
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource.',
            ],
        ], 401);
    }

    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();

        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
