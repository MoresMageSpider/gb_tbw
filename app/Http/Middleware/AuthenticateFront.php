<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthenticateFront
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if (Auth::check() && Auth::user()->hasRole('user')) {
            // Do admin stuff here
            return redirect('reservations');
        }elseif (Auth::check() && Auth::user()->hasRole('admin')) { 
            // Do admin stuff here
            return redirect('admin/dashboard');
        }else {             
            // Do nothing
            return redirect('login');
        }
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource.',
            ],
        ], 401);
    }

    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();

        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
