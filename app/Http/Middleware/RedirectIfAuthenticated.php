<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { 
        if ($request->is('admin/*') && Auth::guard('admin')->check()) {
            return redirect('/admin/dashboard');
        }elseif (Auth::guard($guard)->check()) {
            return redirect('/reservations');
        }

        return $next($request);
    }
}
