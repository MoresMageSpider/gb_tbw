<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class RedirectIfAdmin
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'admin')
	{     
	    if (Auth::guard('admin')->check() && Auth::guard('admin')->user()->hasRole('admin')) {	    	
	        return redirect('admin/dashboard');
	    }  
	    return $next($request);
	}
}