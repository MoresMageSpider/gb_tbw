<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
class CookieConsentMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (! config('cookie-consent.enabled')) {
            return $response;
        }

        if (! $response instanceof Response) {
            return $response;
        }

        if (! $this->containsBodyTag($response)) {
            return $response;
        }
        //return $response;
        return $this->addCookieConsentScriptToResponse($response);
    }

    protected function containsBodyTag(Response $response): bool
    {
        return $this->getLastClosingBodyTagPosition($response->getContent()) !== false;
    }

    /**
     * @param \Illuminate\Http\Response $response
     *
     * @return $this
     */
    protected function addCookieConsentScriptToResponse(Response $response)
    {    
        $route = \Route::currentRouteName();
        $cookieConsentConfig = config('cookie-consent');
        $alreadyConsentedWithCookies = \Cookie::has($cookieConsentConfig['cookie_name']);
        $request_path = request()->path();
        $admin_contains = Str::contains($request_path, 'admin');
        if($admin_contains != true && $route != 'home' && $alreadyConsentedWithCookies != 1){
            return redirect('/');            
        } else{
            $content = $response->getContent();

            $closingBodyTagPosition = $this->getLastClosingBodyTagPosition($content);

            $content = ''
                .substr($content, 0, $closingBodyTagPosition)
                .view('cookieConsent::index')->render()
                .substr($content, $closingBodyTagPosition);

            return $response->setContent($content);
        }     
    }

    protected function getLastClosingBodyTagPosition(string $content = '')
    {
        return strripos($content, '</body>');
    }
}
