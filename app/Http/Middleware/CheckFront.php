<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Str;

class CheckFront
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */    
    public function handle($request, Closure $next)
    {         
        // Get the required roles from the route  
        $routeName = \Request::route()->getName();
        if(!Auth::check() && $routeName == 'page.reservations'){
            return redirect('login');
        }
        return $next($request);
    }
}
