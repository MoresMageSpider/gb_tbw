<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Notifications\ContactUsEmail;
use App\Contact;
use App\User;

class ContactsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',            
            'email' => 'required',
            'contact_method' => 'required',
            'question_type' => 'required', 
            'message' => 'required', 
        ],
        [
            'contact_method.required' => __('請選擇希望聯絡方式')
        ]
    );
        $requestData = $request->all();
        $contact = Contact::create($requestData);  
        $admin_user = User::find(1);
        if(!empty($admin_user) && setting('contact_email') != '')
        {
            $contact_emails = explode(",", setting('contact_email'));
            if(!empty($contact_emails)){
                foreach ($contact_emails as $contact_email) {
                    $notification_email = trim($contact_email);
                    if (filter_var($notification_email, FILTER_VALIDATE_EMAIL)) {
                        $admin_user->email = $notification_email;
                        $admin_user->notify(new ContactUsEmail($contact));    
                    }                        
                }
            }                
        }       
        $msg = [
            'flash_message' => 'Email sent successfully!',
            'contact' => $contact
       ];
        return redirect()->back()->with($msg);
    }
}
