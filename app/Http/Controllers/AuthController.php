<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\PhoneVerificationCode; 
use App\Notifications\PasswordEmail;
use DB;
//use App\Rules\RecaptchaV3;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/reservations';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
    * Validate the user login request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return void
    */ 
    public function index()
    {
        $page = app('rinvex.pages.page')->where('uri', 'reservations')->first();

        return view('signin', compact('page'));
    }

    public function postSignin(Request $request)
    { 
        request()->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        $credentials = array();    
        $check_email = filter_var($request->username , FILTER_VALIDATE_EMAIL ); 
        $credentials['password'] = $request->password;    
        if($check_email === false){
            request()->validate([
                    'username' => 'required|digits:10'
                ],
                [
                    'username.digits' => __('手機格式錯誤， 請重新輸入')
                ]
            );
            $credentials['phone'] = $request->username;        
            $user = User::where('phone', $request->username)->first();
        }else{
            request()->validate([
                    'username' => 'required|email'
                ],
                [
                    'username.email' => __('格式錯誤， 請重新輸入')
                ]
            );
            $credentials['email'] = $request->username;        
            $user = User::where('email', $request->username)->first();
        }  
        if(empty($user)){
            return redirect('/login')->with('error', __('您尚未是會員，請先註冊'));    
        }elseif($user->status == 0){
            return redirect('/login')->with('error', __('您的帳號有誤，請洽詢客服'));    
        }
        //$credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) { 
            // Authentication passed...             
             return redirect('/reservations');
        }
        return redirect('/login')->with('error', __('密碼錯誤，請重新輸入'));
    }

    public function postSignup(Request $request)
    {  
        request()->validate([
            'name' => 'required|max:10',
            'gender' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|digits:10|unique:users',            
            'password' => 'required|string|min:6|max:12|confirmed|alpha_num',
            'verification_code' => 'required',
        ],
        [
            'gender.required' => __('請選擇性別'),
            'email.email' => __('Email 格式 有誤，請重新輸入'),
            'email.unique' => __('您已是網 路會員，請使用會員登入'),
            'phone.required' => __('請先輸入手機號碼'),            
            'phone.unique' => __('您已是網 路會員，請使用會員登入'),
            'phone.digits' => __('手機格式錯誤， 請重新輸入'),
            'password.min' => __('密碼限輸入英文或數字 6-12 位數'),
            'password.max' => __('密碼限輸入英文或數字 6-12 位數'),
            'password.alpha_num' => __('密碼限輸入英文或數字 6-12 位數'),
            'password.confirmed' => __('與設定密碼不符，請重新輸入')
        ]);       
        if(1 === preg_match('~[0-9]~', $request->password) && (1 === preg_match('~[A-Z]~', $request->password) || 1 === preg_match('~[a-z]~', $request->password))){
            
        }else{
            return redirect()->back()->withInput($request->input())->withErrors([__('密碼錯誤，請重新輸入')]); 
        }   
        if(PhoneVerificationCode::where('phone', $request->phone)->where('verification_code', $request->verification_code)->exists()){ 
            $requestData = $request->except('password');  
            $requestData['password'] = bcrypt($request->password);
            $requestData['status'] = 1;
            $user = User::create($requestData); 
            DB::table('phone_verification_codes')->where('phone', $user->phone)->delete(); 
            $credentials = array();    
            $credentials['password'] = $request->password; 
            $credentials['email'] = $request->email;        
            if (Auth::attempt($credentials)) { 
                // Authentication passed...   
                return redirect('reservations')->with('flash_message', __('你已註冊成功，可開始選擇預約日期')); 
            }
            
        }else{
            return redirect()->back()->withInput($request->input())->withErrors([__('請確認您
的驗證碼，或重新寄發')]); 
        }       
    }

    public function password(Request $request)
    {  
        request()->validate([
            'email' => 'required:email'
        ]);
        $user = User::where('email', $request->email)->first();
        if(empty($user)){
            return redirect()->back()->withInput($request->input())->withErrors([__('您尚未是會員，請先註冊')]);   
        }
        $password = User::randomString(6);
        //mt_rand(100000,999999);
        $user->password = Hash::make($password);
        $user->save();
        $user->password = $password;
        $user->notify(new PasswordEmail($user));    
        return redirect()->back()->with('flash_message', __('新密碼已寄至您的電子信箱，請確認'));
    }
}