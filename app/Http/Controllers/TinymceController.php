<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Notifications\ContactUsEmail;
use App\Contact;
use App\User;

class TinymceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    { 
        $imgpath = request()->file('file')->store('uploads', 'public');
        $image = url('storage/app/public/'.$imgpath);
        return json_encode(['location' => $image]);
    } 
}
