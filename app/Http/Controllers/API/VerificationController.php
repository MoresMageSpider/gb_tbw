<?php

namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PhoneVerificationCode; 
use App\SMS; 
use Illuminate\Http\Request;
use Validator;
use DB;
class VerificationController extends Controller
{
    /**
     * Send opt on user phone number.
     *
     * @return \Illuminate\View\View
     */
    public function otp(Request $request)
    {
        $statusCode = 200;  //Request Error Code
        $response = [ 'result' => (object) array(), 'status' => 0, 'message' => ''];
        try { 
            if($request->lang == 'en'){
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|min:10|numeric|unique:users'
                ],
                [
                    'phone.required' => ' Please enter your phone number',
                    'phone.unique' => 'You are already a  member, please Login with member'
                ]);     
            }else{
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|min:10|numeric|unique:users'
                ],
                [
                    'phone.required' => '請先輸入手機號碼',
                    'phone.unique' => '您已是網 路會員，請使用會員登入'
                ]);     
            }            
            if($validator->fails()) {  
                $response['message'] = $validator->errors()->first(); 
            }else{              
                $phone = $request->phone;
                $otp = mt_rand(1000,9999);
                $sms = SMS::sendOTP($phone, $otp);
                if($sms == 1){
                    $requestData = $request->except('_token');  
                    $requestData['verification_code'] = $otp;
                    DB::table('phone_verification_codes')->where('phone', $request->phone)->delete(); 
                    PhoneVerificationCode::create($requestData);
                    $response['status'] = 1;
                    if($request->lang == 'en'){
                        $response['message'] = __('Verification code has been sent to your phone');    
                    }else{
                        $response['message'] = __('已發送驗證碼至您的手機');
                    }                    
                    $response['result'] = [];                              
                }else{
                    $response['status'] = 0;
                    $response['message'] = $sms;
                    $response['result'] = [];                              
                }                
            }            
        } catch (\Exception $e) {
            $statusCode = 200;
            $response['message'] = $e->getMessage();
        } finally {
            return response()->json($response, $statusCode);
        } 
    }
}
