<?php

namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Appointment;
use App\CalendarDates;
use App\CalendarDatePeriods;
use Illuminate\Http\Request;
use App\Notifications\ReservationUserEmail;
use App\User;
use DB;
class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $start = $request->get('start'); 
        $end = $request->get('end'); 
        $appointments = CalendarDates::
                    select('calendar_date_periods.id', 'calendar_dates.id AS calendar_dates_id', 'calendar_dates.date AS start', 'calendar_date_periods.period', 'calendar_date_periods.status', 'calendar_dates.date as title')
                    ->leftJoin('calendar_date_periods', 'calendar_dates.id', '=', 'calendar_date_periods.calendar_dates_id')
                    ->whereBetween('calendar_dates.date',[$start,$end])
                    ->orderBy('calendar_dates.date', 'ASC')->get()->toArray();  
        if(!empty($appointments)){
            foreach ($appointments as $key => $appointment) {                
                //$appointments[$key]['class'] = 'available'; //available/full/disable 
                $appointments[$key]['description'] = "description for All Day Event";
            }
        }
        return response()->json($appointments);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index_admin(Request $request)
    {
        $start = $request->get('start'); 
        $end = $request->get('end'); 
        $appointments = CalendarDates::
                    select('calendar_date_periods.id', 'calendar_dates.id AS calendar_dates_id', 'calendar_dates.date AS start', 'calendar_dates.max_appointments', 'calendar_date_periods.period', 'calendar_date_periods.status')
                    ->leftJoin('calendar_date_periods', 'calendar_dates.id', '=', 'calendar_date_periods.calendar_dates_id')
                    ->whereBetween('calendar_dates.date',[$start,$end])
                    ->orderBy('calendar_dates.date', 'ASC')
                    ->orderBy('calendar_date_periods.sort_order', 'ASC')
                    ->get()->toArray();  
        if(!empty($appointments)){
            foreach ($appointments as $key => $appointment) {  
                $period_class = CalendarDatePeriods::getPeriodClass($appointment['id'], $appointment['calendar_dates_id']);
                $booked = CalendarDatePeriods::getBooked($appointment['id']);
                $appointments[$key]['title'] = "預約時段 ".date('A h:i', strtotime($appointment['period']));
                if($appointment['status'] == 1){
                    $appointments[$key]['description'] = "預約人數 ".$booked." / ".$appointment['max_appointments']." 人";    
                }else{
                    $appointments[$key]['description'] = "";
                }
                
                $appointments[$key]['className'] = ["event", $period_class];
            }
        }
        return response()->json($appointments);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function appointment_form(Request $request, $date)
    {  
        /*$period_date = CalendarDates::whereHas('periods', function ($query) {
                        $query->where('status', '=', 1)->orderBy('sort_order', 'ASC');
                    })->where('date',$date)->first();  */
        $locale = $request->get('locale');
        $period_date = CalendarDates::where('date',$date)->first();     
        $time = '';
        if(!empty($period_date)){
            foreach ($period_date->periods as $key => $period) {
                if($period->status == 1){
                    $time = date('A h:i', strtotime($period->period));
                    break;
                }
            }
            return response()->json([
                'success' => 1,
                'message' => '',
                'date' => date('Y/m/d', strtotime($date)),
                'time' => $time,
                'form' => view('reservations_form', compact('date', 'period_date', 'locale'))->render(),
            ]);
        }else{
            return response()->json([
                'success' => 0, 
                'message' => 'Something wrong!',
                'form' => '',
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function periods_form(Request $request)
    {   
        $period_date = CalendarDates::with('periods')->where('date',$request->date)->first(); 
        if(!empty($period_date)){
            return response()->json([
                'success' => 1, 
                'message' => '',
                'form' => view('admin.calendar.period_form', compact('period_date'))->render(),
            ]);
        }else{
            return response()->json([
                'success' => 0, 
                'message' => 'Something wrong!',
                'form' => view('admin.calendar.period_form', compact('period_date'))->render(),
            ]);
        }        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function periods_update(Request $request)
    {   
        if($request->has('status') && !empty($request->status)){
            foreach ($request->status as $id => $status) {
                $period = CalendarDatePeriods::find($id); 
                $period->status = $status;
                $period->save();
            }
        }        
        return response()->json([
            'success' => 1, 
            'message' => '更新成功'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ecpay_response(Request $request)
    {
        //mail('info.magespider@gmail.com', 'ECPay Responce', 'ECPay Responce');
        $user = User::find(4);
        $appointment = Appointment::find(112);
        $user->notify(new ReservationUserEmail($appointment));    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function periods_date(Request $request)
    { 
        $date = $request->date;
        $calendarDate = CalendarDates::whereDate('date', $date)->first();        
        if(!empty($calendarDate)){
            $Periods = CalendarDatePeriods::where('calendar_dates_id', $calendarDate->id)->where('status', 1)->get();    
            if(!empty($Periods)){
                $period_options = '<select id="sorter" name="period" data-role="sorter" class="sorter-options select"><option value="">選擇時段</option>';
                foreach ($Periods as $key => $Period) {
                    $Period->period;
                    $period_options .= '<option value="'.$Period->period.'">'.date('h:i A', strtotime($Period->period)).'</option>';
                }
                $period_options .= '</select>';
                return response()->json([
                    'status' => 1, 
                    'result' => $period_options,
                    'message' => ''
                ]);    
            }            
        }
        return response()->json([
                'status' => 0, 
                'result' => '',
                'message' => ''
            ]);        
    }
}
