<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ReservationUserEmail;
use App\Notifications\ReservationAdminEmail;
use App\Notifications\ECPayEmail;
use App\Appointment; 
use App\User; 
use App\Admin; 
use Ecpay;
use Auth;
class ECPayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ecpay_response(Request $request)
    {  
        $reservation_number = $_POST['MerchantTradeNo']; 
        //if($_POST['RtnCode'] == 1){            
            $appointment = Appointment::withTrashed()->where('reservation_number', $reservation_number)->first();
            if(!empty($appointment)){
                $appointment->payment_status = 1; 
                $appointment->payment_at = now();
                $appointment->deleted_at = NULL;
                $appointment->cancelled_at = NULL;
                $appointment->save();

                $user = User::find($appointment->user_id);
                $user->email = $appointment->email;
                $user->notify(new ReservationUserEmail($appointment));    
                $admin_user = Admin::find(2);
                if(!empty($admin_user) && setting('reservation_email') != '')
                {
                    $reservation_emails = explode(",", setting('reservation_email'));
                    if(!empty($reservation_emails)){
                        foreach ($reservation_emails as $reservation_email) {
                            $notification_email = trim($reservation_email);
                            if (filter_var($notification_email, FILTER_VALIDATE_EMAIL)) {
                                $admin_user->email = $notification_email;
                                $admin_user->notify(new ReservationAdminEmail($appointment));    
                            }                        
                        }
                    }                
                }
                //mail("info.magespider@gmail.com","ECPay Responce",$reservation_number);
                $user->notify(new ECPayEmail($reservation_number)); 
                return '1|OK';
            }        
        //}        
        return '';
    }
}