<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ReservationUserEmail;
use App\Notifications\ReservationAdminEmail;
use App\Notifications\ECPayEmail;
use App\Appointment;
use App\CalendarDates;
use App\CalendarDatePeriods;
use App\User;
use App\Admin;
use Ecpay;
use Auth;
class ReservationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {       
        $page = app('rinvex.pages.page')->where('uri', 'reservations')->first(); 
        return view($page->view, compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'period_id' => 'required',
            'name' => 'required',
            'phone' => 'required|max:10',
            'email' => 'required|string|max:255|email'
        ]); 
        $user = User::find(Auth::user()->id); 
        $adult_price = (int)setting('adult_price');
        $period = CalendarDatePeriods::find($request->period_id);
        $bookable = CalendarDates::find($request->bookable_id);
        $requestData = $request->except('period_id', 'bookable_id');   
        $TotalAmount = $request->adult * $adult_price;
        $TotalAmount = (int)$TotalAmount;
        $requestData['total_people'] = $request->adult + $request->handicap + $request->underage;
        $requestData['payment_amount'] = $TotalAmount;
        $requestData['appointment_date'] = $bookable->date; 
        $requestData['appointment_time'] = $period->period;                        
        if($TotalAmount > 0){
            $requestData['deleted_at'] = now();
        }else{
            $requestData['payment_status'] = 1; 
            $requestData['payment_at'] = now(); 
        }        
        /*if(Appointment::withTrashed()->orderBy('id', 'DESC')->exists()){
            $last_id = Appointment::withTrashed()->orderBy('id', 'DESC')->first()->id + 1;    
        }else{
            $last_id = 1;
        }*/        
        $requestData['reservation_number'] = Appointment::getReservationNumber($bookable->date);                

        $appointment = new Appointment;
        $result = $appointment->make($requestData)                            
                    ->bookable()->associate($bookable)
                    ->period()->associate($period)
                    ->user()->associate($user)
                    ->save(); 
        if($result == 1){
            $appointment = $user->appointments()->orderBy('id', 'DESC')->first();
            if($TotalAmount > 0){
                $appointment->delete();
            }
            //Send Email to User
            if($request->email != '') {
                $user->email = $request->email;
            }
            //$user->notify(new ReservationUserEmail($appointment));    
            //Send email to admin
            if($TotalAmount <= 0){
                $admin_user = Admin::find(2);
                if(!empty($admin_user) && setting('reservation_email') != '')
                {
                    $reservation_emails = explode(",", setting('reservation_email'));
                    if(!empty($reservation_emails)){
                        foreach ($reservation_emails as $reservation_email) {
                            $notification_email = trim($reservation_email);
                            if (filter_var($notification_email, FILTER_VALIDATE_EMAIL)) {
                                $admin_user->email = $notification_email;
                                $admin_user->notify(new ReservationAdminEmail($appointment));    
                            }                        
                        }
                    }                
                }
            }
            if($TotalAmount > 0){
                //Redirect to ECPay
                $reservation_number = $appointment->reservation_number;
                Ecpay::i()->Send['ReturnURL']         = url('ecpay/response');
                //Ecpay::i()->Send['ReturnURL']         = 'http://demo.magespider.com/GBB/responce.php';
                Ecpay::i()->Send['OrderResultURL']         = url('success/'.$reservation_number);
                Ecpay::i()->Send['MerchantTradeNo']   = $reservation_number;           //訂單編號
                Ecpay::i()->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');      //交易時間
                Ecpay::i()->Send['TotalAmount']       = $TotalAmount;                     //交易金額
                Ecpay::i()->Send['TradeDesc']         = $reservation_number;         //交易描述
                Ecpay::i()->Send['ChoosePayment']     = \ECPay_PaymentMethod::Credit;     //付款方式

                //訂單的商品資料
                array_push(Ecpay::i()->Send['Items'], array('Name' => "GBB Reservations", 'Price' => $adult_price,
                           'Currency' => "元", 'Quantity' => (int) $request->adult, 'URL' => ""));

                //Go to ECPay
                echo "緑界頁面導向中...";
                echo Ecpay::i()->CheckOutString(); die;
            }            
            return redirect('success/'.$appointment->reservation_number)->with(['appointment' => $appointment]);
        }else{
            return redirect()->back()->with('flash_message', 'Somthing wrong!');
        }
        
    }

    /**
     * Show the application success.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function success(Request $request, $reservation_number)
    {  
        //$user = Auth::user(); 
        $page = app('rinvex.pages.page')->where('uri', 'success')->first();
        $appointment = Appointment::withTrashed()->where('reservation_number', $reservation_number)->first();

        $request->session()->flush();
        Auth::logout();
        if(!empty($appointment)){
            return view($page->view, compact('page', 'appointment'));    
        }else{
            return redirect('reservations')->withErrors(['Invalid reservation number!']); 
        }        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function invoice($reservation_number)
    {     
        //$user = Auth::user(); 
        //$appointment = Appointment::where('user_id', $user->id)->where('reservation_number', $reservation_number)->first();
        $appointment = Appointment::withTrashed()->where('reservation_number', $reservation_number)->first();
        if(!empty($appointment)){
            return $appointment->getPdf('download'); 
            //return view('invoice-pdf', compact('appointment'));    
        }else{
            return redirect('reservations')->withErrors(['Invalid reservation number!']); 
        }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ecpay_response(Request $request, $reservation_number)
    {
        $user = User::find(4);
        $appointment = Appointment::find(112);
        $user->notify(new ECPayEmail($appointment)); 
        return 'ECPay';
    }
}