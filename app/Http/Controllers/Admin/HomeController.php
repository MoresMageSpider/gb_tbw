<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    { 
        $pages = app('rinvex.pages.page')::with(['children' => function ($q) {
                        $q->orderBy('sort_order');
                    }])->whereIn('id', [14, 15, 16, 17, 18, 19, 20])->paginate(100);
        return view('admin.home.index', compact('pages'));
    } 
}