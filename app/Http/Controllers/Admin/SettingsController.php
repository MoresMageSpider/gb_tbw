<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $settings = Setting::where('key', 'LIKE', "%$keyword%")
                ->orWhere('value', 'LIKE', "%$keyword%")
                ->orderBy('key')->paginate($perPage);
        } else {
            $settings = Setting::orderBy('key')->paginate($perPage);
        }

        return view('admin.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'key' => 'required|string|unique:settings',
                'value' => 'required'
            ]
        );

        $requestData = $request->all();

        Setting::create($requestData);

        return redirect('admin/settings')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $setting = Setting::findOrFail($id);

        return view('admin.settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        return view('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'key' => 'required|string|unique:settings,key,' . $id,
                'value' => 'required'
            ]
        );
        $requestData = $request->all();

        $setting = Setting::findOrFail($id);
        $setting->update($requestData);

        return redirect('admin/settings')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Setting::destroy($id);

        return redirect('admin/settings')->with('flash_message', '刪除成功');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function entrance()
    {
        return view('admin.settings.entrance.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function entrance_post(Request $request)
    {
        $requestData = $request->all();

        //Setting::create($requestData); 
        if(!empty($request->images)){ 
            foreach ($request->images as $key => $file) {
                $setting = Setting::where('key', $key)->first();
                if(!empty($setting)){
                      if($setting->value != '' && file_exists(base_path().'/storage/app/settings/'.$setting->value)){
                           unlink(base_path().'/storage/app/settings/'.$setting->value);
                       }
                      // $file = $image;
                       // Get File Name
                       $file->getClientOriginalName();
                       // Get File Extension
                       $fileExtension = $file->getClientOriginalExtension();
                       // Get File Real Path
                       $file->getRealPath();
                       // Get File Size
                       $file->getSize();
                       // Get File Mime Type
                       $file->getMimeType();
                       // Rename file name
                       $mime = $file->getMimeType();
                       $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$setting->id. "." . $fileExtension;
                       $destinationPath = base_path().'/storage/app/settings/';
                       $file->move($destinationPath, $fileName);
                       $setting->value = $fileName;
                       $setting->save(); 
                }
            }
        } 
        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function general()
    {
        return view('admin.settings.general.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function general_post(Request $request)
    {
        $requestData = $request->all();

        //Setting::create($requestData); 
        if(!empty($request->settings)){ 
            foreach ($request->settings as $key => $value) {
                $setting = Setting::where('key', $key)->first();
                if(!empty($setting)){ 
                    $setting->value = $value;
                    $setting->save(); 
                }
            }
        } 
        return redirect()->back()->with('flash_message', '更新成功');
    }
}
