<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CalendarDatePeriods;
use App\CalendarDates;
use App\Appointment;
use Illuminate\Http\Request;
use App\Exports\AppointmentExport;
use Maatwebsite\Excel\Facades\Excel;
class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $date = $request->get('date');
        $period = $request->get('period');
        $search_period = $request->get('period');
        $perPage = 25;
        $periods = '';
        if (!empty($keyword) || !empty($date) || !empty($period)) {
            $query = Appointment::latest();
            if(!empty($keyword)){
                $query->where(function ($q) use ($keyword) {
                        $q->where('name', 'LIKE', "%$keyword%")
                            ->orWhere('phone', 'LIKE', "%$keyword%") 
                            ->orWhere('reservation_number', 'LIKE', "%$keyword%");
                });
            }
            if(!empty($period)){ 
                $query->whereTime('appointment_time', '=', $period);
            }
            if(!empty($date)){
                $query->whereDate('appointment_date', '=', $date);
                $calendarDate = CalendarDates::whereDate('date', $date)->first();        
                $periods = CalendarDatePeriods::where('calendar_dates_id', $calendarDate->id)->where('status', 1)->get();    
            }
            $appointments =  $query->paginate($perPage);                           
        } else {
            $appointments = Appointment::latest()->paginate($perPage);
        }
        if(\Auth::guard('admin')->user()->hasRole('guide')){
            return view('admin.appointments.guide.index', compact('appointments', 'periods', 'search_period', 'keyword', 'date'));
        }else{
            return view('admin.appointments.index', compact('appointments', 'periods', 'search_period', 'keyword', 'date'));
        }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        Appointment::create($requestData);

        return redirect('admin/appointments')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $appointment = Appointment::findOrFail($id);

        return view('admin.appointments.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $appointment = Appointment::findOrFail($id);

        return view('admin.appointments.edit', compact('appointment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'service_notes' => 'required'
		]);
        $requestData = $request->all();
        
        $appointment = Appointment::findOrFail($id);
        $appointment->update($requestData);

        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Appointment::destroy($id);

        return redirect('admin/appointments')->with('flash_message', '刪除成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cancel($id)
    {
        $appointment = Appointment::find($id);
        $appointment->cancelled_at = now();
        $appointment->payment_status = -1; 
        $appointment->save();
        return redirect()->back()->with('flash_message', 'Appointment cancelled!');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export(Request $request) 
    { 
        $filaname = 'appointments_'.date('YmdHis').'.xlsx';
        return Excel::download(new AppointmentExport, $filaname);
    }
}
