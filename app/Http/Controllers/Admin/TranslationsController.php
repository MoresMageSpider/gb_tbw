<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Translation;
use Illuminate\Http\Request;

class TranslationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $translations = Translation::where('chinese', 'LIKE', "%$keyword%")
                ->orWhere('english', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $translations = Translation::latest()->paginate($perPage);
        }

        return view('admin.translations.index', compact('translations'));
    }

    public function json_generate(Request $request)
    {
        $translations = Translation::where('type', 0)->pluck('english', 'chinese')->toArray();
        // Read File
        $jsonString = file_get_contents(base_path('resources/lang/admin_en.json'));
        // Write File
        $newJsonString = json_encode($translations, JSON_PRETTY_PRINT);

        file_put_contents(base_path('resources/lang/en.json'), $newJsonString);
        return redirect('admin/translations')->with('flash_message', 'json generated!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.translations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'chinese' => 'required',
			'english' => 'required'
		]);
        $requestData = $request->all();
        
        Translation::create($requestData);

        return redirect('admin/translations')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $translation = Translation::findOrFail($id);

        return view('admin.translations.show', compact('translation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $translation = Translation::findOrFail($id);

        return view('admin.translations.edit', compact('translation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'chinese' => 'required',
			'english' => 'required'
		]);
        $requestData = $request->all();
        
        $translation = Translation::findOrFail($id);
        $translation->update($requestData);

        return redirect('admin/translations')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Translation::destroy($id);

        return redirect('admin/translations')->with('flash_message', '刪除成功');
    }
}
