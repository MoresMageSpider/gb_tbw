<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\FreshBeer;
use Illuminate\Http\Request;

class FreshBeersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    { 
        $keyword = $request->get('search');
        $perPage = 100; 
        if (!empty($keyword)) {
            $freshbeers = app('rinvex.pages.page')::with(['children' => function ($q) {
                    $q->orderBy('sort_order');
                }])->where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orderBy('sort_order', 'ASC')->where('parent_id', 14)->paginate($perPage);
        } else {
            $freshbeers = app('rinvex.pages.page')::with(['children' => function ($q) {
                        $q->orderBy('sort_order');
                    }])->orderBy('sort_order', 'ASC')->where('parent_id', 14)->paginate($perPage);
        } 
        return view('admin.fresh-beers.index', compact('freshbeers')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.fresh-beers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',             
            'slug' => 'required', 
            'image' => 'image|mimes:jpeg,png,jpg|required|max:20000', 
        ],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過2mb，請符合上傳圖片規範'
			    ]); 
        $last_sort_order = Page::where('parent_id', 14)->orderBy('sort_order', 'DESC')->first()->sort_order; 
        $sort_order = $last_sort_order + 1;
        $slug = $request->slug;
        $requestData = $request->all();
        $requestData['uri'] = $slug;
        $requestData['view'] = 'fresh-beer';
        $requestData['parent_id'] = 14;
        $requestData['include_in_navigation'] = 1;
        $requestData['sort_order'] = $sort_order;
        $requestData['route'] = 'frontend.pages.'.$slug;  
        $page = app('rinvex.pages.page')::create($requestData);
        if(!empty($request->file('image'))){  
           $file = $request->file('image');
           // Get File Name
           $file->getClientOriginalName();
           // Get File Extension
           $fileExtension = $file->getClientOriginalExtension();
           // Get File Real Path
           $file->getRealPath();
           // Get File Size
           $file->getSize();
           // Get File Mime Type
           $file->getMimeType();
           // Rename file name
           $mime = $file->getMimeType();
           $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$page->id. "." . $fileExtension;
           $destinationPath = base_path().'/storage/app/pages/';
           $file->move($destinationPath, $fileName);
           $page->image = $fileName;
           $page->save();        
        } 
        return redirect('admin/fresh-beers')->with('flash_message', '新增成功');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $freshbeer = FreshBeer::findOrFail($id);

        return view('admin.fresh-beers.show', compact('freshbeer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $freshbeer = FreshBeer::findOrFail($id);

        return view('admin.fresh-beers.edit', compact('freshbeer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if($request->has('display_beers') && $request->display_beers == 1){
            $this->validate($request, [
                'title' => 'required', 
                'image' => 'image|mimes:jpeg,png,jpg|max:2000'
            ],
			[
				'image.max' => '圖片不得超過2mb，請符合上傳圖片規範'
			]);
        }else{
            $this->validate($request, [
                'title' => 'required',
                'content' => 'required', 
                'image' => 'image|mimes:jpeg,png,jpg|max:2000'
            ],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過2mb，請符合上傳圖片規範'
			    ]);
        }        
        $requestData = $request->except('image');
        
        $freshbeer = FreshBeer::findOrFail($id);
        $freshbeer->update($requestData);
        if(!empty($request->file('image'))){   
            if($freshbeer->image != '' && file_exists(base_path().'/storage/app/freshbeers/'.$freshbeer->image)){  
                 unlink(base_path().'/storage/app/freshbeers/'.$freshbeer->image);
             }
             $file = $request->file('image');
             // Get File Name
             $file->getClientOriginalName();
             // Get File Extension
             $fileExtension = $file->getClientOriginalExtension();
             // Get File Real Path
             $file->getRealPath();
             // Get File Size
             $file->getSize();
             // Get File Mime Type
             $file->getMimeType();
             // Rename file name
             $mime = $file->getMimeType();
             $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$freshbeer->id. "." . $fileExtension;
             $destinationPath = base_path().'/storage/app/freshbeers/';
             $file->move($destinationPath, $fileName);
             $freshbeer->image = $fileName;
             $freshbeer->save();        
        }
        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        FreshBeer::destroy($id);

        return redirect('admin/fresh-beers')->with('flash_message', '刪除成功');
    }

    /**
     * Reposition the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reposition(Request $request)
    {
        $freshbeers = Page::where('parent_id', 14)->get(); 
        if(!empty($freshbeers))
        {
          foreach ($freshbeers as $freshbeer) {
            foreach ($request->order as $order) {
                if ($order['id'] == $freshbeer->id) {
                    $freshbeer->update(['sort_order' => $order['position']]);
                }
            }
          } 
          return response()->json(array('success' => true));
        }
        else
        {
          return response()->json(array('success' => false)); 
        } 
    }
}