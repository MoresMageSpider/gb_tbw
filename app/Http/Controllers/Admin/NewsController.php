<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 100; 
        if (!empty($keyword)) {
            $news = News::where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                //->orWhere('published_at', 'LIKE', "%$keyword%")
                ->orderBy('sort_order', 'ASC')->paginate($perPage);
        } else {
            $news = News::orderBy('sort_order', 'ASC')->paginate($perPage);
        }

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
    			'title' => 'required',
    			'content' => 'required',
    			'image' => 'image|mimes:jpeg,png,jpg|required|max:1000',
          'is_published' => 'required',
    			'published_at' => 'required'
    		],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過1mb，請符合上傳圖片規範'
			    ]);
        News::increment('sort_order');
        $news = new News;
        if($request->is_published == 1){
          $requestData = $request->except('image', 'published_at');        
          $requestData['published_at'] = now();
        }else{
          $requestData = $request->except('image');          
        }
        $requestData['sort_order'] = 1;
        $news = $news->create($requestData);
        if(!empty($request->file('image'))){   
               $file = $request->file('image');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$news->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/news/';
               $file->move($destinationPath, $fileName);
               $news->image = $fileName;
               $news->save();        
        }
        return redirect('admin/news')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
    			'title' => 'required',
    			'content' => 'required',
    			//'published_at' => 'required',
          'image' => 'image|mimes:jpeg,png,jpg|max:1000',
    		],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過1mb，請符合上傳圖片規範'
			    ]);
        if($request->is_published == 1){          
          $current_time = \Carbon\Carbon::now();
          if($request->published_at >= $current_time->toDateTimeString()){
            $requestData = $request->except('image', 'published_at');        
            $requestData['published_at'] = now();
          }else{
            $requestData = $request->except('image');
          }          
        }else{
          $requestData = $request->except('image');
        }        
        $news = News::findOrFail($id);
        $news->update($requestData);
        if(!empty($request->file('image'))){  
            if($news->image != '' && file_exists(base_path().'/storage/app/news/'.$news->image)){
                   unlink(base_path().'/storage/app/news/'.$news->image);
               }
               $file = $request->file('image');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$news->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/news/';
               $file->move($destinationPath, $fileName);
               $news->image = $fileName;
               $news->save();        
        }
        return redirect('admin/news')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        News::destroy($id);

        return redirect('admin/news')->with('flash_message', '刪除成功');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reposition(Request $request)
    {
        $news = News::all(); 
        if(!empty($news))
        {
          foreach ($news as $new) {
            foreach ($request->order as $order) {
                if ($order['id'] == $new->id) {
                    $new->update(['sort_order' => $order['position']]);
                }
            }
          } 
          return response()->json(array('success' => true));
        }
        else
        {
          return response()->json(array('success' => false)); 
        } 
    }
}
