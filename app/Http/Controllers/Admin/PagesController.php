<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {
            $pages = app('rinvex.pages.page')::with(['children' => function ($q) {
                    $q->orderBy('sort_order');
                }])->where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orderBy('sort_order', 'ASC')->whereNull('parent_id')->paginate($perPage);
        } else {
            $pages = app('rinvex.pages.page')::with(['children' => function ($q) {
                        $q->orderBy('sort_order');
                    }])->orderBy('sort_order', 'ASC')->whereNull('parent_id')->paginate($perPage);
        }

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    { 
        $this->validate($request, [
			'title' => 'required',
			'content' => 'required',             
            'slug' => 'required', 
            'image' => 'image|mimes:jpeg,png,jpg|required|max:20000', 
		],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過2mb，請符合上傳圖片規範'
			    ]); 
        
        $slug = $request->slug;
        $requestData = $request->all();
        $requestData['uri'] = $slug;
        $requestData['route'] = 'frontend.pages.'.$slug;  
        $page = app('rinvex.pages.page')::create($requestData);
        if(!empty($request->file('image'))){  
		   $file = $request->file('image');
		   // Get File Name
		   $file->getClientOriginalName();
		   // Get File Extension
		   $fileExtension = $file->getClientOriginalExtension();
		   // Get File Real Path
		   $file->getRealPath();
		   // Get File Size
		   $file->getSize();
		   // Get File Mime Type
		   $file->getMimeType();
		   // Rename file name
		   $mime = $file->getMimeType();
		   $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$page->id. "." . $fileExtension;
		   $destinationPath = base_path().'/storage/app/pages/';
		   $file->move($destinationPath, $fileName);
		   $page->image = $fileName;
		   $page->save();        
        }
        return redirect('admin/pages')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $page = app('rinvex.pages.page')::findOrFail($id);

        return view('admin.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        //$page = Page::findOrFail($id);
        $page = app('rinvex.pages.page')::findOrFail($id);
        return view('admin.pages.edit', compact('page'));
    }


    /**
     * Show the form for editing the single page.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function page_edit($id)
    {
        //$page = Page::findOrFail($id);
        $page = app('rinvex.pages.page')::findOrFail($id);
        return view('admin.pages.page_edit', compact('page'));
    }

    /**
     * Show the form for editing the single page.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function home_edit($id)
    {
        //$page = Page::findOrFail($id);
        $page = app('rinvex.pages.page')::findOrFail($id);
        return view('admin.pages.home_edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ 
                'content' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg|max:2000' 
              ],
              [
                'content.required' => '請確實填寫必填欄位',
                'image.max' => '圖片不得超過2mb，請符合上傳圖片規範'
              ]
            );
        $requestData = $request->except('image');    
        $page = app('rinvex.pages.page')::findOrFail($id);
        $page->update($requestData);
        if(!empty($request->file('image'))){          
           if($page->image != '' && file_exists(base_path().'/storage/app/pages/'.$page->image)){
                   unlink(base_path().'/storage/app/pages/'.$page->image);
           }
           $file = $request->file('image');
           // Get File Name
           $file->getClientOriginalName();
           // Get File Extension
           $fileExtension = $file->getClientOriginalExtension();
           // Get File Real Path
           $file->getRealPath();
           // Get File Size
           $file->getSize();
           // Get File Mime Type
           $file->getMimeType();
           // Rename file name
           $mime = $file->getMimeType();
           $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$page->id. "." . $fileExtension;
           $destinationPath = base_path().'/storage/app/pages/';
           $file->move($destinationPath, $fileName);
           $page->image = $fileName;
           $page->save();        
        }
        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Page::destroy($id);

        return redirect()->back()->with('flash_message', '刪除成功');
    }
}
