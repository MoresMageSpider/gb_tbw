<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Beer;
use Illuminate\Http\Request;

class BeersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $beers = Beer::where('name', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $beers = Beer::orderBy('sort_order', 'ASC')->paginate($perPage);
        }

        return view('admin.beers.index', compact('beers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.beers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
    			'name' => 'required',
    			'content' => 'required',
    			'image' => 'image|mimes:jpeg,png,jpg|required|max:2000',
				'logo' => 'image|mimes:jpeg,png,jpg|required|max:1000'
    		],
            [
              'content.required' => '請確實填寫必填欄位',
			  'image.max' => '圖片不得超過2mb，請符合上傳圖片規範',
			  'logo.max' => '圖片不得超過1mb，請符合上傳圖片規範'
            ]
		);
        Beer::increment('sort_order');
        $beer = new Beer;
        $requestData = $request->except('image', 'logo');        
        $requestData['sort_order'] = 1;
        $beers = $beer->create($requestData); 
        if(!empty($request->file('image'))){   
               $file = $request->file('image');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$beers->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/beers/';
               $file->move($destinationPath, $fileName);
               $beers->image = $fileName;
               $beers->save();        
        }
        if(!empty($request->file('logo'))){   
               $file = $request->file('logo');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$beers->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/beers/logo/';
               $file->move($destinationPath, $fileName);
               $beers->logo = $fileName;
               $beers->save();        
        }
        return redirect('admin/beers')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $beer = Beer::findOrFail($id);

        return view('admin.beers.show', compact('beer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $beer = Beer::findOrFail($id);

        return view('admin.beers.edit', compact('beer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
    			'name' => 'required',
    			'content' => 'required',
				'image' => 'image|mimes:jpeg,png,jpg|max:2000',
				'logo' => 'image|mimes:jpeg,png,jpg|max:1000'
				],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過2mb，請符合上傳圖片規範',
					'logo.max' => '圖片不得超過1mb，請符合上傳圖片規範'
			    ]
			);
        $requestData = $request->except('image', 'logo');        
        $beers = Beer::findOrFail($id);
        $beers->update($requestData);
        if(!empty($request->file('image'))){  
            if($beers->image != '' && file_exists(base_path().'/storage/app/beers/'.$beers->image)){
                   unlink(base_path().'/storage/app/beers/'.$beers->image);
               }
               $file = $request->file('image');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$beers->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/beers/';
               $file->move($destinationPath, $fileName);
               $beers->image = $fileName;
               $beers->save();        
        }
        if(!empty($request->file('logo'))){  
            if($beers->logo != "" && file_exists(base_path().'/storage/app/beers/logo/'.$beers->logo)){
                   unlink(base_path().'/storage/app/beers/logo/'.$beers->logo);
               }
               $file = $request->file('logo');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$beers->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/beers/logo/';
               $file->move($destinationPath, $fileName);
               $beers->logo = $fileName;
               $beers->save();        
        }
        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Beer::destroy($id);

        return redirect('admin/beers')->with('flash_message', '刪除成功');
    }

    /**
     * Reposition the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reposition(Request $request)
    {
        $beers = Beer::all(); 
        if(!empty($beers))
        {
          foreach ($beers as $beer) {
            foreach ($request->order as $order) {
                if ($order['id'] == $beer->id) {
                    $beer->update(['sort_order' => $order['position']]);
                }
            }
          } 
          return response()->json(array('success' => true));
        }
        else
        {
          return response()->json(array('success' => false)); 
        } 
    }
}