<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CalendarMonths;
use App\CalendarDates;
use App\CalendarDatePeriods;
use App\Setting;
class CalendarPeriodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 2500;

        if (!empty($keyword)) {
            $manageappointments = CalendarPeriods::where('period', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $manageappointments = CalendarPeriods::latest()->paginate($perPage);
        }

        return view('admin.calendar-periods.index', compact('manageappointments'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.calendar-periods.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function month($date)
    {          
        $max_appointments = 30;
        $period_month = CalendarMonths::where('year_month', $date)->first();

        $prevmonth = date('Y-m', strtotime("-1 months", strtotime($date.'-01')));
        $period_previous_month = CalendarMonths::where('year_month', $prevmonth)->first();        
        if(!empty($period_month)){
            $max_appointments = $period_month->max_appointments;
            $default_periods = array(array('time' => $period_month->period_1, 'status' => $period_month->status_1), 
            array('time' => $period_month->period_2, 'status' => $period_month->status_2), 
            array('time' => $period_month->period_3, 'status' => $period_month->status_3), 
            array('time' => $period_month->period_4, 'status' => $period_month->status_4));  
        }else{
            $default_periods = array(array('time' => '10:30:00', 'status' => 0), 
            array('time' => '11:30:00', 'status' => 0), 
            array('time' => '14:00:00', 'status' => 0), 
            array('time' => '14:30:00', 'status' => 0));      
        }        
        return view('admin.calendar-periods.create', compact('date', 'default_periods', 'max_appointments', 'period_previous_month'));     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {  
        $this->validate($request, [
            'date' => 'required',
            'max_appointments' => 'required|min:1|max:99'
        ]);  
        $start_date = date('Y-m-01', strtotime($request->date));
        $end_date = date('Y-m-t', strtotime($request->date));

        $requestData = $request->except('period', 'status'); 

        //Create period month        
        $data_month = array();
        $data_month['date'] = $start_date;
        $data_month['year_month'] = date('Y-m', strtotime($request->date));
        $data_month['max_appointments'] = $request->max_appointments;
        foreach ($request->period as $key => $period) {
            $index = $key+1;
            $data_month['period_'.$index] = date('H:i:s', strtotime($period));
            $data_month['status_'.$index] = (isset($request->status[$key]))?$request->status[$key]:1; 
            
        } 
        $period_month = CalendarMonths::where('year_month', $request->date)->first();
        if(!empty($period_month)){
            $period_month->update($data_month);
        }else{
            $period_month = new CalendarMonths;
            $period_month->create($data_month);
        }
        while (strtotime($start_date) <= strtotime($end_date)) {            
            $requestData['date'] = $start_date; 
            $period_date = CalendarDates::where('date',$start_date)->first();
            if(!empty($period_date)){
                $period_date->update($requestData);
            }else{ 
                $period_date  = CalendarDates::create($requestData);
            }            
            $calendar_dates_id = $period_date->id;
            if($calendar_dates_id != '') 
            foreach ($request->period as $key => $period) {
                $data = array();
                $periods = CalendarDatePeriods::where('calendar_dates_id',$calendar_dates_id)->where('sort_order',$key)->first();
                $data['calendar_dates_id'] = $calendar_dates_id;
                $data['period'] = date('H:i:s', strtotime($period));
                $data['status'] = (isset($request->status[$key]))?$request->status[$key]:1; 
                $data['sort_order'] = $key; 
                if(!empty($periods)){
                    $periods->update($data);
                }else{
                    $periods = new CalendarDatePeriods;
                    $periods->create($data);
                }
            }
            $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
        }        
        return redirect('admin/calendar')->with('flash_message', '新增成功');
    }

    public function store_previous_month(Request $request, $previous_month_id)
    {   
        $previous_month = CalendarMonths::find($previous_month_id)->toArray();
        if(!empty($previous_month)){
            $start_date = date('Y-m-01', strtotime($request->date));
            $end_date = date('Y-m-t', strtotime($request->date));
            //Create period month        
            $data_month = array();
            $data_month['date'] = $start_date;
            $data_month['year_month'] = date('Y-m', strtotime($request->date));
            $data_month['max_appointments'] = $previous_month['max_appointments'];
            $max_appointments = $previous_month['max_appointments'];
            for($index = 1; $index <= 4; $index++) { 
                $previous_period = $previous_month['period_'.$index];
                $previous_status = $previous_month['status_'.$index];
                $data_month['period_'.$index] = date('H:i:s', strtotime($previous_period));
                $data_month['status_'.$index] = (isset($previous_status))?$previous_status:1;             
            } 
            $period_month = CalendarMonths::where('year_month', $request->date)->first();
            if(!empty($period_month)){
                $period_month->update($data_month);
            }else{
                $period_month = new CalendarMonths;
                $period_month->create($data_month);
            }
            while (strtotime($start_date) <= strtotime($end_date)) {            
                $requestData['date'] = $start_date; 
                $requestData['max_appointments'] = $max_appointments; 
                $period_date = CalendarDates::where('date',$start_date)->first();
                if(!empty($period_date)){
                    $period_date->update($requestData);
                }else{ 
                    $period_date  = CalendarDates::create($requestData);
                }            
                $calendar_dates_id = $period_date->id;
                if($calendar_dates_id != '')

                for($index = 1; $index <= 4; $index++) { 
                    $previous_period = $previous_month['period_'.$index];
                    $previous_status = $previous_month['status_'.$index];
                    $key = $index - 1;
                    $data = array();   
                    $data['calendar_dates_id'] = $calendar_dates_id;
                    $data['period'] = date('H:i:s', strtotime($previous_period));
                    $data['status'] = (isset($previous_status))?$previous_status:1;
                    $data['sort_order'] = $key;
                    $periods = CalendarDatePeriods::where('calendar_dates_id',$calendar_dates_id)->where('sort_order',$key)->first(); 
                    if(!empty($periods)){
                        $periods->update($data);
                    }else{
                        $periods = new CalendarDatePeriods;
                        $periods->create($data);
                    }        
                }  
                $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
            }
            //$request->date
            return redirect('admin/calendar')->with('flash_message', '新增成功');    
        }else{
            return redirect()->back()->withErrors(['Opps something wrong!']); 
        }        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        CalendarPeriods::destroy($id);

        return redirect()->back()->with('flash_message', 'Record successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($date)
    {
        $period_date = CalendarDates::with('periods')->where('date',$date)->first(); 
        if(!empty($period_date)){
            return view('admin.calendar-periods.edit', compact('period_date', 'date'));
        }else{ 
            $max_appointments = 30;
            $default_periods = array(array('time' => '10:30:00', 'status' => 0), 
                array('time' => '11:30:00', 'status' => 0), 
                array('time' => '14:00:00', 'status' => 0), 
                array('time' => '14:30:00', 'status' => 0)); 
            /*$year_month = date('Y-m', strtotime($date));
            $period_month = CalendarMonths::where('year_month', $year_month)->first(); 
            if(!empty($period_month)){
                $max_appointments = $period_month->max_appointments;
                $default_periods = array(array('time' => $period_month->period_1, 'status' => $period_month->status_1), 
                array('time' => $period_month->period_2, 'status' => $period_month->status_2), 
                array('time' => $period_month->period_3, 'status' => $period_month->status_3), 
                array('time' => $period_month->period_4, 'status' => $period_month->status_4), 
                array('time' => $period_month->period_5, 'status' => $period_month->status_5), 
                array('time' => $period_month->period_6, 'status' => $period_month->status_6));  
            }else{
                $default_periods = array(array('time' => '10:30:00', 'status' => 0), 
                array('time' => '11:30:00', 'status' => 0), 
                array('time' => '14:00:00', 'status' => 0), 
                array('time' => '14:30:00', 'status' => 0), 
                array('time' => '15:00:00', 'status' => 0), 
                array('time' => '15:30:00', 'status' => 0));      
            } */  
            return view('admin.calendar-periods.create', compact('date', 'default_periods', 'max_appointments'));
        }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    { 
        $this->validate($request, [
            'max_appointments' => 'required|min:1|max:99'
        ]); 
        $requestData = $request->except('period', 'status');   
        $period_date = CalendarDates::find($id);
        $period_date->update($requestData);
        $calendar_dates_id = $id;
        foreach ($request->period as $key => $period) {
            $data = array();
            $periods = CalendarDatePeriods::find($key);
            $data['calendar_dates_id'] = $calendar_dates_id;
            $data['period'] = date('H:i:s', strtotime($period));
            $data['status'] = (isset($request->status[$key]))?$request->status[$key]:1; 
            //$data['sort_order'] = $key; 
            $periods->update($data);
        }

        return redirect()->back()->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CalendarPeriods::destroy($id);

        return redirect()->back()->with('flash_message', '刪除成功');
    }
}
