<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use App\Appointment;
use Carbon\Carbon;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
    	$curent_month_users = User::whereYear('created_at', Carbon::now()->year)
			    	->whereMonth('created_at', Carbon::now()->month)->count();
		$users = User::count();

        $curent_month_admin_users = Admin::whereYear('created_at', Carbon::now()->year)
                    ->whereMonth('created_at', Carbon::now()->month)->count();
        $admin_users = Admin::count(); 

		$curent_month_reservations = Appointment::
					    	  whereYear('created_at', Carbon::now()->year)
					    	->whereMonth('created_at', Carbon::now()->month)->count();
		$reservations = Appointment::
					    	  whereYear('created_at', Carbon::now()->year)
					    	->whereMonth('created_at', Carbon::now()->month)->count();

    	$curent_month = array('users' =>  $curent_month_users, 'admin_users' =>  $curent_month_admin_users, 'reservations' =>  $curent_month_reservations); 
    	$total_count = array('users' =>  $users, 'admin_users' =>  $admin_users, 'reservations' =>  $reservations);
        return view('admin.dashboard',compact('curent_month','total_count'));
    }
}
