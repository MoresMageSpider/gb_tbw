<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 250;

        if (!empty($keyword)) {
            $products = Product::where('title', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $products = Product::orderBy('sort_order', 'ASC')->paginate($perPage);
        }

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
    			'title' => 'required',
    			'content' => 'required', 
          'image' => 'image|mimes:jpeg,png,jpg|required|max:10000',
    			'price' => 'required'
    		],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過1mb，請符合上傳圖片規範'
			    ]);

        Product::increment('sort_order');
        $product = new Product;
        $requestData = $request->except('image');        
        $requestData['sort_order'] = 1;
        $product = $product->create($requestData); 
        if(!empty($request->file('image'))){   
               $file = $request->file('image');
               // Get File Name
               $file->getClientOriginalName();
               // Get File Extension
               $fileExtension = $file->getClientOriginalExtension();
               // Get File Real Path
               $file->getRealPath();
               // Get File Size
               $file->getSize();
               // Get File Mime Type
               $file->getMimeType();
               // Rename file name
               $mime = $file->getMimeType();
               $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$product->id. "." . $fileExtension;
               $destinationPath = base_path().'/storage/app/products/';
               $file->move($destinationPath, $fileName);
               $product->image = $fileName;
               $product->save();        
        }
        return redirect('admin/products')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
    			'title' => 'required',
    			'content' => 'required',
    			'image' => 'image|mimes:jpeg,png,jpg|max:10000',
    			'price' => 'required'
    		],
   			    [
					'content.required' => '請確實填寫必填欄位',
					'image.max' => '圖片不得超過1mb，請符合上傳圖片規範'
			    ]);
        $requestData = $request->all();
        
        $product = Product::findOrFail($id);
        $product->update($requestData);
        if(!empty($request->file('image'))){  
           if($product->image != '' && file_exists(base_path().'/storage/app/products/'.$product->image)){
                   unlink(base_path().'/storage/app/products/'.$product->image);
           }
           $file = $request->file('image');
           // Get File Name
           $file->getClientOriginalName();
           // Get File Extension
           $fileExtension = $file->getClientOriginalExtension();
           // Get File Real Path
           $file->getRealPath();
           // Get File Size
           $file->getSize();
           // Get File Mime Type
           $file->getMimeType();
           // Rename file name
           $mime = $file->getMimeType();
           $fileName = md5(microtime() . $file->getClientOriginalName()).'-'.$product->id. "." . $fileExtension;
           $destinationPath = base_path().'/storage/app/products/';
           $file->move($destinationPath, $fileName);
           $product->image = $fileName;
           $product->save();        
        }
        return redirect('admin/products')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect('admin/products')->with('flash_message', '刪除成功');
    }

    /**
     * Reposition the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reposition(Request $request)
    {
        $products = Product::all(); 
        if(!empty($products))
        {
          foreach ($products as $product) {
            foreach ($request->order as $order) {
                if ($order['id'] == $product->id) {
                    $product->update(['sort_order' => $order['position']]);
                }
            }
          } 
          return response()->json(array('success' => true));
        }
        else
        {
          return response()->json(array('success' => false)); 
        } 
    }
}