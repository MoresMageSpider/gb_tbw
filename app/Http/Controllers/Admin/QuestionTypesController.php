<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\QuestionType;
use Illuminate\Http\Request;

class QuestionTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 2500;

        if (!empty($keyword)) {
            $questiontypes = QuestionType::where('title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $questiontypes = QuestionType::orderBy('id', 'ASC')->paginate($perPage);
        }

        return view('admin.question-types.index', compact('questiontypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.question-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'questions' => 'required'
		]);
        $questions = $request->questions;
        $ids = $request->ids; 
        foreach ($questions as $key => $question) {
            if(isset($ids[$key]) && $ids[$key] != 0){
                $questiontype = QuestionType::findOrFail($ids[$key]);
                $questiontype->update(['title' => $question]);
            }else{ 
                QuestionType::create(['title' => $question]); 
            }
        }
        return redirect()->back()->with('flash_message', 'Saved successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        QuestionType::destroy($id);

        return redirect()->back()->with('flash_message', 'Record successfully!');

        /*$questiontype = QuestionType::findOrFail($id);

        return view('admin.question-types.show', compact('questiontype'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $questiontype = QuestionType::findOrFail($id);

        return view('admin.question-types.edit', compact('questiontype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required'
		]);
        $requestData = $request->all();
        
        $questiontype = QuestionType::findOrFail($id);
        $questiontype->update($requestData);

        return redirect('admin/question-types')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        QuestionType::destroy($id);

        return redirect()->back()->with('flash_message', '刪除成功');
    }
}
