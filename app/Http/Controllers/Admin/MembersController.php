<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $users = User::latest()->paginate($perPage);
        }

        return view('admin.members.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        return view('admin.members.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|string|max:255|email|unique:users',
                'password' => 'required|string|min:6|max:12|confirmed|alpha_num', 
                'role' => 'required'
            ],
            [
                'email.email' => 'email已申請過，請重新確認',
                'password.required' => '請確實填寫必填欄位',
                'password.min' => '帳號限輸入英文或數字 6-12 位數',
                'password.max' => '帳號限輸入英文或數字 6-12 位數',
                'password.alpha_num' => '帳號限輸入英文或數字 6-12 位數',
                'password.confirmed' => '設定新密 碼與確認新密碼不符，請重新確認'                
            ]
        );

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        //$user->assignRole('user'); 
        return redirect('admin/members')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.members.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    { 
        $user = User::findOrFail($id);
        return view('admin.members.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {        
        $this->validate(
            $request,
            [
                'name' => 'required',
                'gender' => 'required',
                'email' => 'required|string|max:255|email|unique:users,email,' . $id,
                'phone' => 'required|digits:10|unique:users,phone,' . $id
            ],
            [
                'email.email' => 'email已申請過，請重新確認',
                'email.unique' => 'email已申請過，請重新確認',
                'phone.digits' => '格式錯誤， 請重新輸入'
            ]
        );

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);        
        //$user->roles()->detach(); 
        //$user->assignRole($request->role); 

        return redirect('admin/members')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/members')->with('flash_message', '刪除成功');
    }
}
