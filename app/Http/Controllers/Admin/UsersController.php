<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\Admin;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $users = Admin::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $users = Admin::latest()->paginate($perPage);
        }

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [   
                'name' => 'required',
                'username' => 'required|alpha_num|min:6|max:12|unique:admins',
                'email' => 'required|string|max:255|email|unique:admins', 
                'role' => 'required'
            ],
            [
                'username.alpha_num' => '帳號限輸入英文或數字 6-12 位數',
                'username.min' => '帳號限輸入英文或數字 6-12 位數',
                'username.max' => '帳號限輸入英文或數字 6-12 位數',
                'username.unique' => '帳號不可重複申請，請重新輸入',
                'email.email' => 'email已申請過，請重新確認',
                'email.unique' => 'email已申請過，請重新確認'
            ]
        );

        $data = $request->except('role');
        $password = 27981688;
        $data['password'] = bcrypt($password);
        $user = Admin::create($data); 
        $user->assignRole($request->role);  
        return redirect('admin/users')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = Admin::findOrFail($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');
        $user = Admin::findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }
        return view('admin.users.edit', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {        
        $this->validate(
            $request,
            [
                'name' => 'required',
                'username' => 'required|alpha_num|min:6|max:12|unique:admins,username,' . $id,
                'email' => 'required|string|max:255|email|unique:admins,email,' . $id,
                'role' => 'required'
            ],
            [
                'username.alpha_num' => '帳號限輸入英文或數字 6-12 位數',
                'username.min' => '帳號限輸入英文或數字 6-12 位數',
                'username.max' => '帳號限輸入英文或數字 6-12 位數',
                'username.unique' => '帳號不可重複申請，請重新輸入',
                'email.email' => 'email已申請過，請重新確認',
                'email.unique' => 'email已申請過，請重新確認'
            ]
        );

        $data = $request->except('role');
        $user = Admin::findOrFail($id);
        $user->update($data);        
        $user->roles()->detach(); 
        $user->assignRole($request->role); 

        return redirect('admin/users')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Admin::destroy($id);

        return redirect('admin/users')->with('flash_message', '刪除成功');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function resetPassword(Request $request, $id)
    {        
        $user = Admin::find($id);
        $password = 27981688;
        $user->password = bcrypt($password);
        $user->save();
        //$user->assignRole($request->role);  
        return redirect('admin/users')->with('flash_message', 'Password reset successfully!');
    }
}
