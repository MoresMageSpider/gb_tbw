<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PhoneVerificationCode;
use Illuminate\Http\Request;

class PhoneVerificationCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $phoneverificationcode = PhoneVerificationCode::where('phone', 'LIKE', "%$keyword%")
                ->orWhere('verification_code', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $phoneverificationcode = PhoneVerificationCode::latest()->paginate($perPage);
        }

        return view('admin.phone-verification-code.index', compact('phoneverificationcode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.phone-verification-code.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'phone' => 'required',
			'verification_code' => 'required'
		]);
        $requestData = $request->all();
        
        PhoneVerificationCode::create($requestData);

        return redirect('admin/phone-verification-code')->with('flash_message', '新增成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $phoneverificationcode = PhoneVerificationCode::findOrFail($id);

        return view('admin.phone-verification-code.show', compact('phoneverificationcode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $phoneverificationcode = PhoneVerificationCode::findOrFail($id);

        return view('admin.phone-verification-code.edit', compact('phoneverificationcode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'phone' => 'required',
			'verification_code' => 'required'
		]);
        $requestData = $request->all();
        
        $phoneverificationcode = PhoneVerificationCode::findOrFail($id);
        $phoneverificationcode->update($requestData);

        return redirect('admin/phone-verification-code')->with('flash_message', '更新成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PhoneVerificationCode::destroy($id);

        return redirect('admin/phone-verification-code')->with('flash_message', '刪除成功');
    }
}
