<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Admin;
use Illuminate\Http\Request;
use Auth;
use Hash;
class ChangePasswordController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function showChangePassword()
    { 
        return view('admin.changepassword.edit');
    }
    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::guard('admin')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("flash_message","請確認您目前 的密碼");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("flash_message","您的新密碼不能與目前密碼相同，請重新確認");
        }

        $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|max:12|confirmed|alpha_num',
            ],
            [
                'current-password.required' => '請確實填寫必填欄位',
                'new-password.required' => '請確實填寫必填欄位',
                'new-password.min' => '密碼限輸入英文或數字 6-12 位數',
                'new-password.max' => '密碼限輸入英文或數字 6-12 位數',
                'new-password.alpha_num' => '密碼限輸入英文或數字 6-12 位數',
                'new-password.confirmed' => '設定新密 碼與確認新密碼不符，請重新確認'                
            ]
    );

        //Change Password
        $user = Auth::guard('admin')->user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("flash_message","密碼修改成功");

    }
}
