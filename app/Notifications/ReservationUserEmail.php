<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReservationUserEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
         $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    { 
        $contact_us = route('page.customer-service');
        return (new MailMessage)
            ->from(setting('from_email'), setting('from_name'))
            //->to(setting('contact_email'))
            ->subject('預約訂單確認：歡迎您預約參觀【GB鮮釀識酒館】')
            ->line(new \Illuminate\Support\HtmlString('親愛的 '.$this->appointment->name.' 您好，您已成功預約【GB鮮釀識酒館】導覽活動，請您確認下方訂單資訊，如有任何問題，敬請向我們的<a href="'.$contact_us.'">客服中心</a>聯繫。謝謝！〈下方放訂單資訊〉'))
            ->line('預約單號: '.$this->appointment->reservation_number)
            ->line('預約日期: '.date('Y/m/d', strtotime($this->appointment->appointment_date)))
            ->line('預約時段: '.date('A h:i', strtotime($this->appointment->appointment_time)))
            ->line('聯 絡 人: '.$this->appointment->name)
            ->line('手機號碼: '.$this->appointment->phone)
            ->line('電子信箱: '.$this->appointment->email)
            ->line('成人票:'. $this->appointment->adult.' 位')
            ->line('身障人士優惠:'. $this->appointment->handicap.' 位')
            ->line('未成年票:'. $this->appointment->underage.' 位')
            ->line('預約總人數:'. $this->appointment->total_people.' 位')
            ->line('付款金額:'. '$ '.number_format($this->appointment->payment_amount));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
