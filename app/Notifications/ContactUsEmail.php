<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactUsEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contact_us)
    {
         $this->contact_us = $contact_us;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    { 
        return (new MailMessage)
            ->from($this->contact_us->email, $this->contact_us->name)
            //->to(setting('contact_email'))
            ->subject('客服專區 - '.$this->contact_us->name)
            ->line('姓名: '.$this->contact_us->name)
            ->line('手機號碼: '.$this->contact_us->phone)
            ->line('電子郵件: '.$this->contact_us->email)
            ->line('希望聯絡方式: '.$this->contact_us->contact_method)
            ->line('問題種類: '.$this->contact_us->question_type)
            ->line('問題內容:'. $this->contact_us->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
