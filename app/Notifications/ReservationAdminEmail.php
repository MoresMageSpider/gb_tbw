<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReservationAdminEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
         $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    { 
        return (new MailMessage)
            ->from($this->appointment->email, $this->appointment->name)
            //->to(setting('contact_email'))
            ->subject('New Reservation #'.$this->appointment->reservation_number)
            ->line('預約單號: '.$this->appointment->reservation_number)
            ->line('預約日期: '.date('Y/m/d', strtotime($this->appointment->appointment_date)))
            ->line('預約時段: '.date('A h:i', strtotime($this->appointment->appointment_time)))
            ->line('聯 絡 人: '.$this->appointment->name)
            ->line('手機號碼: '.$this->appointment->phone)
            ->line('電子信箱: '.$this->appointment->email)
            ->line('成人票:'. $this->appointment->adult.' 位')
            ->line('身障人士優惠:'. $this->appointment->handicap.' 位')
            ->line('未成年票:'. $this->appointment->underage.' 位')
            ->line('預約總人數:'. $this->appointment->total_people.' 位')
            ->line('付款金額:'. '$ '.number_format($this->appointment->payment_amount))
            ->line('公司 / 團體名稱: '.$this->appointment->group_name)
            ->line('備註: '.$this->appointment->appointment_note);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
