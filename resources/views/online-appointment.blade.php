@extends('layouts.app')

@section('content') 
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main">
  <div class="content-center">
  <div class="container overlay-content">
    <div class="row">
      <div class="col-md-6 text-left"> 
        <div class="main-content">   
            @if($page->parent_page != '')       
                <h2 class="page-title">{{$page->parent_page}}</h2>
            @endif
            <h1 class="display-1 sub-title">{{$page->title}}</h1>  
            <div class="content desc">
              <div class="scrollbar-inner">  
                <div class="description"> 
                    <?php echo $page->content;?>
                    <div class="">
                      @if (\Auth::check()) 
                        <p><a class="btn btn-outline-white btn-round" href="{{url('reservations')}}" target="_blank">{{__('立 即 預 約')}}</a></p>
                      @else
                        <p><a class="btn btn-outline-white btn-round" href="{{url('login')}}" target="_blank">{{__('立 即 預 約')}}</a></p>
                      @endif                      
                    </div>
                    <?php if($page->excerpt != ''){ echo $page->excerpt;}?>
              </div>
              </div>
            </div>
        </div>
        <div class="nav-slider d-flex justify-content-between mx-auto">
             @if (Route::has($page->previous_route))
              <a href="{{ route($page->previous_route) }}" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
            @endif
            @if (Route::has($page->next_route))
            <a href="{{ route($page->next_route) }}" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
            @endif
        </div>
    </div> 
    @if(isset($page) && $page->image != '')
    <div class="col-md-6">
      <div class="thumbnail_inside_2 thumbnail_zoom">
        <div class="overlay overlay-1 d-none d-md-block"></div>
        <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->title}}">  
      </div>
    </div>
    @endif
  </div> 
</div>
</div>
</div>
</main> 
@endsection