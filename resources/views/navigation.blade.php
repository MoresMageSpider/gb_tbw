@extends('layouts.app')

@section('content') 
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('public/images/cms/05-img-c-min.jpg')}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main">
  <div class="content-center">
    <div class="container overlay-content">
    <div class="row">
        <div class="col-md-12 text-left"> 
          <div class="main-content">          
              @if($page->parent_page != '')       
                  <h2 class="page-title">{{$page->parent_page}}</h2>
              @endif
              <h1 class="display-1 sub-title">{{$page->title}}</h1>  
              <div class="content desc">
                <div class="scrollbar-inner">  
                  <div class="description"> 
                      <img src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->parent_page}}">
                  </div>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-6 text-center"> 
              <div class="nav-slider d-flex justify-content-between mx-auto">
                @if (Route::has($page->previous_route))
                  <a href="{{ route($page->previous_route) }}" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
                @endif
                @if (Route::has($page->next_route))
                <a href="{{ route($page->next_route) }}" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
                @endif
              </div>
            </div>
          </div>
      </div>  
    </div>  
  </div>
</div>
</main> 
@endsection