@extends('layouts.app')
@section('content')  
  <div class="fresh-beer-page"> 
          <main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
            <div class="overlay overlay-3 d-none d-lg-block"></div>  
              <div class="main">
                <div class="content-center"><div class="container overlay-content">
                  <div class="row">
                    <div class="col-md-6 mb-4 mb-sm-0"> 
                      <div class="main-content">       
                          <h2 class="page-title">{{__('鮮釀啤酒')}}</h2> 
                          <h1 class="display-1 sub-title">{{$page->title}}</h1>  
                          @if($page->id == 33)
                            @include('include.beers') 
                          @else
                            <div class="content desc">
                              <div class="scrollbar-inner">  
                                <div class="description">
                                    <?php echo $page->content;?>
                                </div>
                              </div>
                            </div>
                          @endif
                      </div>
                      <div class="nav-slider d-flex justify-content-between mx-auto">
                          @if (Route::has($page->previous_route))
                            <a href="{{ route($page->previous_route) }}" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
                          @endif
                          @if (Route::has($page->next_route))
                          <a href="{{ route($page->next_route) }}" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
                          @endif
                      </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="thumbnail_inside_2 thumbnail_zoom">
                      <div class="overlay overlay-1 d-none d-md-block"></div>
                      <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->title}}">  
                    </div>
                  </div>
                </div> 
              </div>
            </div>
          </div></main>  
  </div>  
<div class="modal dark-modal beer-model fade" id="beerModal" tabindex="-1" role="dialog" aria-labelledby="beerModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-header pb-0">
        <h5 class="modal-title font-weight-midium" id="beerModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
          
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#beerModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var name = button.data('name');
    var image = button.data('image');
    var modal = $(this)
    if(image != ''){
        modal.find('.modal-body').html('<img class="beer-img" src="'+image+'" alt="'+name+'">');
    }    
    modal.find('.modal-title').text(name)
    //modal.find('.modal-body input').val(recipient)
  })
</script>
@endsection