@extends('layouts.app')

@section('content')
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{ url('public/images/slider/slider-img-1-min.jpg') }}');">
    <div class="overlay overlay-3 d-none d-lg-block"></div>
    <div class="slider-image overlay-content">
        <div class="container-fluid p-0">
            <div class="col-md-7 ml-auto">
              <div class="thumbnail_inside">
                <div class="overlay overlay-1 d-none d-md-block"></div>
                <img class="slider_Img" src="{{ url('public/images/slider/slider-img-1-min.jpg') }}">  
              </div>
            </div> 
        </div>
    </div>  
    <div class="content-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="jumbotron-slider"> 
              <div class="mb-5">
                <h2 class="page-title">鮮釀啤酒</h2>
              </div>  
              <div class="content mb-4 display-1">
                <p>純度、精確、完美！<br> 為每一位挑剔的口味製作一系列均衡的啤酒。</p>
              </div>
              <div class="jumbotron-bottom">
                <div class="button mb-5">
                  <a class="btn btn-outline-white btn-round" href="03.html">了 解 更 多</a>
                </div>
                <div class="mast-action d-flex justify-content-between">
                  <div class="title align-self-end">
                    <p class="f-light-14 m-0">Gordon Biersch<br> BEER！</p>
                  </div>
                  <div class="nav-slider d-flex justify-content-between mx-auto">
                    <a href="02_g.html" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
                    <a href="02_b.html" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
                  </div>  
                </div>
              </div>
          </div>
        </div> 
      </div>
      <div class="vertical-text"><h3>吉比鮮釀股份有限公司</h3></div>
    </div>
</main> 
@endsection