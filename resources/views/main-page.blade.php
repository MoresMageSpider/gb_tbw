@extends('layouts.app')

@section('content')
<main role="main" class="item item_1 main-page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');" id="main-page">
    <div class="overlay overlay-3 d-none d-lg-block"></div>
    @if(isset($page) && $page->image != '')
    <div class="slider-image overlay-content">
        <div class="container-fluid p-0">
            <div class="col-md-7 ml-auto">
              <div class="thumbnail_inside">
                <div class="overlay overlay-1 d-none d-md-block"></div>
                <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}">  
              </div>
            </div> 
        </div>
    </div>   
    @endif
    <div class="content-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="jumbotron-slider main-page"> 
              <div class="mb-4 mb-sm-5">
                <h2 class="page-title">{{$page->title}}</h2>
              </div>  
              <div class="content mb-4 display-1">
                <?php echo nl2br($page->content);?>
              </div>
              <div class="jumbotron-bottom">
                @if (Route::has($page->more_route))
                    <div class="button mb-5"> 
                      <a class="btn btn-outline-white btn-round" href="{{ route($page->more_route) }}">{{ __('web.button_more') }}</a>
                    </div>
                @endif
                <div class="mast-action d-sm-flex justify-content-between">
                  <div class="title align-self-end">
                    <p class="f-light-14 m-0">Gordon Biersch<br> BEER！</p>
                  </div>
                  <div class="nav-slider d-flex justify-content-between mx-auto">
                    @if (Route::has($page->previous_route))
                      <a href="{{ route($page->previous_route) }}" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
                    @endif
                    @if (Route::has($page->next_route))
                    <a href="{{ route($page->next_route) }}" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
                    @endif
                  </div>  
                </div>
              </div>
          </div>
        </div> 
      </div>
      <div class="vertical-text"><h3>{{__('吉比鮮釀股份有限公司')}}</h3></div>
    </div>
</main> 
@endsection