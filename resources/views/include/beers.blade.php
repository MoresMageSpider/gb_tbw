<div class="content desc">
  <div class="scrollbar-inner">  
    <div class="description"> 
    @php
      $beers = \App\Beer::whereStatus(1)->orderBy('sort_order', 'ASC')->get(); 
    @endphp
    @if(!empty($beers))
      <div class="container-fluid p-0 card-list beers-list">
        <div class="row">
        	@foreach($beers as $beer)
            <div class="col-12 item">
                <div class="card" data-toggle="modal" data-target="#beerModal" data-name="{{$beer->name}}" data-image="{{asset('storage/app/beers/'.$beer->image)}}">
                    <div class="d-flex justify-content-between mb-3">
                      <div class="beer-title mr-3">{{$beer->name}}</div>
                      <div class="yellow-color text-right font-weight-midium">{{__('點擊查看')}}</div>
                    </div>
                    <div class="card-horizontal">
                    	@if($beer->logo != '')
	                        <div class="img-square-wrapper"> 
	                              <img src="{{asset('storage/app/beers/logo/'.$beer->logo)}}" class="beer-logo" alt="{{$beer->name}}"> 
	                        </div>
                        @endif
                        @if($beer->content != '')
	                        <div class="card-body p-0"> 
	                            <p class="card-text">{{$beer->content}}</p>  
	                        </div>
                        @endif
                    </div> 
                </div>
            </div> 
            @endforeach   
        </div>
      </div>
    @endif
  	</div>
  </div>
</div>