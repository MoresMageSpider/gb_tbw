@extends('layouts.app')

@section('content')
@php
    $page = app('rinvex.pages.page')->where('uri', 'reservations')->first();
@endphp  
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main" id="signin-page">
  <div class="content-center">
    <div class="container overlay-content">
        <div class="row">
            <div class="col-md-12 text-left"> 
                <div class="main-content">   
                    @if($page->parent_page != '')       
                        <h2 class="page-title">{{$page->parent_page}}</h2>
                    @endif 
                    @if ( session('error'))
                      <div class="alert alert-danger">
                        {{ session('error') }}
                      </div>
                    @endif
           @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if (Session::has('flash_message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            @endif
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-5 text-left signin"> 
                <h3 class="subtitle">{{__('會員登入')}}</h3>
                <p>{{__('親愛的訪客 您好，預約導覽請先登入，若您尚未擁有IGB /網路會員的帳號，可填寫右側首次登入資訊，預約成功時，我們會將您加入會員。')}}</p>
                <?php /*<p>{{setting('login_text')}}</p>*/?>
                <div class="form-container signin-form">
                    <form method="post" action="{{route('signin')}}" id="signinForm">
                      {{ csrf_field() }} 
                      <div class="form-group">
                        <label for="username" class="mb-1">*{{__('電子郵件 / 手機號碼')}}</label>
                        <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}" required="">
                        <span class="help-text error" id="username-message"></span>                        
                      </div> 
                      <div class="form-group">
                        <label for="password" class="mb-1">*{{__('密碼')}}</label>
                        <input type="password" class="form-control" id="password" name="password" required="" maxlength="12">
                      </div> 
                      <div class="form-group mt-4 d-flex justify-content-between">
                        <div class="align-self-center">
                          <a href="#" class="f-light-14" data-toggle="modal" data-target="#forgetModal">{{__('忘 記 密 碼')}}</a></div>
                        <div class="align-self-center"><button type="submit" id="login_submit" class="btn btn-outline-white btn-round">{{__('會 員 登 入')}}</button></div>
                      </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7 text-left signup"> 
                <h3 class="subtitle">{{__('首次預約')}}</h3>
                <div class="form-container signup-form">
                    <form method="post" action="{{route('signup')}}" id="signupForm">
                      {{ csrf_field() }} 
                      <div class="row">
                        <div class="col-md-6">
                              <div class="row">
                                  <div class="form-group col-md-6">
                                    <label for="name" class="mb-1">*{{__('姓名')}}</label>
                                    <input type="text" class="form-control" id="name" name="name" maxlength="10" required="" value="{{old('name')}}">
                                  </div> 
                                  <div class="form-group col-md-6 pl-2">
                                    <label for="gender_male">*{{__('性別')}}</label><br>
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" @if(!empty(old('gender')) && old('gender') == 1) checked="checked" @endif name="gender" id="gender_male" value="1">
                                        <label class="form-check-label" for="gender_male">{{__('男')}}</label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender" id="gender_female" value="2" @if(!empty(old('gender')) && old('gender') == 2) checked="checked" @endif>
                                        <label class="form-check-label" for="gender_female">{{__('女')}}</label>
                                      </div>  
                                  </div>
                              </div>
                              <div class="form-group">
                                <label for="signup_password" class="mb-1">*{{__('設定密碼')}}</label>
                                <input type="password" class="form-control" id="signup_password" name="password" required="" minlength="6" maxlength="12" placeholder="{{__('請設定英數混合6-12位數')}}">
                              </div> 
                              <div class="form-group">
                                <label for="password_confirmation" class="mb-1">*{{__('再次確認密碼')}}</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required="" minlength="6" maxlength="12" placeholder="{{__('請設定英數混合6-12位數')}}">
                              </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email" class="mb-1">*{{__('電子郵件')}}</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" required="">
                            </div> 
                            <div class="form-group phone_verification_code">
                                <label for="phone" class="mb-1">*{{__('手機號碼')}}</label>
                                <div class="phone-field">
                                  <span class="contry-code">+886</span>
                                  <div class="input-group">                                    
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}" required="" maxlength="10">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" id="btn_verification_code" type="button">
                                            <span id="verification_code_button">{{__('發送驗證')}}</span>
                                        </button>
                                    </span>
                                  </div>
                                </div>
                              <span class="help-text" id="phone-message"></span>
                            </div> 
                            <div class="form-group">                          
                                <label for="verification_code" class="mb-1">*{{__('手機驗證碼')}}</label>
                              <div class="verification_code_field">
                                    <input type="text" class="form-control" id="verification_code" name="verification_code" required="" maxlength="4">
                                <i class="fa fa-question-circle-o" aria-hidden="true" title="{{__('收不到驗證碼，請洽客服。')}}"></i>  
                              </div>
                            </div> 
                        </div>
                      </div> 
                      <div class="row">                     
                          <div class="form-group text-left mt-3 col-md-6 offset-md-6">
                            <button type="submit" class="btn btn-outline-white btn-round">{{__('立 即 註 冊')}}</button>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   </div>
</div>
</main> 
<!-- Modal -->
<div class="modal dark-modal fade" id="forgetModal" tabindex="-1" role="dialog" aria-labelledby="forgetModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <h5 class="modal-title" id="forgetModalTitle">{{__('請選擇您的會員身分')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-6 border-right d-flex flex-column justify-content-between"> 
              <div>
                <h3>{{__('iGB會員')}}</h3>
                <p>{{__('請前往GB鮮釀餐廳官網，進行忘記密碼流程。若有任何疑問，請洽詢客服。')}}</p>
              </div>
              <div> 
                    <a href="https://www.gordonbiersch.com.tw/zh/igb/account/
" class="btn btn-outline-white btn-round" target="_blank">{{__('前往GB鮮釀餐廳')}}</a> 
              </div> 
          </div>
          <div class="col-md-6">
            <form method="post" action="{{route('forget_post')}}" id="forgetForm">
                      {{ csrf_field() }} 
              <h3>{{__('網路會員(網路會員)')}}</h3>
              <p>{{__('請填寫您註冊時的電子郵件，我們會將新密碼寄送給您。')}}</p>
              <div class="forgetForm form-container">
                <div class="form-group">
                    <label for="forget_email" class="mb-1">*{{__('電子郵件')}}</label>
                    <input type="email" class="form-control" id="forget_email" name="email" value="{{old('email')}}" required="">
                </div> 
                <div class="form-group mb-0 text-center mt-4">
                  <button type="submit" class="btn btn-outline-white btn-round">{{__('確 認 送 出')}}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {
  $("#login_submit").on("click", function(){ 
    var username = $("#username").val().trim();    
    if(username != ''){
      if ( (username+"").match(/^\d+$/)) {
        if((username.length < 10 || username.length > 10)){
          $("#username-message").text("手機格式錯誤， 請重新輸入");
          return false;
        }else{
          $("#username-message").text("");
        }     
      }else{
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(regex.test(username) == false){
          $("#username-message").text("格式錯誤， 請重新輸入");
          return false;
        }        
      }
      $("#username-message").text("");
    } 
  });
  $("#btn_verification_code").on("click", function(){ 
      if($( "#phone" ).val().length < 10 || $( "#phone" ).val().length > 10){
        $("#phone-message").text("手機格式錯誤， 請重新輸入");
        $("#phone-message").addClass('error');
      }else{
        $(this).prop( "disabled", true);
        var lang = $('html').attr('lang');
        var token = $('meta[name="csrf-token"]').attr('content');
        var phone = $("#phone").val(); 
        $.ajax({
          type: "POST", 
          dataType: "json", 
          url: "{{ url('api/otp/send') }}",
          data: { 
            _token: token,
            lang: lang,
            phone: phone
          },
          success: function(response) {
            //$("#btn_verification_code").prop( "disabled", false);
            $("#phone-message").text(response.message);
            if(response.status == 1){
                $("#phone-message").removeClass('error');
                $("#btn_verification_code").prop( "disabled", true);
                var timeLeft = 30;
                var elem = document.getElementById('verification_code_button');
                var timerId = setInterval(function(){ 
                if (timeLeft == -1) {
                        clearTimeout(timerId);
                        doSomething();
                    } else {
                        elem.innerHTML = '{{__("發送驗證")}} ('+timeLeft+')';
                        timeLeft--;
                    }
                }, 1000);
            }else{
              $("#btn_verification_code").prop( "disabled", false);
            }
          }
        });
      }      
  }); 
});
function countdown() {
    if (timeLeft == -1) {
        clearTimeout(timerId);
        doSomething();
    } else {
        elem.innerHTML = '{{__("發送驗證")}} ('+timeLeft+')';
        timeLeft--;
    }
}

function doSomething() {
    document.getElementById('verification_code_button').innerHTML = '{{__("發送驗證")}}';
    $("#btn_verification_code").prop( "disabled", false);
}
$(document).ready(function () {
  //called when key is pressed in textbox
  $("#phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
@endsection