@extends('layouts.app')

@section('content') 
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main merchandise-page">
  <div class="content-center">
  <div class="container overlay-content">
    <div class="row">
      <div class="col-md-6 text-left"> 
        <div class="main-content">   
            @if($page->parent_page != '')       
                <h2 class="page-title">{{$page->parent_page}}</h2>
            @endif
            <h1 class="display-1 sub-title">{{$page->title}}</h1>  
            <div class="content desc">
                          <div class="scrollbar-inner">  
                            <div class="description"> 
                              <div class="container-fluid p-0 product-list">
                                @php
                                  $products = \App\Product::whereStatus(1)->orderBy('sort_order', 'ASC')->get(); 
                                @endphp
                                @if(!empty($products))
                                <div class="row">
                                  @foreach($products as $product)
                                    <div class="col-12 mb-3">
                                        <div class="card">
                                            <div class="card-horizontal">
                                               @if($product->image != '')
                                                <div class="img-square-wrapper">
                                                  <div class="thumbnail_inside_3" data-toggle="modal" data-target="#productModal" data-name="{{$product->title}}" data-image="{{asset('storage/app/products/'.$product->image)}}">
                                                      <img class="slider_Img" src="{{asset('storage/app/products/'.$product->image)}}" alt="{{ $product->title }}">
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="card-body">
                                                    <h4 class="card-title">{{ $product->title }}</h4>
                                                    <p class="card-text">{{$product->content}}</p>
                                                    @if($product->price != '')
                                                      <div class="price-box d-flex">
                                                        <div class="align-self-center">
                                                          <label>{{__('建議售價')}}</label>
                                                        </div>
                                                        <div class="align-self-center">
                                                          <span class="price">＄ {{$product->price}}</span>
                                                        </div>
                                                      </div>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                  @endforeach 
                                </div>
                                @endif                                
                            </div>
                          </div>
                          </div>
                        </div>
        </div>
        <div class="nav-slider d-flex justify-content-between mx-auto">
             @if (Route::has($page->previous_route))
              <a href="{{ route($page->previous_route) }}" role="presentation" class="owl1-prev"><span aria-label="Previous">‹</span></a>
            @endif
            @if (Route::has($page->next_route))
            <a href="{{ route($page->next_route) }}" role="presentation" class="owl1-next"><span aria-label="Next">›</span></a>
            @endif
        </div>
    </div> 
    @if(isset($page) && $page->image != '')
    <div class="col-md-6">
      <div class="thumbnail_inside_2 thumbnail_zoom">
        <div class="overlay overlay-1 d-none d-md-block"></div>
        <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->title}}">  
      </div>
    </div>
    @endif
  </div> 
</div>
</div>
</main> 
<div class="modal dark-modal beer-model fade imageModal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-header pb-0">
        <h5 class="modal-title font-weight-midium" id="productModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
          
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#productModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var name = button.data('name');
    var image = button.data('image');
    var modal = $(this)
    if(image != ''){
        modal.find('.modal-body').html('<img class="beer-img" src="'+image+'" alt="'+name+'">');
    }    
    modal.find('.modal-title').text(name)
    //modal.find('.modal-body input').val(recipient)
  })
</script>
@endsection