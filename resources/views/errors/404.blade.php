@extends('layouts.app')

@section('content')
<?php
$page = app('rinvex.pages.page')->where('uri', 'home')->first();?>
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
    <div class="overlay overlay-3 d-none d-lg-block"></div>
    @if(isset($page) && $page->image != '')
    <div class="slider-image overlay-content">
        <div class="container-fluid p-0">
            <div class="col-md-7 ml-auto">
              <div class="thumbnail_inside">
                <div class="overlay overlay-1 d-none d-md-block"></div>
                <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}">  
              </div>
            </div> 
        </div>
    </div>   
    @endif
    <div class="content-center">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="jumbotron-slider"> 
              <div class="mb-5">
                <h2 class="page-title">404 Page</h2>
              </div>  
              <div class="content mb-4 display-1">
                <?php echo nl2br($page->content);?>
              </div>
              <div class=""> 
                    <div class="button mb-5"> 
                      <a class="btn btn-outline-white btn-round" href="{{ url('/') }}">鮮釀啤酒</a>
                    </div> 
              </div>
          </div>
        </div> 
      </div> 
    </div>
</main> 
@endsection