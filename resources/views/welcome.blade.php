 <style type="text/css">
    body{ min-height: 100% !important; }
    body.h-100 {height: unset !important; }
</style>
<div class="text-center w-100" id="welcome-page"> 
    <div class="cover-container d-flex h-100 w-100 mx-auto flex-column">
        <div class="d-md-flex h-md-100 align-items-center">
          <!-- First Half -->
          <div class="col-md-6 p-0 h-md-100 cover-bg bg-cover-1" @if(setting('entrance_bg_1') != '')style="background-image:url('{{asset('storage/app/settings/'.setting('entrance_bg_1'))}}')" @endif>
              <div class="overlay overlay-2"></div>
              <div class="text-white d-md-flex align-items-center h-100 p-3 pt-sm-5 text-center justify-content-center overlay-content">
                  <div class="logoarea pt-5 pb-5"> 
                    <div class="logo-container">
                      <p>
                        @if(setting('entrance_logo_1') != '')
                          <img src="{{asset('storage/app/settings/'.setting('entrance_logo_1'))}}" alt="{{__('GB鮮釀餐廳')}}">
                        @else
                          <img src="{{ url('public/images/gb_logo_b.png') }}" alt="{{__('GB鮮釀餐廳')}}">
                        @endif
                      </p>
                      <h1 class="mb-0 mt-3 logo-text">{{__('GB鮮釀餐廳')}}</h1> 
                    </div>
                    <div class="pt-4 pt-sm-5">
                      <a class="btn btn-outline-white btn-round" href="http://www.gordonbiersch.com.tw" target="_blank">{{__('進 入 網 站')}}</a> 
                    </div>
                  </div>
              </div>
          </div>
          <!-- Second Half -->
          <div class="col-md-6 p-0 h-md-100 loginarea entrance-col-2 cover-bg bg-cover-2" @if(setting('entrance_bg_2') != '')style="background-image:url('{{asset('storage/app/settings/'.setting('entrance_bg_2'))}}')" @endif>
              <div class="overlay overlay-2"></div>
              <div class="d-md-flex align-items-center h-md-100 p-3 pt-sm-5 justify-content-center overlay-content">
                  <div class="logoarea pt-5 pb-5"> 
                    <div class="logo-container d-md-flex align-items-center">  
                        @if(setting('entrance_logo_2') != '')
                          <img src="{{asset('storage/app/settings/'.setting('entrance_logo_2'))}}" class="brewery_logo" alt="{{__('進 入 網 站')}}">
                        @else
                          <img src="{{ url('public/images/gb_logo_b.png') }}" class="brewery_logo" alt="{{__('進 入 網 站')}}">
                        @endif 
                    </div>
                    <div class="enter-button pt-4 pt-sm-5">
                      <button type="button" class="btn btn-outline-white btn-round btn-access-website" id="btn_access_page" data-hide="welcome-page" data-show="access-page">{{__('進 入 網 站')}}</button> 
                    </div>
                  </div>
              </div>
          </div>          
       </div>
       <footer id="footer" class="mastfoot mt-auto">
        <div class="inner">
          <div class="gb-logo mb-1">
            <img src="{{ url('public/images/gb_logo_s.png') }}" alt="{{ config('app.name', 'Laravel') }}">
          </div>
          <p class="f-10">© <?php echo date('Y');?> {{__('Gordon Biersch Restaurant & Brewery')}}</p>
        </div>
      </footer>
    </div> 
</div>    
<div class="text-center w-100" id="access-page" style="display: none;"> 
    <div class="cover-container d-flex m-h-500 h-100 w-100 mx-auto flex-column cover-bg bg-cover-3" @if(setting('entrance_bg_3') != '')style="background-image:url('{{asset('storage/app/settings/'.setting('entrance_bg_3'))}}')" @endif>
          <div class="overlay overlay-2"></div>
        <div class="d-md-flex h-100 align-items-center"> 
          <!-- Second Half -->
          <div class="col-md-12 p-0 h-md-100 loginarea"> 
              <div class="d-md-flex align-items-center h-md-100 py-5 p-sm-5 justify-content-center overlay-content">
                  <div class="logoarea pt-5 pb-5"> 
                    <div class="d-md-flex align-items-center">  
                        @if(setting('entrance_logo_3') != '')
                          <img src="{{asset('storage/app/settings/'.setting('entrance_logo_3'))}}" alt="{{ config('app.name', 'Laravel') }}" class="brewery_logo">  
                        @else
                          <img src="{{ url('public/images/brewery_logo_b.png') }}" class="brewery_logo" alt="{{ config('app.name', 'Laravel') }}">  
                        @endif                          
                    </div>
                    <div class="enter-button pt-4 pt-sm-5">
                      <button type="button" class="btn btn-outline-white btn-round m-w-136 js-cookie-consent-agree cookie-consent__agree">{{__('我 已 年 滿 18 歲')}}</button> <span class="divider">/</span> <button type="button" class="btn btn-outline-white btn-round btn-access-website m-w-136" id="btn_cover_page" data-hide="access-page" data-show="welcome-page">{{__('我 未 滿 18 歲')}}</button> 
                    </div>
                  </div>
              </div>
          </div>          
        </div>
        <footer id="footer" class="mastfoot mt-auto">
            <div class="inner">
              <div class="gb-logo mb-1">
                <img src="{{ url('public/images/gb_logo_s.png') }}" alt="GB">
              </div>
              <p class="f-10">© <?php echo date('Y');?> {{__('Gordon Biersch Restaurant & Brewery')}}</p>
            </div>
        </footer>
    </div> 
</div>