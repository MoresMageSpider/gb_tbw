<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{$appointment->reservation_number}}</title>
    <style type="text/css">
        @page {
            margin: 0px;
        }
        @font-face {
           font-family: SimHei;
           src: url('{{base_path().'/storage/'}}fonts/simhei.ttf') format('truetype');
        }
        * {
          font-family: SimHei;
        }
        body {
            background-image: url("{{ url('public/images/invoice-pdf.jpg') }}");
            background-size: 100%;
            background-repeat: no-repeat;
            margin: 0px;
            font-size: 14px;
            color: rgba(255,255,255,0.8);
            letter-spacing: .17px;font-weight: 400;
        }
        .color-white{ color: #fff !important; } 

        a {
            color: #fff;
            text-decoration: none;
        }
        hr{border-color:rgba(255,255,255,0.4);}
        table {
            font-size: 14px;
        }
        table td{ padding: 5px; }
        tfoot tr td { 
            font-size: 22px;
        }
        .invoice{ max-width: 460px; margin: 0 auto; }
        .invoice table {
            margin: 15px;
        }
        .font-light{ border-color:rgba(255,255,255,0.8); }
        .font-large{ font-size: 22px; font-weight: 500; }
        .font-medium{ font-size: 14px; font-weight: 500; }
        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
        .per-unit{ min-width: 120px; display: inline-block;}
        .per-unit span{ float: right; }
        .font-large{ font-size: 22px; }
    </style>

</head>
<body style="background-color: red;"> 
<div class="invoice"> 
    <table width="100%">
        <thead>
        <tr align="center">
            <td colspan="2" align="center">
                <img src="{{ url('public/images/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}" class="pdf-logo"/>
            </td>
        </tr>
        </thead>
        <tbody> 
        <tr class="color-white" style="font-size: 22px; font-family: Arial, Helvetica, sans-serif;">
            <td style="padding-bottom: 20px;">{{date('Y/m/d', strtotime($appointment->appointment_date))}}</td>
            <td style="padding-bottom: 20px;" align="right">{{date('A h:i', strtotime($appointment->appointment_time))}}</td>
        </tr>  
        <tr>
            <td>{{__('預約單號')}}</td>
            <td align="right">{{$appointment->reservation_number}}</td>
        </tr> 
        <tr>
            <td>{{__('聯 絡 人')}}</td>
            <td align="right">{{$appointment->name}}</td>
        </tr> 
        <tr>
            <td>{{__('手機號碼')}}</td>
            <td align="right">{{$appointment->phone}}</td>
        </tr> 
        <tr>
            <td>{{__('電子信箱')}}</td>
            <td align="right">{{$appointment->email}}</td>
        </tr> 
        <tr>
            <td>{{__('成人票')}}</td>
            <td align="right">{{$appointment->adult}} 位</td>
        </tr> 
        <tr>
            <td>{{__('身障人士優惠')}}</td>
            <td align="right">{{$appointment->handicap}} 位</td>
        </tr> 
        <tr>
            <td>{{__('未成年票')}}</td>
            <td align="right">{{$appointment->underage}} 位</td>
        </tr>  
        <tr>
            <td>{{__('總計金額')}}</td>
            <td align="right">$ {{number_format($appointment->payment_amount)}}</td>
        </tr>  
        <tr><td colspan="2"><hr></td></tr>        
        <tr style="font-size: 22px;">
            <td>
                {{__('成人票')}}<br>
                NTD {{setting('adult_price')}} / {{__('位')}}
            </td>
            <td align="right">X {{$appointment->adult}}</td>
        </tr> 
        <tr style="font-size: 22px;">
            <td>
                {{__('身障人士優惠')}}<br>
                NTD 0 / {{__('位')}}
            </td>
            <td align="right">X {{$appointment->handicap}}</td>
        </tr> 
        <tr style="font-size: 22px;">
            <td>
                {{__('未成年票')}}<br>
                NTD 0 / {{__('位')}}
            </td>
            <td align="right">X {{$appointment->underage}}</td>
        </tr>  
        </tbody>
    </table>
</div> 
</body>
</html>