<footer id="footer" class="bottomfoot sticky-footer mt-auto text-center">
    <div class="inner">
      <div class="gb-logo mb-1">
        <img src="{{ url('public/images/gb_logo_s.png') }}" alt="{{ config('app.name', 'Laravel') }}">
      </div>
      <p class="f-10">© <?php echo date('Y');?> Gordon Biersch Restaurant & Brewery</p>
    </div>
</footer>