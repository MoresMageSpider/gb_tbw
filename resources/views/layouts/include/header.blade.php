<header id="header" class="header nav-fixed">
  <nav class="navbar navbar-expand-lg navbar-default">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('public/images/logo.png') }}" class="logo" alt="{{ config('app.name', 'Laravel') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <div class="collapse navbar-collapse" id="mainNav"> 
          @php
            $pages = app('rinvex.pages.page')::with(['children' => function ($q) {
                          $q->where('include_in_navigation', 1)->orderBy('sort_order');
                      }])->orderBy('sort_order', 'ASC')
                      ->whereNull('parent_id')
                      ->where('include_in_navigation', 1)->get();  
              $route = Route::currentRouteName();
              $home_page = app('rinvex.pages.page')::with(['children' => function ($q) {
                          $q->where('include_in_navigation', 1)->where('is_active', 1)->orderBy('sort_order');
                      }])->orderBy('sort_order', 'ASC')
                      ->whereNull('parent_id')
                      ->where('id', 14)->first(); 

              $page = app('rinvex.pages.page')->select('parent_id', 'route')->where('route', $route)->first();             
          @endphp    
          <ul class="navbar-nav ml-auto">
              <li class="nav-item dropdown @if($route == 'home' || (isset($page) && $page->parent_page_route == 'page.home')) {{'active'}} @endif">
                <div class="link">
                  <a class="nav-link dropdown-toggle" href="{{ route('home') }}">{{__('鮮釀啤酒')}}</a>
                </div>
                <button type="button" role="button" class="nav-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Toggle</button>
                @if(!empty($home_page->children))
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      @foreach($home_page->children as $home_sub_nav)
                          @if (Route::has($home_sub_nav->route))
                            <a class="dropdown-item @if(isset($page) && ($page->route == $home_sub_nav['route'] || $page->parent_page_route == $home_sub_nav['route'])) {{'active'}} @endif" href="{{ route($home_sub_nav->route) }}">{{$home_sub_nav->title}}</a> 
                          @endif
                      @endforeach
                  </div>
                @endif
              </li>
              @foreach($pages as $nav) 
                @if (Route::has($nav->route))
                  @if(!empty($nav->children))
                    <li class="nav-item dropdown @if(isset($page) && ($page->route == $nav->route || $page->parent_page_route == $nav->route)) {{'active'}} @endif">
                        <div class="link">
                          <a class="nav-link dropdown-toggle" href="{{ route($nav->route) }}">{{$nav->title}}</a>
                        </div>
                        <button type="button" role="button" class="nav-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Toggle</button>                        
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach($nav->children as $sub_nav)
                              @if (Route::has($sub_nav->route))
                                @if($sub_nav->route == 'page.reservations' && !\Auth::check())
                                  <a class="dropdown-item @if(isset($page) && ($page->route == $sub_nav['route'] || $page->parent_page_route == $sub_nav['route'])) {{'active'}} @endif" href="{{ url('login') }}">{{$sub_nav->title}}</a>  
                                @else
                                  <a class="dropdown-item @if(isset($page) && ($page->route == $sub_nav['route'] || $page->parent_page_route == $sub_nav['route'])) {{'active'}} @endif" href="{{ route($sub_nav->route) }}">{{$sub_nav->title}}</a>           
                                @endif              
                              @endif            
                            @endforeach                 
                        </div>
                    </li> 
                  @else
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route($nav->route) }}">{{$nav->title}}</a>
                      </li> 
                  @endif
                @endif
              @endforeach  
              @if (\Auth::check()) 
                <li class="nav-item dropdown d-none">
                  <a class="nav-link dropdown-toggle" href="javascript:void(0);">{{\Auth::user()->name}}</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                          @csrf
                                      </form>                                          
                  </div>
                </li>
              @endif 
              <li class="dropdown lang-dropdown">
                  <a class="nav-link dropdown-toggle" href="javascript:void(0);"><i class="fa fa-globe" aria-hidden="true"></i>
</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item @if(app()->getLocale() == 'zh') active @endif" href="{{ url('locale/zh') }}">繁體中文</a> 
                    <a class="dropdown-item @if(app()->getLocale() == 'en') active @endif" href="{{ url('locale/en') }}">ENGLISH</a> 
                  </div>
              </li>
          </ul>   
      </div>
    </div>
  </nav>
</header>