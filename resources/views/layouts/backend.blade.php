<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '管 理 後 台') }} - 管 理 後 台</title>

    <script
            src="{{ asset('public/js/jquery-3.3.1.min.js') }}"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <!-- Custom styles for this template -->  
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
<script src="{{ asset('public/js/bootstrap.min.js') }}" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="{{ asset('public/js/bootbox.min.js') }}"></script>
        <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
      <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('public/css/tempusdominus-bootstrap-4.min.css') }}" />

    <link href="{{ asset('public/admin/css/offcanvas.css') }}" rel="stylesheet">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- jquery-ui -->
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <link href="{{ asset('public/css/jquery-ui.css') }}" rel="stylesheet"> 

    <!--Full Calender--> 
    <link href="{{ asset('public/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
    <script src="{{ asset('public/fullcalendar/moment.min.js') }}"></script>
    <!--<script src="{{ asset('public/fullcalendar/jquery.min.js') }}"></script>-->
    <script src="{{ asset('public/fullcalendar/fullcalendar.min.js') }}"></script>     
    <script src="{{ asset('public/fullcalendar/core/locales/zh-tw.js') }}"></script>
    <script src="{{ asset('public/fullcalendar/popper.min.js') }}"></script>    
    
    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet"> 
    <link href="{{ asset('public/admin/css/style.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('public/admin/js/admin.js') }}"></script>
    <script src="{{ asset('public/admin/js/jquery-input-char-count.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar bg-dark navbar-expand-md sticky-top fixed-top navbar-dark navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand p-0" href="{{ route('admin') }}">                    
                    <img src="{{ url('storage/images/admin-logo.png') }}" alt="" title="{{ config('app.name', 'Laravel') }}" style="height: 42px;"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar --> 

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown lang-dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-globe" aria-hidden="true"></i>
                                </a>  
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item @if(app()->getLocale() == 'zh') active @endif" href="{{ url('locale/zh') }}">繁體中文</a> 
                                <a class="dropdown-item @if(app()->getLocale() == 'en') active @endif" href="{{ url('locale/en') }}">ENGLISH</a> 
                              </div>
                          </li> 
                        @guest
                            <li><a class="nav-link" href="{{ url('/login') }}">{{__('登入後台')}}</a></li> 
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" style="left: -50px;" aria-labelledby="navbarDropdown">
                                    @if(\Auth::guard('admin')->user()->hasRole('admin'))
                                        <a class="dropdown-item" href="{{ url('/admin/users') }}">
                                            {{__('帳號管理')}}
                                        </a>
                                        <div class="dropdown-divider"></div>
                                    @endif                                        
                                        <a class="dropdown-item" href="{{ url('/admin/change-password') }}">
                                            {{__('變更密碼')}}
                                        </a>
                                        <div class="dropdown-divider"></div> 

                                        <a class="dropdown-item" href="{{ url('/admin/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{__('登出')}}
                                    </a>

                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form> 
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-2 main">
            @if (Session::has('flash_message'))
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_message') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @yield('content')
        </main> 
    </div>
 <!-- Scripts -->
    <!--<script src="{{ asset('public/js/app.js') }}"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.8.1/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            force_br_newlines : true,
  force_p_newlines : false,
  forced_root_block : '',
  selector: '.crud-richtext',
  language_url : '<?php echo asset("public/tinymce_languages/langs/zh_TW.js");?>',
  language: 'zh_TW',
  height: 200,
  menubar: "insert",
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen textcolor',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo code | formatselect | ' +
  ' link bold italic backcolor image media | alignleft aligncenter | table tabledelete ' +
  ' alignright alignjustify | bullist numlist outdent indent |' +
  ' removeformat | fontsizeselect forecolor backcolor | help', 
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tiny.cloud/css/codepen.min.css'
  ], 
  media_live_embeds: true,
  image_title: true,
  automatic_uploads: true,
  images_upload_url: "{{ url('tinymce/upload') }}",
  file_picker_types: 'image',
  relative_urls : false,
  remove_script_host : false,
  convert_urls : true,
  file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
            var file = this.files[0];

            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
                cb(blobInfo.blobUri(), { title: file.name });
            };
        };
        input.click();
  }
});
    </script>
    <script type="text/javascript">
        $(function () {
            // Navigation active
            $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
        });
    </script> 
    @yield('scripts')
</body>
</html>