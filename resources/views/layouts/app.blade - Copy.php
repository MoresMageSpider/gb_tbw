<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('public/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/owlcarousel/assets/owl.theme.default.min.css') }}">
    <script type="text/javascript" src="{{ asset('public/js/jquery.min.js') }}"></script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Placed at the end of the document so the pages load faster -->    
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- scrollbar -->
    <link rel="stylesheet" href="{{ asset('public/css/jquery.scrollbar.css') }}">
    @if(\Route::currentRouteName() == 'page.reservations')
        <!--Full Calender--> 
        <link href="{{ asset('public/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
        <script src="{{ asset('public/fullcalendar/moment.min.js') }}"></script>
        <!--<script src="{{ asset('public/fullcalendar/jquery.min.js') }}"></script>-->
        <script src="{{ asset('public/fullcalendar/fullcalendar.min.js') }}"></script> 
    @endif
    <!-- Custom styles for this template -->
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    @php
        $route = \Route::currentRouteName();
        $cookieConsentConfig = config('cookie-consent');
        $alreadyConsentedWithCookies = \Cookie::has($cookieConsentConfig['cookie_name']);        
    @endphp    
</head>
<body class="h-100 @if($route == 'page.customer-service') {{'contact-page'}} @endif">   
    @if($alreadyConsentedWithCookies != 1)
        @include('welcome') 
    @else
        @include('layouts.include.header') 
        <div class="wrapper">
                @yield('content') 
        </div>
        @include('layouts.include.footer') 
    @endif    
    <!-- scrollbar -->
    <script src="{{ asset('public/js/jquery.scrollbar.js') }}"></script>
    <script src="{{ asset('public/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom.js') }}"></script> 
</body>
</html>