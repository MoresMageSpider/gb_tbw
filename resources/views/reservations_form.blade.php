<div class="step-one-form"> 
<input type="hidden" name="bookable_id" value="{{$period_date->id}}">
<div class="row w-100">
  <div class="col-md-6">
    <h3 class="f-light-14">{{$locale == 'zh' ? '預約資訊' : 'Select date'}}</h3>    
    @if(!empty($period_date->periods))    
    <div class="form-group">
      <label for="period_1" class="mb-3">*{{$locale == 'zh' ? '選擇時段' : 'Select time period
'}}</label>      
      <?php $i = 0;?>
      @foreach($period_date->periods as $period)
        @if($period->status == 1)
        <?php if($period->available > 0 && $period->is_future == true){$i++;}?>
        <div class="form-check @if($period->available <= 0 || $period->is_future == false) {{'disabled'}} @endif">
          <input class="form-check-input check-period-time" type="radio" name="period_id" id="period_{{$i}}" value="{{$period->id}}" data-period="{{$period->period}}" data-time="{{date('A h:i', strtotime($period->period))}}" data-max="{{$period->available}}" @if($period->available <= 0 || $period->is_future == false) {{'disabled'}} @elseif($i == 1) {{'checked'}} @endif>
          <label class="form-check-label" for="period_{{$i}}">
          {{date('A h:i', strtotime($period->period))}} ( {{$locale == 'zh' ? '剩餘' : 'Remaining'}} {{$period->available}} )
          </label>
        </div>
        @endif
      @endforeach  
    </div>
    @endif
  </div>
  <div class="col-md-6">            
      <h3 class="f-light-14">{{$locale == 'zh' ? '報名資訊' : 'Registration Information
'}}</h3>
      <div class="d-flex form-group flex-row adult">
        <div class="align-self-center">
            <div class="d-flex justify-content-between">
              <div class="">
                <label class="m-0">*{{$locale == 'zh' ? '成人票' : 'Adult ticket'}}</label>
              </div>
              <div class="">
                $ {{setting('adult_price')}}/{{__('位')}}
              </div>
            </div>
            <div class="input-group">
              <input type="hidden" name="hidden_price_1" class="hidden_price_1" value="{{setting('adult_price')}}">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="adult" data-id="1">
                      -
                  </button>
              </span>
              <input type="text" name="adult" class="form-control input-number" value="0" min="0" max="100">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="adult" data-id="1">
                      +
                  </button>
              </span>
            </div>
        </div>
        <div class="align-self-center"> 
          <div class="cal-price"> = <div class="price price_1">0</div></div>
          <input type="hidden" name="total_amount_1" id="total_amount_1">
        </div> 
      </div> 
      <div class="d-flex form-group flex-row handicap">
        <div class="align-self-center">
            <div class="d-flex justify-content-between">
              <div class="">
                <label class="m-0">*{{$locale == 'zh' ? '身障人士優惠' : 'Concession for Physically'}}</label>
              </div>
              <div class="">
                {{$locale == 'zh' ? '免費' : 'free'}}
              </div>
            </div>
            <div class="input-group">
              <input type="hidden" name="hidden_price_2" class="hidden_price_2" value="0">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="handicap" data-id="2">
                      -
                  </button>
              </span>
              <input type="text" name="handicap" class="form-control input-number" value="0" min="0" max="100">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="handicap" data-id="2">
                      +
                  </button>
              </span>
            </div>
        </div>
        <div class="align-self-center"> 
          <div class="cal-price"> = <div class="price price_2">0</div></div>
          <input type="hidden" name="total_amount_2" id="total_amount_2">
        </div> 
      </div> 
      <div class="form-group">
        <div class="d-flex flex-row underage">
          <div class="align-self-center">
              <div class="d-flex justify-content-between">
                <div class="">
                  <label class="m-0">*{{$locale == 'zh' ? '未成年票' : 'Handicapped Persons'}}</label>
                </div>
                <div class="">
                  {{$locale == 'zh' ? '免費' : 'free'}}
                </div>
              </div>
              <div class="input-group">
                <input type="hidden" name="hidden_price_3" class="hidden_price_3" value="0">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="underage" data-id="3">
                        -
                    </button>
                </span>
                <input type="text" name="underage" class="form-control input-number" value="0" min="0" max="100">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="underage" data-id="3">
                        +
                    </button>
                </span>
              </div>                  
          </div>
          <div class="align-self-center"> 
            <div class="cal-price"> = <div class="price price_3">0</div></div>
            <input type="hidden" name="total_amount_3" id="total_amount_3">
          </div> 
        </div> 
        <span class="help-text">＊{{$locale == 'zh' ? '依法規定，未滿18歲禁止飲酒。' : 'According to the law, drinking alcohol is prohibited before the age of 18.'}}</span>
      </div>
      <div class="form-group text-left mt-3">
        <button type="button" class="btn btn-outline-white btn-round next-step step-1" data-step="step-2">{{$locale == 'zh' ? '下 一 步' : 'Next'}}</button>
      </div>            
  </div>
</div>
</div>