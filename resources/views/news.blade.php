@extends('layouts.app')

@section('content') 
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('public/images/cms/04-img-1-min.jpg')}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main">
  <div class="content-center">
  <div class="container text-left overlay-content">
    <div class="row">
      <div class="col-md-12 mt-0 mt-lg-5 mb-3">  
        <h2 class="page-title">{{__('最新消息')}}</h2>
      </div>  
    </div> 
    @php
      $news = \App\News::whereStatus(1)->where('published_at', '<=', now())->orderBy('sort_order', 'ASC')->get(); 
    @endphp
    @if(!empty($news))
    <div class="news-list mb-4">
      <div class="scrollbar-inner">  
        <div class="news-container">
          <div class="row">
            @foreach($news as $post)
              <div class="col-6 item col-md-4 mb-4 thumbnail_zoom">
                <a href="#" data-toggle="modal" data-target="#newsModal" data-title="{{$post->title}}" data-date="{{date('Y/m/d', strtotime($post->published_at))}}" data-url="{{ route('get_news', ['id' => $post->id]) }}">
                  <div class="card">
                    @if($post->image != '')
                      <div class="card-image">
                      <img alt="{{$post->title}}" class="card-img-top img-fluid" src="{{asset('storage/app/news/'.$post->image)}}" />
                      </div>
                    @endif
                    <div class="card-block">
                      <h4 class="card-title">{{$post->title}}</h4>
                      <p class="card-text">{{date('Y/m/d', strtotime($post->published_at))}}</p>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach   
          </div>
      </div>
      </div>
    </div>
    @endif
  </div>
</div>
</main>  
<!-- Modal -->
<div class="modal dark-modal news-model fade" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="newsModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body text-left">
        <div class="date"></div>
        <h2 class="news-title"></h2>
        <div class="news-content">
          <div class="scrollbar-inner desc">  
            <div class="desc-inner">
              <div class="description loading"></div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>     

@endsection