<div class="row">
    <div class="form-group required {{ $errors->has('title') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('title', __('標題'), ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control input-char-count', 'maxlength' => 40, 'required' => 'required'] : ['class' => 'form-control input-char-count', 'maxlength' => 40]) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('content', __('內容'), ['class' => 'control-label']) !!}
        {!! Form::textarea('content', null, ('required' == 'required') ? ['class' => 'form-control crud-richtext', 'rows' => 5] : ['class' => 'form-control crud-richtext', 'rows' => 5]) !!}
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('image', __('封面圖片'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($news) && $news->image != '')
                    <a class="zoom" href="{{asset('storage/app/news/'.$news->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/news/'.$news->image)}}" alt="{{ $news->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：1024*768 px 之 png or jpg 圖片，')}}<br>
{{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit')?__('重新上傳圖片') : __('上傳圖片')}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">       
    <div class="form-group required {{ $errors->has('status') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('status', __('發佈狀態'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="status" id="status_1" value="1" @if(!empty(old('status')) && old('status') == 1){{'checked'}}@elseif(isset($news->status) && $news->status == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="status_1">{{__('啟用')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="status" id="status_2" value="0" @if(!empty(old('status')) && old('status') == 0){{'checked'}}@elseif(isset($news->status) && $news->status == 0){{'checked'}}@endif >
              <label class="form-check-label" for="status_2">{{__('停用')}}</label>
            </div>
        </div> 
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
    <div class="row">
        <div class="form-group required {{ $errors->has('is_published') ? 'has-error' : ''}} col-md-6">
            {!! Form::label('is_published', __('發佈時間'), ['class' => 'control-label']) !!}<br>
            <div>
                <div class="form-check form-check-inline"> 
                  <input class="form-check-input is_published" type="radio" name="is_published" id="is_published_1" value="1" @if(!empty(old('is_published')) && old('is_published') == 1){{'checked'}}@elseif(isset($news->is_published) && $news->is_published == 1){{'checked'}}@else{{'checked'}}@endif >
                  <label class="form-check-label" for="is_published_1">{{__('立即發佈')}}</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input is_published" type="radio" name="is_published" id="is_published_2" value="0" @if(!empty(old('is_published')) && old('is_published') == 0){{'checked'}}@elseif(isset($news->is_published) && $news->is_published == 0){{'checked'}}@endif>
                  <label class="form-check-label" for="is_published_2">{{__('排程發佈')}}</label>
                </div>
            </div>     
            {!! $errors->first('is_published', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @php
        $current_time = Carbon\Carbon::now();
        $current_time = ($formMode === 'create')?$current_time->toDateTimeString():null; 
    @endphp
    <div class="row">    
        <div id="published_at" class="form-group required {{($formMode === 'create' || (isset($news->is_published) && $news->is_published == 1)) ? 'd-none' : ''}} {{ $errors->has('published_at') ? 'has-error' : ''}} col-md-3">
            <!--{!! Form::label('published_at', 'Published At', ['class' => 'control-label']) !!}-->
            <div class="input-group date" id="datetimepicker" data-target-input="nearest"> 
                {!! Form::input('text', 'published_at', $current_time, ('' == 'required') ? ['class' => 'form-control datetimepicker', 'required' => 'required', 'data-target' => 'datetimepicker'] : ['class' => 'form-control datetimepicker', 'data-target' => 'datetimepicker', 'required' => 'required']) !!}
                <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
                {!! $errors->first('published_at', '<p class="help-block">:message</p>') !!}
            </div>        
        </div>
    </div>  
<script type="text/javascript">
    $(function () {
        $('#datetimepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss'}); 
        $( ".is_published" ).on( "click", function() {
            var is_published = $("input[name='is_published']:checked").val();
            if(is_published == 1){
                $('#published_at').addClass('d-none');
            }else{
                $('#published_at').removeClass('d-none');
            }
        });
    });
</script>   
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
