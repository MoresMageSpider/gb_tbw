<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <!-- Include whatever JQuery which you are using -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- Other js and css scripts -->
    <style type="text/css">
        body{ color: #5D5D5D;font-family: Microsoft JhengHei;letter-spacing: .17px; }
        .auth-page .card-header{ background-color: #F0F0F0;border-radius: 0 !important; border-color: #F0F0F0;font-size: 16px;font-weight: 500;}
        .auth-page .card{ border-radius: 0 !important; border-color: #F0F0F0; box-shadow:0 0 20px 0 rgba(0,0,0,0.30);}
        /* Checkbox Style */
/* Customize the label (the container) */
.chk_container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer; 
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.chk_container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 22px;
  width: 22px;
  background-color: #fff; border: 1px solid #4A90E2;border-radius: 3px;
}

/* On mouse-over, add a grey background color */
.chk_container:hover input ~ .checkmark {
  background-color: #fff;
}

/* When the checkbox is checked, add a blue background */
.chk_container input:checked ~ .checkmark {
  background-color: #fff;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.chk_container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.chk_container .checkmark:after {
  left: 7px;
    top: 2px;
    width: 7px;
    height: 14px;
  border: solid #2196F3;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
    </style>    
</head>
<body>
    <div id="app">
        <nav class="navbar bg-dark navbar-expand-md sticky-top fixed-top navbar-dark navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand p-0" href="{{ url('/admin') }}">                    
                    <img src="{{ url('storage/images/admin-logo.png') }}" alt="" title="{{ config('app.name', 'Laravel') }}" style="height: 42px;"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>                
            </div>
        </nav>


        <main class="py-4 auth-page">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}"></script>
</body>
</html>
