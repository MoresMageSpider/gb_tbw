@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{ $page->title }}</div>
                    <div class="card-body"> 
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($page, [
                            'method' => 'PATCH',
                            'url' => ['/admin/pages', $page->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}
                        @if($page->id == 4)
                            @include ('admin.pages.forms.page_4', ['formMode' => 'page_edit'])                        
                        @else
                            @include ('admin.pages.form', ['formMode' => 'page_edit'])
                        @endif
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection