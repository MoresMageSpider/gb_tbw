@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">Pages</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/pages/create') }}" class="btn btn-success btn-sm" title="Add New Page">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a> 

                        <!--{!! Form::open(['method' => 'GET', 'url' => '/admin/pages', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}-->

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Title</th><th>Slug</th><th>Order</th><th>Parent Page</th><th>Nav</th><th>Page</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                @foreach($pages as $item)
                                    <tr class="parent_page">
                                        <td>{{ $loop->index+1 }}</td>
                                        <td><a href="{{ url('/admin/pages/' . $item->id) }}" title="{{__('View')}}">{{ $item->title }}</a></td>
                                        <td>{{ $item->slug }}</td>
                                        <td>{{ $item->sort_order }}</td>
                                        <td>{{$item->parent_page}}</td>
                                        <td>{{($item->include_in_navigation == 1)?"Yes":"No"}}</td>
                                        <td>{{ $item->subtitle }}</td>
                                        <td>
                                            <a href="{{ url('/admin/pages/' . $item->id) }}" title="{{__('View')}}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @if(!empty($item->children))
                                        @foreach($item->children as $item)
                                            <tr class="child_page bg-light">
                                                <td></td>
                                                <td><a href="{{ url('/admin/pages/' . $item->id) }}" title="View Page">{{ $item->title }}</a></td>
                                                <td>{{ $item->slug }}</td>
                                                <td>{{ $item->sort_order }}</td>
                                                <td>{{$item->parent_page}}</td>
                                                <td>{{($item->include_in_navigation == 1)?"Yes":"No"}}</td>
                                                <td>{{ $item->subtitle }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/pages/' . $item->id) }}" title="{{__('View')}}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                                    <a href="{{ url('/admin/pages/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'url' => ['/admin/pages', $item->id],
                                                        'style' => 'display:inline'
                                                    ]) !!}
                                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                                'type' => 'submit',
                                                                'class' => 'btn btn-danger btn-sm',
                                                                'title' => __('刪除'),
                                                                'onclick'=>'return confirm("是否確認刪除?")'
                                                        )) !!}
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $pages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
