<div class="row">
    <div class="form-group required {{ $errors->has('title') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('title', __('頁面內容'), ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
@if($formMode === 'create')
<div class="row"> 
    <div class="form-group {{ $errors->has('view') ? 'has-error' : ''}} col-sm-4">
        {!! Form::label('view', __('View'), ['class' => 'control-label']) !!}
        {!! Form::text('view', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('view', '<p class="help-block">:message</p>') !!}
    </div>
</div>    
@endif
<div class="row">
<div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-8">
    {!! Form::label('content', __('頁面內容'), ['class' => 'control-label']) !!}
    {!! Form::textarea('content', null, ['class' => 'form-control crud-richtext']) !!}
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('image', __('頁面圖片'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($page) && $page->image != '')
                    <a class="zoom" href="{{asset('storage/app/pages/'.$page->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{ $page->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：1000*1220 px 之 png or jpg 圖片')}}<br>{{__('檔案需小於 2mb。')}}</p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit' || $formMode === 'page_edit')?__('重新上傳圖片') : __('上傳圖片')}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
@if($formMode === 'create' || $formMode === 'edit')
<div class="row">
    <div class="form-group{{ $errors->has('status') ? 'has-error' : ''}} col-md-3">
            {!! Form::label('parent_id', 'Parent Page', ['class' => 'control-label']) !!}
            <?php
                $page_query = app('rinvex.pages.page')::orderBy('sort_order', 'ASC');
                if(isset($page)){
                    $page_query->where('id', '!=', $page->id);   
                }
                $pages = $page_query->pluck("title","id");  

                $status = array(1 => 'Active', 0 => 'Inactive');
            ?>
            {!! Form::select('parent_id',  $pages, null, ['placeholder' => __('Please select ...'),'class' => 'form-control']) !!}
            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('slug') ? 'has-error' : ''}} col-md-3">
        {!! Form::label('slug', 'Slug', ['class' => 'control-label']) !!}
        {!! Form::text('slug', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}} col-md-3">
        {!! Form::label('sort_order', 'Order', ['class' => 'control-label']) !!}
        {!! Form::number('sort_order', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'min' => 0] : ['class' => 'form-control', 'min' => 0]) !!}
        {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('include_in_navigation') ? 'has-error' : ''}} col-md-3">
            {!! Form::label('include_in_navigation', 'Include in Navigation?', ['class' => 'control-label']) !!}
            <?php
            $include_in_navigation = array(0 => 'No', 1 => 'Yes');
            ?>
            {!! Form::select('include_in_navigation',  $include_in_navigation, null, ['class' => 'form-control']) !!}
            {!! $errors->first('include_in_navigation', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
@endif 
@if($page->parent_id == 14)
<div class="row">       
    <div class="form-group required {{ $errors->has('is_active') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('is_active', __('發佈狀態'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="is_active" id="is_active_1" value="1" @if(!empty(old('is_active')) && old('is_active') == 1){{'checked'}}@elseif(isset($page->is_active) && $page->is_active == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="is_active_1">{{__('啟用')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="is_active" id="is_active_2" value="0" @if(!empty(old('is_active')) && old('is_active') == 0){{'checked'}}@elseif(isset($page->is_active) && $page->is_active == 0){{'checked'}}@endif >
              <label class="form-check-label" for="is_active_2">{{__('停用')}}</label>
            </div>
        </div> 
        {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
@endif
<div class="form-group">
    {!! Form::submit($formMode === 'create'  ? __('新增') : __('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div> 