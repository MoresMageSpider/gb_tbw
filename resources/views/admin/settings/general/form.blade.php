<div class="row">
    <div class="form-group required {{ $errors->has('contact_email') ? 'has-error' : ''}} col-md-12 col-lg-8">
        {!! Form::label('contact_email', __('客服收件者'), ['class' => 'control-label']) !!}
        {!! Form::text('settings[contact_email]', setting('contact_email'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'maxlength' => 100] : ['class' => 'form-control', 'maxlength' => 100, 'required' => 'required']) !!}
        {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
        <small id="contact_email_help" class="form-text text-muted">{{__('為客服專區的客服信_收件者信箱，您可以使用“，”增加多個電子信箱')}}</small>
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('reservation_email') ? 'has-error' : ''}} col-md-12 col-lg-8">
        {!! Form::label('reservation_email', __('預約訂單收件者'), ['class' => 'control-label']) !!}
        {!! Form::text('settings[reservation_email]', setting('reservation_email'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('reservation_email', '<p class="help-block">:message</p>') !!}
        <small id="reservation_email_help" class="form-text text-muted">{{__('為參觀導覽的預約訂單_收件者信箱，您可以使用“，”增加多個電子信箱')}}</small>
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('from_name') ? 'has-error' : ''}} col-md-6 col-lg-4">
        {!! Form::label('from_name', __('寄件者名稱'), ['class' => 'control-label']) !!}
        {!! Form::text('settings[from_name]', setting('from_name'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'maxlength' => 15] : ['class' => 'form-control', 'required' => 'required', 'maxlength' => 15]) !!}
        {!! $errors->first('from_name', '<p class="help-block">:message</p>') !!}
        <small id="from_name_help" class="form-text text-muted">{{__('寄發eamil的寄件者名稱')}}</small>
    </div>
    <div class="form-group required {{ $errors->has('from_email') ? 'has-error' : ''}} col-md-6 col-lg-4">
        {!! Form::label('from_email', __('寄件電子郵件'), ['class' => 'control-label']) !!}
        {!! Form::email('settings[from_email]', setting('from_email'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'maxlength' => 100] : ['class' => 'form-control', 'required' => 'required', 'maxlength' => 100]) !!}
        {!! $errors->first('from_email', '<p class="help-block">:message</p>') !!}
        <small id="from_email_help" class="form-text text-muted">{{__('寄發eamil的寄件者名稱')}}</small>
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('adult_price') ? 'has-error' : ''}} col-md-6 col-lg-4">
        {!! Form::label('adult_price', __('成人票金額'), ['class' => 'control-label']) !!}
        {!! Form::number('settings[adult_price]', setting('adult_price'), ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'min' => 0] : ['class' => 'form-control', 'required' => 'required', 'min' => 0]) !!}
        {!! $errors->first('adult_price', '<p class="help-block">:message</p>') !!}
        <small id="adult_price_help" class="form-text text-muted">{{__('用於參觀導覽門票的單張費用計算')}}</small>
    </div>
</div>
<div class="form-group mt-3">
    {!! Form::submit(__('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
