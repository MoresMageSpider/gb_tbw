<div class="row">
    <div class="form-group required {{ $errors->has('entrance_logo_1') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_logo_1', __('左區logo'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_logo_1') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_logo_1'))}}" target='_blank'><img id="entrance_logo_1" src="{{asset('storage/app/settings/'.setting('entrance_logo_1'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_logo_1" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_1").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_logo_1]" id="image_1" onchange="loadPreview(this, '#entrance_logo_1');" class="form-control d-none" >
            </div>
        </div>
	</div>
	<div class="form-group required {{ $errors->has('entrance_bg_1') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_bg_1', __('左區背景圖'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_bg_1') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_bg_1'))}}" target='_blank'><img id="entrance_bg_1" src="{{asset('storage/app/settings/'.setting('entrance_bg_1'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_bg_1" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_2").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_bg_1]" id="image_2" onchange="loadPreview(this, '#entrance_bg_1');" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
<div class="row">
    <div class="form-group required {{ $errors->has('entrance_logo_2') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_logo_2', __('右區logo'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_logo_2') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_logo_2'))}}" target='_blank'><img id="entrance_logo_2" src="{{asset('storage/app/settings/'.setting('entrance_logo_2'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_logo_2" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_3").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_logo_2]" id="image_3" onchange="loadPreview(this, '#entrance_logo_2');" class="form-control d-none" >
            </div>
        </div>
	</div>
	<div class="form-group required {{ $errors->has('entrance_bg_2') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_bg_2', __('右區背景圖'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_bg_2') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_bg_2'))}}" target='_blank'><img id="entrance_bg_2" src="{{asset('storage/app/settings/'.setting('entrance_bg_2'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_bg_2" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_4").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_bg_2]" id="image_4" onchange="loadPreview(this, '#entrance_bg_2');" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
<div class="row">
    <div class="form-group required {{ $errors->has('entrance_logo_3') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_logo_3', __('年齡驗證頁logo'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_logo_3') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_logo_3'))}}" target='_blank'><img id="entrance_logo_3" src="{{asset('storage/app/settings/'.setting('entrance_logo_3'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_logo_3" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_5").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_logo_3]" id="image_5" onchange="loadPreview(this, '#entrance_logo_3');" class="form-control d-none" >
            </div>
        </div>
	</div>
	<div class="form-group required {{ $errors->has('entrance_bg_3') ? 'has-error' : ''}} col-md-4">
        {!! Form::label('entrance_bg_3', __('年齡驗證頁背景圖'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(setting('entrance_bg_3') != '')
                    <a class="zoom" href="{{asset('storage/app/settings/'.setting('entrance_bg_3'))}}" target='_blank'><img id="entrance_bg_3" src="{{asset('storage/app/settings/'.setting('entrance_bg_3'))}}" class="preview_image" width="100"></a>
                @else                    
                    <img id="entrance_bg_3" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：png or jpg 圖片')}}<br>
                    {{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image_6").click();'>{{__('重新上傳圖片')}}</button>
                <input type="file" name="images[entrance_bg_3]" id="image_6" onchange="loadPreview(this, '#entrance_bg_3');" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
<div class="form-group">
    {!! Form::submit(__('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
