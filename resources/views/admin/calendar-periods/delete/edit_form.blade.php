<div class="period_form">
<div class="row">
	<div class="{{ $errors->has('max_appointments') ? 'has-error' : ''}} col-md-5">
		<div class="input-group form-group required"> 
		    {!! Form::label('max_appointments', '時段最高預約人數', ['class' => 'control-label w-100']) !!}
		    {!! Form::number('max_appointments', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'min' => 1, 'max' => 99] : ['class' => 'form-control', 'min' => 1, 'max' => 99]) !!}
		    {!! $errors->first('max_appointments', '<p class="help-block">:message</p>') !!}
		</div>
	</div>	
</div>	
<hr>	
<div class="period_fields">
@foreach($period_date->periods as $period)
<div class="row">
	@php 
		$index = $loop->index + 1;
	@endphp
	<div class="form-group required fieldPeriodGroup col-md-5">
		{!! Form::label('period_'.$index, '時段0'.$index, ['class' => 'control-label w-100']) !!}	
		<div class="d-flex flex-row bd-highlight mb-3">
			<div class="input-group align-self-center mr-3">
				<div class="input-group date timepicker" id="datetimepicker{{$index}}" data-target-input="nearest">				    		    
				    {!! Form::input('text', 'period['.$period->id.']', $period->period, ('required' == 'required') ? ['class' => 'form-control period_timepicker', 'data-target' => '#datetimepicker'.$index, 'required' => 'required', 'id' => 'period_'.$index] : ['class' => 'form-control period_timepicker', 'data-target' => '#datetimepicker'.$index, 'required' => 'required', 'id' => 'period_'.$index]) !!}
				    <div class="input-group-append" data-target="#datetimepicker{{$index}}" data-toggle="datetimepicker">
	                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
	                </div>
				</div>
		    </div>
		    <div class="period_status align-self-center">
		    	<div class="form-check">
				    <input type="checkbox" class="form-check-input" data-id="{{$index}}" value="0" name="status[{{$period->id}}]" id="status_{{$index}}" @if($period->status == 0)  checked="" @endif>
				    <label class="form-check-label" for="status_{{$index}}">不開放</label>
				</div>
		    </div>
		</div>
	    {!! $errors->first('period_'.$index, '<p class="help-block">:message</p>') !!}	
	</div>
</div>
@endforeach
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
</div>