@extends('layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">管理 {{date('m', strtotime($date))}}月時段</div>
                    <div class="card-body">
                        <a href="{{ route('appointments_calendar') }}" title="返回 管理開放日期"><button class="btn btn-outline-primary btn-md mr-2">返回 管理開放日期</button></a>
                        @if(empty($period_previous_month))
                            <button class="btn btn-outline-primary btn-md mr-2" onclick='bootbox.alert("無上月資料可供複製");' type="button">套用上個月資訊</button>
                        @else
                            {!! Form::open([
                                'method' => 'POST',
                                'url' => ['/admin/periods/month', $period_previous_month->id],
                                'style' => 'display:inline'
                            ]) !!}
                                <input type="hidden" name="date" value="{{$date}}">
                                {!! Form::button('套用上個月資訊', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-outline-primary btn-md',
                                        'title' => '套用上個月資訊',
                                        'onclick'=>'return confirm("確定覆寫?")'
                                )) !!}
                            {!! Form::close() !!}                            
                        @endif
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/periods', 'class' => 'form-horizontal', 'files' => true]) !!}

                        @include ('admin.calendar-periods.form', ['formMode' => 'create'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1, #datetimepicker2, #datetimepicker3, #datetimepicker4, #datetimepicker5, #datetimepicker6').datetimepicker({format: 'LT'}); 
        $('.form-check-input').click(function(){ 
           $("#period_"+$(this).data('id')).prop("readonly", $(this).is(':checked'));
        });
        $('.period_fields input[type=checkbox]').each(function () {  
            $("#period_"+$(this).data('id')).prop("readonly", this.checked);         
        });
    });
</script>  
@endsection