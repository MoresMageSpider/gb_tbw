@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">管理預約資訊</div>
                    <div class="card-body">
                        <a href="{{ route('appointments_calendar') }}" class="btn btn-outline-primary btn-md" title="返回 預約明細">
                            返回 預約明細
                        </a> 
                        <br><br> 
                        {!! Form::open(['url' => '/admin/manage-appointments', 'class' => 'form-horizontal', 'files' => true]) !!}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group required">
                                        {!! Form::label('max_appointments', '時段最高預約人數', ['class' => 'control-label']) !!}
                                        <input type="number" min="0" name="max_appointments" class="form-control max_appointments" id="max_appointments" value="{{setting('max_appointments')}}" required=""/>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    @foreach($manageappointments as $item)
                                    <div class="form-group fieldPeriodGroup">
                                        <input type="hidden" name="ids[]" value="{{ $item->id }}">
                                        <div class="input-group date timepicker" id="datetimepicker{{$loop->iteration}}" data-target-input="nearest"> 
                                            <input type="text" name="periods[]" class="form-control period_timepicker" value="{{ $item->period }}" required="" data-target="#datetimepicker{{$loop->iteration}}"/>
                                            <div class="input-group-append" data-target="#datetimepicker{{$loop->iteration}}" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                            </div>
                                            @if($loop->iteration != 1)
                                                <div class="input-group-addon ml-2"> 
                                                    <a href="{{ url('/admin/manage-appointments/' . $item->id) }}" onclick='return confirm("是否確認刪除??")' class="btn btn-outline-danger btn-sm mt-1"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 刪除時段</a> 
                                                </div>
                                            @endif
                                        </div> 
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="input-group-addon"> 
                                <a href="javascript:void(0)" class="btn btn-outline-primary btn-sm addPeriodMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> ＋ 新增時段</a>  <span class="remaining_group ml-3">還可以新增 <span class="remaining">3</span> 組</span>
                            </div>
                            <div class="form-group mt-4">
                                {!! Form::submit('儲存變更', ['class' => 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                        <!-- copy of input fields group -->
                        <div class="form-group fieldPeriodGroupCopy" style="display: none;">
                            <div class="input-group">
                                <div class="input-group date timepicker" id="datetimepicker2" data-target-input="nearest">
                                    <input type="hidden" name="ids[]" value="0">
                                    <input type="text" name="periods[]" class="form-control period_timepicker" required=""/> 
                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                    <div class="input-group-addon ml-2"> 
                                    <a href="javascript:void(0)" class="btn btn-outline-danger remove btn-sm mt-1"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 刪除項目</a>
                                </div>
                                </div>                                    
                            </div>
                        </div> 

                    </div>
                </div>
            </div>
        </div>
    </div>     
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1, .period_timepicker, #datetimepicker2').datetimepicker({format: 'LT'}); 
    });
</script>        
@endsection