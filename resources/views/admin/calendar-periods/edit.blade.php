@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{$date}}</div>
                    <div class="card-body">
                        <a href="{{ route('appointments_calendar') }}" title="返回 管理開放日期"><button class="btn btn-outline-primary btn-md mr-2">返回 管理開放日期</button></a> 
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($period_date, [
                            'method' => 'PATCH',
                            'url' => ['/admin/periods', $period_date->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.calendar-periods.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1, #datetimepicker2, #datetimepicker3, #datetimepicker4, #datetimepicker5, #datetimepicker6').datetimepicker({format: 'LT'}); 
        $('.period_fields input[type=checkbox]').each(function () {  
            $("#period_"+$(this).data('id')).prop("readonly", this.checked);         
        });
        $('.form-check-input').click(function(){ 
           $("#period_"+$(this).data('id')).prop("readonly", $(this).is(':checked'));
        });
    });
</script>      
@endsection