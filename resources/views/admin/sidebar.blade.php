<nav class="col-md-2 d-none d-md-block sidebar pt-4">
    <div class="sidebar-sticky pt-5 @if(\Auth::guard('admin')->user()->hasRole('guide')) {{'nav-guide'}} @elseif(\Auth::guard('admin')->user()->hasRole('customer_service')) {{'nav-service'}} @else {{'nav-admin'}} @endif">
    @php 
        if(\Auth::guard('admin')->user()->hasRole('guide')){
          $menus = $laravelGuideMenus->menus;
        }elseif(\Auth::guard('admin')->user()->hasRole('customer_service')){
          $menus = $laravelServiceMenus->menus;
        }elseif(\Auth::guard('admin')->user()->hasRole('admin')){
          $menus = $laravelAdminMenus->menus;
        }
    @endphp 
    @foreach($menus as $section)
        @if($section->items) 
            <div class="card-header nav-{{$loop->index+1}}">
                    {{ __($section->section) }}
            </div>
            <div class="card-body">
                <ul class="nav navbar-nav flex-column">
                    @foreach($section->items as $menu)
                        @php
                            $nav_active = '';
                            if(isset($menu->type)){
                                if($menu->type == 'page' && isset($page) && $page->id == $menu->slug){
                                    $nav_active = 'active';
                                } 
                            }                            
                        @endphp
                        <li class="nav-item  {{$nav_active}}" role="presentation">                            
                            <a class="nav-link" href="{{ url($menu->url) }}">
                                {{ __($menu->title) }}
                            </a>
                        </li>
                    @endforeach
                </ul> 
            </div>
        @endif
    @endforeach
    </div>
</nav>
