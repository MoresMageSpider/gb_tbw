@extends('layouts.backend')
@section('content')
<script src="{{ asset('public/fullcalendar/bootstrap.min.js') }}"></script>
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">管理開放日期</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between"> 
                            <div class="add_button">
                                <!--<a href="{{ url('/admin/manage-appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="管理預約資訊">
                                    管理預約資訊
                                </a>-->
                                <a href="{{ url('admin/appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="匯出" style="min-width: 100px;">
                                    返回 預約明細
                                </a>
                                <a href="{{ url('/admin/manage-appointments') }}" class="btn btn-outline-primary btn-md mr-2">
                                    管理 03月時段
                                </a>
                            </div>                        
                        </div> 
                        <br>
                        <div id='calendar'></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
$('#calendar').fullCalendar({
    header: {
        left: 'prev, title, next',
        center: '',
        right: '<ul class="calender-type"><li>可預約</li></ul>'
      },
   eventSources: [ 
      {
        url: 'http://localhost:1234/GBB/php/get-events.php', 
      }
    ],
    /*eventRender: function(event, eventElement) {         
        //if (event.title == "Demo") {
        var event_class = event.class;
        $('td[data-date = '+event.start.format('YYYY-MM-DD')+']').addClass(event_class); 
        //}
    }, 
    eventRender: function(event, element, view) {
        return $('<div>' + event.title + '</div><div>' + event.title + '</div>');
    },*/ 
    dayRender: function(date, cell) { 
        $('td[data-date = '+date.format('YYYY-MM-DD')+'] .fc-day-number').after('<a href="{{ route('timeslot_edit', [1]) }}" class="edit-event">編輯</a>');
    },
    eventRender: function(eventObj, $el) { 
        $el.popover({
          title: eventObj.title,
          content: eventObj.description,
          trigger: 'hover',
          placement: 'top',
          container: 'body'
        });
      },
    viewRender: function(view) {
        var calender_info = '<ul class="calender_info"><li class="nav-1">開放預約</li><li class="nav-2">已有預約</li><li class="nav-3">不開放</li><li class="nav-4">預約額滿</li></ul>';
        $(".fc-center").html(calender_info);
    }
});
</script>    
@endsection