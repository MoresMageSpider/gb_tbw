<div class="form-group{{ $errors->has('period') ? 'has-error' : ''}}">
    {!! Form::label('period', 'Period', ['class' => 'control-label']) !!}
    {!! Form::input('time', 'period', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('period', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
