<div class="row">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('name', __('姓名'), ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('username') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('username', __('帳號'), ['class' => 'control-label']) !!}
        {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 12]) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('email', __('電子郵件'), ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if($formMode === 'edit')
<div class="row">
    <div class="form-group required col-md-6 col-lg-4">
        {!! Form::button('返回 帳號管理', array(
                    'type' => 'button',
                    'class' => 'btn btn-outline-primary btn-sm',
                    'title' => __('返回 帳號管理'),
                    'onclick'=>"if(confirm('Are you sure?')) { document.getElementById('passwordReset').submit();}"
            )) !!}
    </div>
</div>
@endif
<?php /*@if($formMode === 'create')
<div class="row">
    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('password', __('密碼'), ['class' => 'control-label']) !!}
        @php
            $passwordOptions = ['class' => 'form-control'];
            if ($formMode === 'create') {
                $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
            }
        @endphp
        {!! Form::password('password', $passwordOptions) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('password_confirmation', __('再次輸密碼'), ['class' => 'control-label']) !!}
        @php
            $passwordOptions = ['class' => 'form-control'];
            if ($formMode === 'create') {
                $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
            }
        @endphp
        {!! Form::password('password_confirmation', $passwordOptions) !!}
        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
    </div> 
</div>
@endif */?>
<hr>
<div class="row">       
    <div class="form-group required {{ $errors->has('role') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('role', __('權限'), ['class' => 'control-label']) !!}        
        <div>
            <div class="form-check pl-0 mb-2">  
              <input class="form-check-input" type="radio" name="role" id="role_1" value="admin" @if(!empty(old('role')) && old('role') == 1){{'checked'}}@elseif(isset($user->roles[0]->name) && $user->roles[0]->name == 'admin'){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="role_1">{{__('管理員')}}</label>
            </div>
            <div class="form-check pl-0 mb-2">
              <input class="form-check-input" type="radio" name="role" id="role_2" value="guide" @if(!empty(old('role')) && old('role') == 0){{'checked'}}@elseif(isset($user->roles[0]->name) && $user->roles[0]->name == 'guide'){{'checked'}}@endif >
              <label class="form-check-label" for="role_2">{{__('導覽員 (僅能查看預約明細)
')}}</label>
            </div>
            <div class="form-check pl-0">
              <input class="form-check-input" type="radio" name="role" id="role_3" value="customer_service" @if(!empty(old('role')) && old('role') == 0){{'checked'}}@elseif(isset($user->roles[0]->name) && $user->roles[0]->name == 'customer_service'){{'checked'}}@endif>
              <label class="form-check-label" for="role_3">{{__('客服人員 (可編輯預約明細、客服專區)')}}</label>
            </div>
        </div> 
        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">       
    <div class="form-group required {{ $errors->has('status') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('status', __('狀態'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="status" id="status_1" value="1" @if(!empty(old('status')) && old('status') == 1){{'checked'}}@elseif(isset($user->status) && $user->status == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="status_1">{{__('啟用')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="status" id="status_2" value="0" @if(!empty(old('status')) && old('status') == 0){{'checked'}}@elseif(isset($user->status) && $user->status == 0){{'checked'}}@endif >
              <label class="form-check-label" for="status_2">{{__('停用')}}</label>
            </div>
        </div> 
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group {{ $errors->has('note') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('note', __('備註'), ['class' => 'control-label']) !!}
        {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 4]) !!}
        {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group mt-4">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('新增帳號'), ['class' => 'btn btn-primary']) !!}
</div>
