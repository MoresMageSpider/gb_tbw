@extends('layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')
            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('帳號管理')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                <div class="input-group" style="width: 300px;">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('搜尋姓名、電子郵件')}}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}                               
                            </div>
                            <div class="add_button">
                                <a href="{{ url('/admin/users/create') }}" class="btn btn-outline-primary btn-md" title="{{__('新增後台帳號')}}">
                                    {{__('新增後台帳號')}}
                                </a>
                            </div>
                        </div>   
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th width="100">{{__('姓名')}}</th>
                                        <th width="250">{{__('電子郵件')}}</th>
                                        <th width="80">{{__('帳號')}}</th>
                                        <th width="80">{{__('權限')}}</th>
                                        <th>{{__('備註')}}</th>
                                        <th width="80">{{__('狀態')}}</th>
                                        <th width="120">{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $item) 
                                    <tr> 
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{$item->username}}</td>
                                        <td>
                                            @if(isset($item->roles[0]->name))
                                                @if($item->roles[0]->name == 'admin')
                                                    {{__('管理員')}}
                                                @elseif($item->roles[0]->name == 'guide')
                                                    {{__('導覽員')}}
                                                @elseif($item->roles[0]->name == 'customer_service')
                                                    {{__('客服人員')}}
                                                @endif  
                                            @endif
                                         </td>
                                        <td>{{ ($item->note == '')?'-':$item->note }}</td>
                                        <td>{{ ($item->status == 1)?_('啟用'):_('停用')}}</td>
                                        <td> 
                                            <a href="{{ url('/admin/users/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection