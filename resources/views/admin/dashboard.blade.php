@extends('layouts.backend')

@section('content')    
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar') 
            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('行事曆')}}</div>
                    <div class="card-body dashboard">
                        <div class="row">
                          <div class="col-12">
                              <p class="text-lg">{{__('當月')}} (<?php echo date('m/Y');?>)</p>
                          </div>
                        </div>
                        <div class="row dashboard-cards">
                          <!-- Earnings (Monthly) Card Example -->
                          <a href="{{ url('admin/members') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-primary shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('會員管理')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$curent_month['users']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                          <!-- Earnings (Monthly) Card Example -->
                          <a href="{{ url('admin/users') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-info shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">{{__('帳號管理')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$curent_month['admin_users']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                          <!-- Earnings (Monthly) Card Example -->
                          <a href="{{ url('admin/appointments') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-success shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{__('預約明細')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$curent_month['reservations']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-list-alt fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a> 
                        </div>
                        <div class="row">
                          <div class="col-12">
                              <p class="text-lg">{{__('總計')}}</p>
                          </div>
                        </div>
                        <div class="row dashboard-cards">
                          <!-- Earnings (Monthly) Card Example -->
                          <a href="{{ url('admin/members') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-primary shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{__('會員管理')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_count['users']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                          <!-- Card -->
                          <a href="{{ url('admin/users') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-info shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">{{__('帳號管理')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_count['admin_users']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                          <!-- Earnings (Monthly) Card Example -->
                          <a href="{{ url('admin/appointments') }}" class="col-xl-3 col-md-6 mb-4 item">
                            <div class="card border-left-success shadow h-100 py-2">
                              <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                  <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">{{__('預約明細')}}</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_count['reservations']}}</div>
                                  </div>
                                  <div class="col-auto">
                                    <i class="fa fa-list-alt fa-2x text-gray-300"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
