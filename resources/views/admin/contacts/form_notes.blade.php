<div class="row">       
    <div class="form-group{{ $errors->has('processing_status') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('processing_status', __('發佈狀態'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="processing_status" id="processing_status_0" value="0" @if(!empty(old('processing_status')) && old('processing_status') == 0){{'checked'}}@elseif(isset($contact->processing_status) && $contact->processing_status == 0){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="processing_status_0">{{__('未處理')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="processing_status" id="processing_status_1" value="1" @if(!empty(old('processing_status')) && old('processing_status') == 1){{'checked'}}@elseif(isset($contact->processing_status) && $contact->processing_status == 1){{'checked'}}@endif >
              <label class="form-check-label" for="processing_status_1">{{__('處理中')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="processing_status" id="processing_status_2" value="2" @if(!empty(old('processing_status')) && old('processing_status') == 2){{'checked'}}@elseif(isset($contact->processing_status) && $contact->processing_status == 2){{'checked'}}@endif >
              <label class="form-check-label" for="processing_status_2">{{__('已處理')}}</label>
            </div>
        </div> 
        {!! $errors->first('processing_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('notes') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('notes', __('客服備註'), ['class' => 'control-label mb-1']) !!}
        {!! Form::textarea('notes', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 4] : ['class' => 'form-control', 'rows' => 4]) !!}
        {!! $errors->first('notes', '<p class="help-block">:notes</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::submit(__('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
