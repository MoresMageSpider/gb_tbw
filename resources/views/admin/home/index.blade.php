@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('標題與圖片')}}</div>
                    <div class="card-body">  
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th width="120">{{__('圖片')}}</th><th width="180">{{__('項目')}}</th><th>{{__('標題')}}</th><th>{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                @foreach($pages as $item)
                                    <tr class="parent_page">
                                        <td>
                                             @if($item->image != '')
                                                <a class="zoom" href="{{asset('storage/app/pages/'.$item->image)}}" target='_blank'>
                                                <img src="{{asset('storage/app/pages/'.$item->image)}}" alt="{{ $item->title }}" class="img-shadow" width="105">
                                            </a>
                                            @endif
                                        </td>
                                        <td>{{ $item->title }}</td>
                                        <td><?php echo nl2br($item->content);?></td> 
                                        <td> 
                                            <a href="{{ url('/admin/home/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> 
                                        </td>
                                    </tr> 
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $pages->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
