<div class="form-group{{ $errors->has('chinese') ? 'has-error' : ''}}">
    {!! Form::label('chinese', __('中文'), ['class' => 'control-label']) !!}
    {!! Form::text('chinese', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('chinese', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('english') ? 'has-error' : ''}}">
    {!! Form::label('english', __('英文'), ['class' => 'control-label']) !!}
    {!! Form::text('english', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('english', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('新增') : __('儲存變更'), ['class' => 'btn btn-primary']) !!}
</div>
