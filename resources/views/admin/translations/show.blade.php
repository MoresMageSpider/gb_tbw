@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">Translation {{ $translation->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/translations') }}" title="{{__('返回')}}"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{__('返回')}}</button></a>
                        <a href="{{ url('/admin/translations/' . $translation->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/translations', $translation->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => __('刪除'),
                                    'onclick'=>'return confirm("是否確認刪除?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $translation->id }}</td>
                                    </tr>
                                    <tr><th> Chinese </th><td> {{ $translation->chinese }} </td></tr><tr><th> English </th><td> {{ $translation->english }} </td></tr><tr><th> Type </th><td> {{ $translation->type }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
