@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('後台目錄翻譯')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/translations', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="搜尋" value="{{ request('search') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}                              
                            </div>
                            <div class="add_button">
                                <a href="{{ url('/admin/translations/json/generate') }}" class="btn btn-primary btn-md mr-3" title="{{__('語系切換')}}">
                                    {{__('語系切換')}}
                                </a>
                                <a href="{{ url('/admin/translations/create') }}" class="btn btn-primary btn-md" title="Add New Translation">
                                    <i class="fa fa-plus" aria-hidden="true"></i> {{__('新增')}}
                                </a>
                            </div>
                        </div>                         
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border db">
                                <thead>
                                    <tr>
                                        <th>#</th><th>{{__('中文')}}</th><th>{{__('英文')}}</th><th>{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($translations as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->chinese }}</td><td>{{ $item->english }}</td>
                                        <td> 
                                            <a href="{{ url('/admin/translations/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/translations', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $translations->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
