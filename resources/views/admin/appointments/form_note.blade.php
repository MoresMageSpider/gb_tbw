<div class="form-group {{ $errors->has('service_notes') ? 'has-error' : ''}}">
    {!! Form::label('service_notes', __('客服備註'), ['class' => 'control-label']) !!}
    {!! Form::textarea('service_notes', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 3] : ['class' => 'form-control', 'required' => 'required', 'rows' => 3]) !!}
    {!! $errors->first('service_notes', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
