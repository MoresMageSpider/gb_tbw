@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('內容管理')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <!--<div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/fresh-beers', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>-->
                            <div class="add_button ml-auto">
                                <a href="{{ url('/admin/fresh-beers/create') }}" class="btn btn-primary btn-md" title="{{__('新增 鮮釀啤酒')}}">
                                    {{__('新增')}}
                                </a>
                            </div>                            
                        </div> 
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th>{{__('拖拉排序')}}</th>
                                        <th width="w120">{{__('封面圖片')}}</th>
                                        <th width="40%">{{__('標題')}}</th>
                                        <th class="w120">{{__('發佈狀態')}}</th>
                                        <th>{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="sort_table">
                                @foreach($freshbeers as $item)
                                    <tr class="ui-state-default row1" data-id="{{ $item->id }}">
                                        <td class="handle"><i class="fa fa-sort move"></i></td>
                                        <td>
                                            @if($item->image != '')
                                                <a class="zoom" href="{{asset('storage/app/pages/'.$item->image)}}" target='_blank'>
                                                <img src="{{asset('storage/app/pages/'.$item->image)}}" alt="{{ $item->title }}" class="img-shadow" width="88">
                                            </a>
                                            @endif
                                        </td>
                                        <td>{{ $item->title }}</td> 
                                        <td>{{ ($item->is_active == 1)?__('已發佈'):__('停用') }}</td>
                                        <td> 
                                            <a href="{{ url('/admin/page/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/pages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $freshbeers->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
  $(function () {
    $( "#sort_table" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
          sendOrderToServer();
      }
    });
    function sendOrderToServer() {
      var order = [];
      var token = $('meta[name="csrf-token"]').attr('content');
      $('tr.row1').each(function(index,element) {
        order.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });
      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "{{ route('freshbeers_reposition') }}",
            data: {
          order: order,
          _token: token
        },
        success: function(response) {
            if (response.status == "success") {
              console.log(response);
            } else {
              console.log(response);
            }
        }
      });
    }
  });
</script>      
@endsection