<div class="row">
    <div class="form-group required {{ $errors->has('title') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('title', '標題', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div> 
@if(isset($freshbeer->display_beers) && $freshbeer->display_beers == 1)

@else
    <div class="row">
        <div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-8">
            {!! Form::label('content', '頁面內容', ['class' => 'control-label']) !!}
            {!! Form::textarea('content', null, ('required' == 'required') ? ['class' => 'form-control crud-richtext', 'rows' => 5] : ['class' => 'form-control crud-richtext', 'rows' => 5]) !!}
            {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('image', '頁面圖片', ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($freshbeer) && $freshbeer->image != '')
                    <a class="zoom" href="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" alt="{{ $freshbeer->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>建議尺寸：1000*1220 px 之 png or jpg 圖片<br>
                    檔案需小於 2mb。
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit')?'重新上傳圖片' : '上傳圖片'}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>    
<div class="row">       
    <div class="form-group required {{ $errors->has('status') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('status', '發佈狀態', ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="status" id="status_1" value="1" @if(!empty(old('status')) && old('status') == 1){{'checked'}}@elseif(isset($freshbeer->status) && $freshbeer->status == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="status_1">啟用</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="status" id="status_2" value="0" @if(!empty(old('status')) && old('status') == 0){{'checked'}}@elseif(isset($freshbeer->status) && $freshbeer->status == 0){{'checked'}}@endif >
              <label class="form-check-label" for="status_2">停用</label>
            </div>
        </div> 
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">       
    <div class="form-group{{ $errors->has('display_beers') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('display_beers', '顯示在列表中?', ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="display_beers" id="display_beers_1" value="1" @if(!empty(old('display_beers')) && old('display_beers') == 1){{'checked'}}@elseif(isset($freshbeer->display_beers) && $freshbeer->display_beers == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="display_beers_1">是</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="display_beers" id="display_beers_2" value="0" @if(!empty(old('display_beers')) && old('display_beers') == 0){{'checked'}}@elseif(isset($freshbeer->display_beers) && $freshbeer->display_beers == 0){{'checked'}}@endif >
              <label class="form-check-label" for="display_beers_2">否</label>
            </div>
        </div> 
        {!! $errors->first('display_beers', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? '儲存變更' : '新增', ['class' => 'btn btn-primary']) !!}
</div>
