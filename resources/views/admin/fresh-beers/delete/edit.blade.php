@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex">
                            <div class="mr-auto p-2">{{ $freshbeer->title }}</div>
                            @if(isset($freshbeer->display_beers) && $freshbeer->display_beers == 1)
                                <div class="p-2">
                                    <a href="{{ url('/admin/beers') }}" target="_blank" class="btn btn-primary">啤酒得獎記錄</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($freshbeer, [
                            'method' => 'PATCH',
                            'url' => ['/admin/fresh-beers', $freshbeer->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.fresh-beers.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
