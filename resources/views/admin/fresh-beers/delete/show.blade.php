@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">內容管理 #{{ $freshbeer->title }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/fresh-beers') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> 返回</button></a>
                        <a href="{{ url('/admin/fresh-beers/' . $freshbeer->id . '/edit') }}" title="Edit FreshBeer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/freshbeers', $freshbeer->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete FreshBeer',
                                    'onclick'=>'return confirm("是否確認刪除?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody> 
                                    <tr><th> 頁面內容 </th><td> {{ $freshbeer->title }} </td></tr><tr><th> 頁面內容 </th><td> <?php echo $freshbeer->content;?> </td></tr>
                                    <tr><th> 封面圖片 </th><td> @if($freshbeer->image != '')
                                                <a class="zoom" href="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" target='_blank'>
                                                <img src="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" alt="{{ $freshbeer->title }}" class="img-shadow" width="150">
                                            </a>
                                            @endif
                                    </td></tr>
                                    <tr><th> 發佈狀態 </th><td> {{ ($freshbeer->status == 1)?__("已發佈"):__("停用") }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
