@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('會員管理')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/members', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                                <div class="input-group" style="width: 300px;">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('搜尋姓名、手機號碼、電子郵件')}}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}                               
                            </div> 
                        </div>   
                        <br/> 
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th width="120">{{__('姓名')}}</th>
                                        <th width="80">{{__('性別')}}</th>
                                        <th width="110">{{__('手機號碼')}}</th>
                                        <th>{{__('電子郵件')}}</th>
                                        <th width="90">{{__('狀態')}}</th>
                                        <th width="170">{{__('建立時間')}}</th>
                                        <th width="80">{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $item) 
                                    <tr> 
                                        <td>{{ $item->name }}</td>
                                        <td>{{ ($item->gender == 1)?__('男性'):__('女性')}}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ ($item->status == 1)?__('啟用'):__('停用')}}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td> 
                                            <a href="{{ url('/admin/members/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
