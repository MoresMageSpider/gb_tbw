<div class="row">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('name', __('姓名'), ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">       
    <div class="form-group required {{ $errors->has('gender') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('gender', __('性別'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="gender" id="gender_1" value="1" @if(!empty(old('gender')) && old('gender') == 1){{'checked'}}@elseif(isset($user->gender) && $user->gender == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="gender_1">{{__('男性')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="gender" id="gender_2" value="2" @if(!empty(old('gender')) && old('gender') == 2){{'checked'}}@elseif(isset($user->gender) && $user->gender == 2){{'checked'}}@endif >
              <label class="form-check-label" for="gender_2">{{__('女性')}}</label>
            </div>
        </div> 
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('phone', __('手機號碼'), ['class' => 'control-label']) !!}
        {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 10]) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('email', __('電子郵件'), ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if($formMode === 'create')
<div class="row">
    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('password', __('密碼'), ['class' => 'control-label']) !!}
        @php
            $passwordOptions = ['class' => 'form-control'];
            if ($formMode === 'create') {
                $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
            }
        @endphp
        {!! Form::password('password', $passwordOptions) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}} required col-md-6 col-lg-4">
        {!! Form::label('password_confirmation', __('再次輸密碼'), ['class' => 'control-label']) !!}
        @php
            $passwordOptions = ['class' => 'form-control'];
            if ($formMode === 'create') {
                $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
            }
        @endphp
        {!! Form::password('password_confirmation', $passwordOptions) !!}
        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
    </div> 
</div>
@endif
<div class="row">
    <div class="form-group {{ $errors->has('note') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('note', __('客服備註'), ['class' => 'control-label']) !!}
        {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 4]) !!}
        {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
    </div>
</div>  
<div class="row">       
    <div class="form-group required {{ $errors->has('status') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('status', __('狀態'), ['class' => 'control-label']) !!}
        <div>
            <div class="form-check form-check-inline"> 
              <input class="form-check-input" type="radio" name="status" id="status_1" value="1" @if(!empty(old('status')) && old('status') == 1){{'checked'}}@elseif(isset($user->status) && $user->status == 1){{'checked'}}@else{{'checked'}}@endif >
              <label class="form-check-label" for="status_1">{{__('啟用')}}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="status" id="status_2" value="0" @if(!empty(old('status')) && old('status') == 0){{'checked'}}@elseif(isset($user->status) && $user->status == 0){{'checked'}}@endif >
              <label class="form-check-label" for="status_2">{{__('停用')}}</label>
            </div>
        </div> 
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-8">
        {!! Form::label('created', __('建立時間'), ['class' => 'control-label']) !!}<br>
        {{ $user->created_at }}
    </div>
</div>  
<div class="form-group mt-2">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('新增帳號'), ['class' => 'btn btn-primary']) !!}
</div>
