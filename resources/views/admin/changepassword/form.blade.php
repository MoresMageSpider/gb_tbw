<div class="row">
    <div class="form-group required {{ $errors->has('current-password') ? ' has-error' : ''}} col-md-4">
        {!! Form::label('current-password', __('輸入舊密碼'), ['class' => 'control-label']) !!} 
        {!! Form::password('current-password', ['class' => 'form-control', 'required' => 'required', 'maxlength' => 12]) !!}
        {!! $errors->first('current-password', '<p class="help-block">:message</p>') !!}
    </div> 
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('new-password') ? ' has-error' : ''}} col-md-4">
        {!! Form::label('new-password', __('輸入新密碼'), ['class' => 'control-label']) !!} 
        {!! Form::password('new-password', ['class' => 'form-control', 'required' => 'required', 'maxlength' => 12]) !!}
        {!! $errors->first('new-password', '<p class="help-block">:message</p>') !!}
    </div> 
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('new-password_confirmation') ? ' has-error' : ''}} col-md-4">
        {!! Form::label('new-password_confirmation', __('再次輸入新密碼'), ['class' => 'control-label']) !!} 
        {!! Form::password('new-password_confirmation', ['class' => 'form-control', 'required' => 'required', 'maxlength' => 12]) !!}
        {!! $errors->first('new-password_confirmation', '<p class="help-block">:message</p>') !!}
    </div> 
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('新增'), ['class' => 'btn btn-primary']) !!}
</div> 
