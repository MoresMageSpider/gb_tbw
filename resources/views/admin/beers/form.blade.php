<div class="row">
    <div class="form-group required {{ $errors->has('name') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('name', __('產品名稱'), ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control input-char-count', 'maxlength' => 50, 'required' => 'required'] : ['class' => 'form-control input-char-count', 'maxlength' => 50]) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('content', __('產品說明'), ['class' => 'control-label']) !!}
        {!! Form::textarea('content', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'rows' => 5] : ['class' => 'form-control', 'rows' => 5]) !!}
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>    
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('image', __('產品圖片'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($beer) && $beer->image != '')
                    <a class="zoom" href="{{asset('storage/app/beers/'.$beer->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/beers/'.$beer->image)}}" alt="{{ $beer->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：1180*890 px 之 png or jpg 圖片，')}}<br>{{__('檔案需小於 2mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit')?__('重新上傳圖片') : __('上傳圖片')}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('logo') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('logo', __('產品Logo'), ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="logo align-self-center mr-2">
                @if(isset($beer) && $beer->logo != '')
                    <a class="zoom" href="{{asset('storage/app/beers/logo/'.$beer->logo)}}" target='_blank'><img id="preview_img_2" src="{{asset('storage/app/beers/logo/'.$beer->logo)}}" alt="{{ $beer->name }}" width="80"></a>
                @else                    
                    <img id="preview_img_2" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>{{__('建議尺寸：150*150 px 之 png or jpg 圖片，')}}<br>
{{__('檔案需小於 1mb。')}}
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("logo").click();'>{{($formMode === 'edit')?__('重新上傳圖片') : __('上傳圖片')}}</button>
                <input type="file" name="logo" id="logo" onchange="loadPreview_2(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
    </div>
</div>     
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? __('儲存變更') : __('新增產品'), ['class' => 'btn btn-primary']) !!}
</div>