@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('啤酒得獎記錄')}}</div>
                    <div class="card-body">
                        <div class="float-right mb-3">
                            <a href="{{ url('/admin/beers/create') }}" class="btn btn-primary btn-md" title="{{__('新增商品')}}">
                                {{__('新增商品')}}
                            </a>
                        </div>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th width="90">{{__('拖拉排序')}}</th>
                                        <th>{{__('產品圖片')}}</th>
                                        <th width="25%">{{__('產品名稱')}}</th>
                                        <th width="45%">{{__('產品說明')}}</th>
                                        <th width="130">{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody id="sort_table">
                                @foreach($beers as $item)
                                    <tr class="ui-state-default row1" data-id="{{ $item->id }}">
                                        <td class="handle"><i class="fa fa-sort move"></i></td>
                                        <td>
                                            @if($item->image != '')
                                                <a class="zoom" href="{{asset('storage/app/beers/'.$item->image)}}" target='_blank'>
                                                <img src="{{asset('storage/app/beers/'.$item->image)}}" alt="{{ $item->title }}" class="img-shadow" width="88">
                                            </a>
                                            @endif
                                        </td> 
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->content }}</td> 
                                        <td> 
                                            <a href="{{ url('/admin/beers/' . $item->id . '/edit') }}" title="{{__('編輯')}}"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/beers', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $beers->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
  $(function () {
    $( "#sort_table" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
          sendOrderToServer();
      }
    });
    function sendOrderToServer() {
      var order = [];
      var token = $('meta[name="csrf-token"]').attr('content');
      $('tr.row1').each(function(index,element) {
        order.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });
      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "{{ route('beers_reposition') }}",
            data: {
          order: order,
          _token: token
        },
        success: function(response) {
            if (response.status == "success") {
              console.log(response);
            } else {
              console.log(response);
            }
        }
      });
    }
  });
</script>      
@endsection