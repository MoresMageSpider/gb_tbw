{!! Form::model($period_date, [
    'method' => 'POST',
    'url' => ['/admin/periods', $period_date->id],
    'class' => 'form-horizontal',
    'files' => true
]) !!}
<div class="day_period_form">
<div class="row row-28">    
        @foreach($period_date->periods as $period)
          @php 
            $index = $loop->index + 1; 
            $booked = $period->booked;
            $status_class = '';
            if($booked > 0)
            {
              $status_class = 'disabled';
            }
          @endphp
          <div class="col-md-6 col @if($loop->index % 2 == 0) border-right @endif">
          <div class="form-group required">
              <label for="status_1_{{$index}}" class="control-label w-100 mb-1">時段0{{$index}}</label>
              <div class="d-flex justify-content-between f-12">
                  <div>{{date('A H:i', strtotime($period->period))}}</div>
                  <div>{{$booked}} / 30 人</div>
              </div>
              <div class="d-flex justify-content-between">
                  <div class="form-check form-check-inline {{$status_class}}"> 
                    <input class="form-check-input" type="radio" name="status[{{$period->id}}]" id="status_1_{{$index}}" value="1" @if($period->status == 1) checked="" @endif  {{$status_class}}>
                    <label class="form-check-label" for="status_1_{{$index}}">開放預約</label>
                  </div>
                  <div class="form-check form-check-inline mr-0 {{$status_class}}">
                    <input class="form-check-input" type="radio" name="status[{{$period->id}}]" id="status_2_{{$index}}" value="0" @if($period->status == 0) checked="" @endif  {{$status_class}}>
                    <label class="form-check-label" for="status_2_{{$index}}">關閉</label>
                  </div>
              </div>
          </div>
          </div>
        @endforeach   
</div> 
<div class="form-group mt-3 text-center">
    <button type="submit" name="submit" class="btn btn-primary btn-md" id="save_period">儲存變更</button>
</div>
</div>
{!! Form::close() !!}