@extends('layouts.backend')
@section('content')
<script src="{{ asset('public/fullcalendar/bootstrap.min.js') }}"></script>
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">管理開放日期</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between"> 
                            <div class="add_button">
                                <!--<a href="{{ url('/admin/manage-appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="管理預約資訊">
                                    管理預約資訊
                                </a>-->
                                <a href="{{ url('admin/appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="匯出" style="min-width: 100px;">
                                    返回 預約明細
                                </a>
                                <div class="month_button">
                                    <a href="{{ url('/admin/periods') }}/{{date('Y-m')}}" class="btn btn-outline-primary btn-md mr-2">
                                        <span>管理 {{date('m')}}月時段</span>
                                    </a>
                                </div>                                
                            </div>                        
                        </div> 
                        <br>
                        <div class="w-100 calendar-container">
                            <input type="hidden" value="{{date('Y-m-t')}}" id="current_month_end">
                            <input type="hidden" value="{{date('Y-m-d')}}" id="current_date">
                            <div id='loading' class="cal-loading">讀取中</div>          
                            <div id='calendar'></div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="dayModal" tabindex="-1" role="dialog" aria-labelledby="dayModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dayModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="form_loading">讀取中</div>
        <div id="dayModalForm"> 

        </div>
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
$('#calendar').fullCalendar({
    locale: 'zh-tw',
    header: {
        left: 'prev, title, next',
        center: '',
        right: '<ul class="calender-type"><li>可預約</li></ul>'
      },
   eventSources: [ 
      {
        url: "{{ route('appointments_admin_api') }}", 
        //url: 'http://localhost:1234/GBB/php/get-events.php', 
      }
    ],
    loading: function( isLoading, view ) {
        if(isLoading) {// isLoading gives boolean value
            //show your loader here 
            $("#loading").show();
        } else {
            $("#loading").hide();
            //hide your loader here
        }
    },
    /*eventRender: function(event, eventElement) {         
        //if (event.title == "Demo") {
        var event_class = event.class;
        $('td[data-date = '+event.start.format('YYYY-MM-DD')+']').addClass(event_class); 
        //}
    }, 
    eventRender: function(event, element, view) {
        return $('<div>' + event.title + '</div><div>' + event.title + '</div>');
    },*/ 
    dayRender: function(date, cell) { 
        var date_format = date.format('YYYY-MM-DD');
        var current_date = $('#current_date').val(); 
        if(date_format > current_date){
          $('td[data-date = '+date.format('YYYY-MM-DD')+'] .fc-day-number').after('<button type="button" class="edit-event" data-toggle="modal" data-target="#dayModal" data-date="'+date_format+'">編輯</buttona>');
        }  
    },
    eventRender: function(eventObj, $el) { 
        $el.popover({
          title: eventObj.title,
          content: eventObj.description,
          trigger: 'hover',
          placement: 'top',
          container: 'body'
        });
      },
    viewRender: function(view, element) {
        var c_date = $('#calendar').fullCalendar('getDate');
        var date_format = c_date.format('YYYY-MM-DD');
        var current_month_end = $('#current_month_end').val(); 
        var calender_info = '<ul class="calender_info"><li class="nav-1">開放預約</li><li class="nav-2">已有預約</li><li class="nav-3">不開放</li><li class="nav-4">預約額滿</li></ul>';
            $(".fc-center").html(calender_info);
        if(date_format > current_month_end){
            $('.month_button').show();
            var c_month = c_date.format('MM');         
            var c_year_month = c_date.format('YYYY-MM');         
            $('.month_button span').text("管理 "+c_month+"月時段");
            $('.month_button a').attr("href", "{{ url('/admin/periods') }}/"+c_year_month);            
        }else{
            $('.month_button').hide();
        }        
    }
});
$('#dayModal').on('show.bs.modal', function (event) {
  $('#form_loading').show();
  $('.day_period_form').remove();
  var button = $(event.relatedTarget) // Button that triggered the modal
  var date = button.data('date');
  var modal = $(this)
  modal.find('.modal-title').text(date);  
  var token = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
      type: "POST", 
      dataType: "json", 
      url: "{{ url('/api/periods_form') }}",
      data: { 
        _token: token,
        date: date,
      },
      success: function(response) {
        if(response.success == 1){
          $('#form_loading').hide();
          $('#dayModalForm').html(response.form);      
        }else{
          bootbox.alert(response.message);
        }       
      }
    });
});
$( "#dayModalForm" ).on( "submit", 'form', function( event ) {
    event.preventDefault();    
    $( "#save_period" ).prop( "disabled", true );
    $( "#save_period" ).text( "Saving..." );
    var serialize = $( this ).serialize();
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
      type: "POST", 
      dataType: "json", 
      url: "{{ url('/api/periods/update') }}",
      data: serialize,
      success: function(response) {
        if(response.success == 1){          
          $('.day_period_form').remove();
          $('#dayModal').modal('hide');
          bootbox.alert(response.message);
          $('#calendar').fullCalendar('refetchEvents');
        }        
      }
    });
});
</script>    
@endsection