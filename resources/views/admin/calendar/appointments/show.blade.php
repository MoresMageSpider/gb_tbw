@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('預約明細')}}</div>
                    <div class="card-body"> 
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('預約時間')}}</label>
                            <div class="col-sm-11">{{ date('Y/m/d', strtotime($appointment->appointment_date)) }}  {{ date('h:i a', strtotime($appointment->appointment_time)) }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('預約單號')}}</label>
                            <div class="col-sm-11">{{ $appointment->reservation_number }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('預約人姓名')}}</label>
                            <div class="col-sm-11">{{ $appointment->name }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('手機號碼')}}</label>
                            <div class="col-sm-11">{{ $appointment->phone }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('電子郵件')}}</label>
                            <div class="col-sm-11">{{ $appointment->email }}</div>
                        </div>
                        <hr> 
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('團體名稱')}}</label>
                            <div class="col-sm-11">{{ $appointment->group_name }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('成人票')}}</label>
                            <div class="col-sm-11">{{ $appointment->adult }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('身障優惠')}}</label>
                            <div class="col-sm-11">{{ $appointment->handicap }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('未成年票')}}</label>
                            <div class="col-sm-11">{{ $appointment->underage }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('總人數')}}</label>
                            <div class="col-sm-11">{{ $appointment->total_people }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('預約備註')}}</label>
                            <div class="col-sm-11">{{ ($appointment->appointment_note != '')?$appointment->appointment_note:'-' }}</div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('總人數')}}</label>
                            <div class="col-sm-11">$ {{ number_format($appointment->payment_amount) }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('預約狀態')}}</label>
                            <div class="col-sm-11"> 
                                {{(!is_null($appointment->cancelled_at))?"已取消":"付款完成"}}
                                @if(is_null($appointment->cancelled_at))
                                    {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/reservations', $appointment->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('取消預約', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-outline-primary ml-2 btn-sm',
                                                        'title' => '取消預約',
                                                        'onclick'=>'return confirm("Confirm cancel?")'
                                                )) !!}
                                            {!! Form::close() !!}                                    
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('建立日期')}}</label>
                            <div class="col-sm-11">{{ date('Y/m/d', strtotime($appointment->created_at)) }}</div>
                        </div>
                        @if(!is_null($appointment->cancelled_at))
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">{{__('取消時間')}}</label>
                            <div class="col-sm-11">{{ date('Y/m/d A h:i', strtotime($appointment->cancelled_at)) }}</div>
                        </div>
                        @endif
                        <hr>
                        {!! Form::model($appointment, [
                            'method' => 'PATCH',
                            'url' => ['/admin/appointments', $appointment->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                            @include ('admin.appointments.form_note', ['formMode' => 'edit'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
