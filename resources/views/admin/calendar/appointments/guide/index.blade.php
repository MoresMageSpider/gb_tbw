@extends('layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')
            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('預約明細')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/appointments', 'class' => 'form-inline my-2 my-lg-0', 'role' => 'search'])  !!}
                                <div class="input-group" style="width: 300px;">
                                    <input type="text" class="form-control" name="search" placeholder="{{__('搜尋預約單號、姓名、手機號碼')}}" value="{{ request('search') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="add_button">
                                <!--<a href="{{ url('/admin/manage-appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="管理預約資訊">
                                    管理預約資訊
                                </a>-->
                                <a href="{{ route('appointments_export') }}" class="btn btn-outline-primary btn-md mr-2" title="{{__('匯出')}}" style="min-width: 100px;">
                                    {{__('匯出')}}
                                </a> 
                            </div>                        
                        </div> 
                        <br/> 
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th>{{__('預約單號')}}</th>
                                        <th>{{__('預約日期')}}</th>
                                        <th>{{__('預約時段')}}</th>
                                        <th>{{__('預約人姓名')}}</th>
                                        <th>{{__('手機號碼')}}</th> 
                                        <th>{{__('成人/身障/未成年')}}</th>
                                        <th>{{__('付款金額')}}</th>
                                        <th>{{__('預約狀態')}}</th>
                                        <th>{{__('建立日期')}}</th>
                                        <th>{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($appointments as $item)
                                    <tr>
                                        <td>{{ $item->reservation_number }}</td>
                                        <td>{{ date('Y/m/d', strtotime($item->appointment_date)) }}</td>
                                        <td>{{ date('h:i a', strtotime($item->appointment_time)) }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->phone }}</td> 
                                        <td>{{ $item->adult.' / '.$item->handicap.' / '.$item->underage }}</td>
                                        <td>$ {{ number_format($item->payment_amount) }}</td>
                                        <td> </td>
                                        <td> </td>
                                        <td> 
                                            <a href="{{ url('/admin/appointments/' . $item->id) }}" title="View Appointment"><button class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> 
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/appointments', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $appointments->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection