<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('appointment_date') ? 'has-error' : ''}}">
    {!! Form::label('appointment_date', 'Appointment Date', ['class' => 'control-label']) !!}
    {!! Form::date('appointment_date', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('appointment_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('appointment_time') ? 'has-error' : ''}}">
    {!! Form::label('appointment_time', 'Appointment Time', ['class' => 'control-label']) !!}
    {!! Form::input('time', 'appointment_time', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('appointment_time', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('reservation_number') ? 'has-error' : ''}}">
    {!! Form::label('reservation_number', 'Reservation Number', ['class' => 'control-label']) !!}
    {!! Form::text('reservation_number', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('reservation_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('group_name') ? 'has-error' : ''}}">
    {!! Form::label('group_name', 'Group Name', ['class' => 'control-label']) !!}
    {!! Form::text('group_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('group_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('adult') ? 'has-error' : ''}}">
    {!! Form::label('adult', 'Adult', ['class' => 'control-label']) !!}
    {!! Form::number('adult', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('adult', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('handicap') ? 'has-error' : ''}}">
    {!! Form::label('handicap', 'Handicap', ['class' => 'control-label']) !!}
    {!! Form::number('handicap', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('handicap', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('underage') ? 'has-error' : ''}}">
    {!! Form::label('underage', 'Underage', ['class' => 'control-label']) !!}
    {!! Form::number('underage', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('underage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_people') ? 'has-error' : ''}}">
    {!! Form::label('total_people', 'Total People', ['class' => 'control-label']) !!}
    {!! Form::number('total_people', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_people', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('appointment_note') ? 'has-error' : ''}}">
    {!! Form::label('appointment_note', 'Appointment Note', ['class' => 'control-label']) !!}
    {!! Form::textarea('appointment_note', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('appointment_note', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('payment_amount') ? 'has-error' : ''}}">
    {!! Form::label('payment_amount', 'Payment Amount', ['class' => 'control-label']) !!}
    {!! Form::number('payment_amount', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('payment_amount', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('payment_status') ? 'has-error' : ''}}">
    {!! Form::label('payment_status', 'Payment Status', ['class' => 'control-label']) !!}
    {!! Form::text('payment_status', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('payment_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('service_notes') ? 'has-error' : ''}}">
    {!! Form::label('service_notes', 'Service Notes', ['class' => 'control-label']) !!}
    {!! Form::textarea('service_notes', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('service_notes', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
