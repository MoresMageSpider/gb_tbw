@extends('layouts.backend')
@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')
            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">{{__('預約明細')}}</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/appointments', 'class' => 'form-inline my-2 my-lg-0', 'role' => 'search'])  !!}
                                <div class="filter d-flex flex-row">
                                    <div class="search mr-3">
                                        <div class="input-group" style="width: 200px;">
                                            <input type="text" class="form-control" name="search" placeholder="{{__('搜尋預約單號、姓名、手機號碼')}}" value="{{ request('search') }}"> 
                                        </div>                                    
                                    </div> 
                                    <div class="mr-3" id="appointment_date">
                                        <div class="input-group date" id="datetimepicker" data-target-input="nearest"> 
                                            {!! Form::input('text', 'date', request('date'), ('' == 'required') ? ['class' => 'form-control datetimepicker', 'data-target' => 'datetimepicker', 'placeholder' => '選擇預約日期'] : ['class' => 'form-control datetimepicker', 'data-target' => 'datetimepicker', 'placeholder' => '選擇預約日期']) !!}
                                            <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
                                        </div>  
                                    </div>
                                    <div class="mr-3">
                                        <div class="period_select">
                                            <select id="sorter" name="period" data-role="sorter" class="sorter-options select">
                                                <option value="">選擇時段</option>
                                                <?php
                                                    if(!empty($periods)){
                                                        foreach ($periods as $key => $period) {
                                                            $selected = ''; 
                                                            if(!empty($search_period) && $search_period == $period->period){
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option '.$selected.' value="'.$period->period.'">'.date('h:i A', strtotime($period->period)).'</option>';
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="mr-3">
                                        <button class="btn btn-secondary" type="submit">
                                            {{__('搜尋')}}
                                        </button>          
                                    </div>            
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="add_button">
                                <!--<a href="{{ url('/admin/manage-appointments') }}" class="btn btn-outline-primary btn-md mr-2" title="管理預約資訊">
                                    管理預約資訊
                                </a>-->
                                <a href="{{ route('appointments_export') }}" class="btn btn-outline-primary btn-md mr-2" title="{{__('匯出')}}" style="min-width: 100px;">
                                    {{__('匯出')}}
                                </a>
                                @if(\Auth::guard('admin')->user()->hasRole('admin'))
                                    <a href="{{ route('appointments_calendar') }}" class="btn btn-outline-primary btn-md mr-2" title="{{__('管理開放日期')}}">
                                        {{__('管理開放日期')}}
                                    </a>
                                @endif
                            </div>                        
                        </div> 
                        <br/> 
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th>{{__('預約單號')}}</th>
                                        <th>{{__('預約日期')}}</th>
                                        <th>{{__('預約時段')}}</th>
                                        <th>{{__('預約人姓名')}}</th>
                                        <th>{{__('手機號碼')}}</th> 
                                        <th>{{__('成人/身障/未成年')}}</th>
                                        <th>{{__('付款金額')}}</th>
                                        <th>{{__('預約狀態')}}</th>
                                        <th>{{__('建立日期')}}</th>
                                        <th>{{__('編輯')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($appointments as $item)
                                    <tr>
                                        <td>{{ $item->reservation_number }}</td>
                                        <td>{{ date('Y/m/d', strtotime($item->appointment_date)) }}</td>
                                        <td>{{ date('h:i a', strtotime($item->appointment_time)) }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->phone }}</td> 
                                        <td>{{ $item->adult.' / '.$item->handicap.' / '.$item->underage }}</td>
                                        <td>$ {{ number_format($item->payment_amount) }}</td>
                                        <td>{{(!is_null($item->cancelled_at))?"已取消":"付款完成"}}</td>
                                        <td>{{(!is_null($item->cancelled_at))?date('Y/m/d', strtotime($item->cancelled_at)):date('Y/m/d', strtotime($item->payment_at))}}</td>
                                        <td> 
                                            <a href="{{ url('/admin/appointments/' . $item->id) }}" title="View Appointment"><button class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> 
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/appointments', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => __('刪除'),
                                                        'onclick'=>'return confirm("是否確認刪除?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $appointments->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker').datetimepicker({format: 'YYYY-MM-DD'}); 
    });
    $(function() {
        $('#appointment_date').on('change.datetimepicker', function(e) {
            if (e.date !== null){
            //do stuff                 
                var date = e.date;                
                periods(date);
            }
        });  
    });
    function periods(date){
        var token = $('meta[name="csrf-token"]').attr('content');
        var date = date.format('YYYY-MM-DD'); 
        $.ajax({
            type: "POST", 
            dataType: "json", 
            url: "{{ url('api/periods_date') }}",
            data: { 
              _token: token,
              date: date
            },
            success: function(response) { 
              if(response.status == 1){
                $('.period_select').html(response.result);
              }
            }
        });
    }
</script>      
@endsection