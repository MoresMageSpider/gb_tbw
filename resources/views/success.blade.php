@extends('layouts.app')

@section('content')
<main role="main" class="item item_1 page-header cover-bg">
    <div class="overlay overlay-3 d-none d-lg-block"></div>
    @if(isset($page) && $page->image != '')
    <div class="slider-image overlay-content">
        <div class="container-fluid p-0">
            <div class="col-md-7 ml-auto">
              <div class="thumbnail_inside">
                <div class="overlay overlay-1 d-none d-md-block"></div>
                <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}">  
              </div>
            </div> 
        </div>
    </div>   
    @endif
    <div class="content-center">
      <div class="container container-md">
        <div class="row">
          <div class="col-md-12"> 
              <div class="m2-5">
                <h2 class="page-title">{{$page->title}}</h2>
              </div>  
              <div class="row row-28">
                  <div class="col-md-4 col border-right">
                    <?php /*@if (Session::has('appointment'))
                      @php
                        $appointment = Session::get('appointment');
                      @endphp
                    @else                      
                      <script>window.location = "{{ route('page.reservations') }}";</script>
                      <?php die();?>
                    @endif*/?>
                    <div class="content-success">
                        <p class="f-light-14">{{__('預約詳細資訊，已透過E-mail發送給您，若有需要變更的內容，請洽客服人員。')}}</p>
                        <div class="reservations-datetime">
                          <div class="d-flex justify-content-between">
                            <div class="en-font">{{date('Y/m/d', strtotime($appointment->appointment_date))}}</div>
                            <div class="">
                              @php
                                setlocale(LC_TIME, "zh_CN");
                              @endphp
                              {{date('A h:i', strtotime($appointment->appointment_time))}}</div>
                          </div>
                        </div>
                        <div class="reservations-info">
                            <ul class="list-group">
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('預約單號')}}</label>
                                <span class="value">{{$appointment->reservation_number}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('聯 絡 人')}}</label>
                                <span class="value">{{$appointment->name}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('手機號碼')}}</label>
                                <span class="value">{{$appointment->phone}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('電子信箱')}}</label>
                                <span class="value">{{$appointment->email}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('成人票')}}</label>
                                <span class="value">{{$appointment->adult}} {{__('位')}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('身障人士優惠')}}</label>
                                <span class="value">{{$appointment->handicap}} {{__('位')}}</span>
                              </li>
                              <li class="list-group-item d-flex justify-content-between align-items-center">
                                <label>{{__('未成年票')}}</label>
                                <span class="value">{{$appointment->underage}} {{__('位')}}</span>
                              </li>
                              <li class="list-group-item total-amount d-flex justify-content-between align-items-center">
                                <label>{{__('總計金額')}}</label>
                                <span class="value">$ {{number_format($appointment->payment_amount)}}</span>
                              </li> 
                            </ul>
                        </div>
                    </div>  
                  </div>
                  <div class="col-md-8 col d-flex flex-column justify-content-between">
                    <div class="display-1">
                      <?php echo $page->content;?>
                    </div>
                    <div class="button-set">
                      <a href="{{ url('invoice') }}/{{$appointment->reservation_number}}" type="button" class="btn btn-outline-white btn-round">{{__('下載資訊')}}</a>
                      <a href="{{ url('/') }}" type="button" class="btn btn-outline-white btn-round ml-4">{{__('回 首 頁')}}</a>
                    </div>
                </div>
              </div>
        </div> 
      </div> 
    </div>
  </div>
</main> 
@endsection