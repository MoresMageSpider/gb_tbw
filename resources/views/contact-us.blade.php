@extends('layouts.app')

@section('content') 
@if (Session::has('flash_message')) 
  @php
    $contact = Session::get('contact'); 
  @endphp
<script>
    jQuery(document).ready(function(){
        jQuery("#contactModal").modal('show');
    });
</script>
<!-- Modal -->
@if(!empty($contact))
  <div class="modal dark-modal contact-model fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
      <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-body text-left">
              <p>{{__('已送出您的表單，我們將盡快與您聯繫。')}}</p>
              <div class="contact-form-info">              
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('姓      名')}}</label>
                  <div class="col-sm-10">
                    {{$contact->name}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('手機號碼')}}</label>
                  <div class="col-sm-10">
                    {{$contact->phone}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('電子郵件')}}</label>
                  <div class="col-sm-10">
                    {{$contact->email}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('聯絡方式')}}</label>
                  <div class="col-sm-10">
                    {{$contact->contact_method}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('問題種類')}}</label>
                  <div class="col-sm-10">
                    {{$contact->question_type}}
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 m-0">{{__('問題內容')}}</label>
                  <div class="col-sm-10">
                    {{$contact->message}}
                  </div>
                </div> 
              </div>
              <div class="mt-5 text-center mb-3">
                  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">{{__('關 閉 視 窗')}}</button>
              </div>
          </div>
        </div> 
    </div>
  </div> 
@endif
@endif
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
  <div class="overlay overlay-3 d-none d-lg-block"></div>  
  <div class="main" id="contact-page">
    <div class="content-center">
    <div class="container overlay-content">
      <div class="row">
        <div class="col-md-6 text-left"> 
          <div class="main-content">               
              @if($page->parent_page != '')       
                <h2 class="page-title">{{$page->parent_page}}</h2>
              @endif
              <h1 class="display-1 sub-title">{{$page->title}}</h1>          
              <div class="content desc"> 
                  <div class="description"> 
                    <?php echo $page->content;?>                    
                  </div> 
                  @if ( session('error'))
                    <div class="pb-4">
                      <div class="alert alert-danger">
                        {{ session('error') }}
                      </div>
                    </div>
                 @endif
                 @if ($errors->any())
                   <div class="pb-4">
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>                
                  @endif
                  <div class="form-container">
                    <form method="post" action="{{url('contacts')}}" id="contactForm">
                      {{ csrf_field() }}
                      <div class="row">
                          <div class="form-group col-md-6">
                            <label for="name" class="mb-1">{{__('姓名')}}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" required="">
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-6">
                            <label for="phone" class="mb-1">{{__('手機號碼')}}</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}" maxlength="10" required="">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="email" class="mb-1">{{__('電子郵件')}}</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" required="">
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 mb-2">  
                          <label for="contact_method_1">{{__('希望聯絡方式')}}</label><br>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="contact_method" id="contact_method_1" value="電話聯絡">
                            <label class="form-check-label" for="contact_method_1">{{__('電話聯絡')}}</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="contact_method" id="contact_method_2" value="電子郵件">
                            <label class="form-check-label" for="contact_method_2">{{__('電子郵件')}}</label>
                          </div>  
                          <span class="help-block error_contact_method alert alert-danger" style="display: none;">{{__('請選擇希望聯絡方式')}}</span> 
                        </div>
                      </div>
                      @php
                        $questions = \App\QuestionType::orderBy('id', 'ASC')->get(); 
                      @endphp
                      @if(!empty($questions))
                      <div class="row">
                          <div class="form-group col-md-6">
                            <label for="question_type" class="mb-1">{{__('問題種類')}}</label>
                            <select class="form-control question_type" id="question_type" name="question_type" required="">
                              <option value="">{{__('請選擇問題種類')}}</option>
                              @foreach($questions as $question)
                                <option value="{{$question->title}}">{{$question->title}}</option>
                              @endforeach 
                            </select>
                          </div>
                      </div>
                      @endif    
                      <div class="row">
                          <div class="form-group col-md-12">
                            <label for="message" class="mb-1">{{__('問題內容')}}</label>
                            <textarea class="form-control" id="message" name="message" required="">{{old('message')}}</textarea>
                          </div>
                      </div>
                      <div class="row mt-3">
                        <div class="col-md-12 mb-2 text-center">  
                          <button type="submit" class="btn btn-outline-white btn-round" id="submit">{{__('送出表單')}}</button>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div> 
      </div> 
      @if(isset($page) && $page->image != '')
        <div class="col-md-6">
          <div class="thumbnail_inside_2 thumbnail_zoom">
            <div class="overlay overlay-1 d-none d-md-block"></div>
            <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->title}}">  
          </div>
        </div>
        @endif 
    </div> 
  </div>
  </div>
</main>      
<script type="text/javascript">
$("#submit").click(function() {  
  if($('#name').val() == '' || $('#phone').val() == '' || $('#email').val() == ''){ 
    // 
  }
  else if($('input[name="contact_method"]:checked').length == 0) { 
    $('.error_contact_method').show(); 
    return false;
  }else{
    $('.error_contact_method').hide();
  }
  return true;
});     
</script>           
@endsection