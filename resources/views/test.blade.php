@extends('layouts.app')

@section('content') 
<?php \App::setLocale('zh');?>
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main">
  <div class="content-center">
  <div class="container overlay-content">
    <div class="row">
      <div class="col-md-6 text-left"> 
        <div class="main-content">    
            <h1 class="display-1 sub-title">{{$page->title}}</h1>  
            <div class="content desc">
              <div class="scrollbar-inner">  
                <div class="description"> 
                    <?php echo $page->content;?>
                    <?php if($page->excerpt != ''){ echo $page->excerpt;}?>
              </div>
              </div>
            </div>
        </div> 
    </div> 
    @if(isset($page) && $page->image != '')
    <div class="col-md-6">
      <div class="thumbnail_inside_2 thumbnail_zoom">
        <div class="overlay overlay-1 d-none d-md-block"></div>
        <img class="slider_Img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{$page->title}}">  
      </div>
    </div>
    @endif
  </div> 
</div>
</div>
</div>
</main> 
@endsection