@extends('layouts.app')

@section('content') 
<main role="main" class="item item_1 page-header cover-bg" style="background-image: url('{{asset('storage/app/pages/'.$page->image)}}');">
<div class="overlay overlay-3 d-none d-lg-block"></div>  
<div class="main">
  <div class="content-center">
  <div class="container container-md overlay-content">

    <h2 class="page-title">{{$page->title}}</h2> 
    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    @if (Session::has('flash_message'))
      <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_message') }}
        </div>
    @endif
    <form method="post" action="{{route('reservationsPost')}}" id="reservationsForm">
      {{ csrf_field() }}       
      <div class="row row-28 step-1 form-step">
        <div class="col-md-5 cols text-left border-right calendar-container pb-0"> 
          <div class="calendar-content">                           
              <div class="content">      
                  <div id='loading' class="cal-loading">{{__('讀取中')}}</div>          
                  <div id='calendar'></div> 
              </div>
          </div> 
        </div>  
        <div class="col-md-7 cols">      
          <div class="loading" style="display: none;"></div>
          <div class="reservationsForm" style="display: none;">                      
          </div>
        </div> 
      </div> 
      <div class="row row-28 form-container step-2 form-step" style="display: none;">
          <div class="col-md-5 cols text-left border-right user-info"> 
              <h3 class="f-light-14">&nbsp;</h3>
              <div class="w-190">
                <div class="form-group">
                  <label for="name">*{{__('預約人姓名')}}</label>
                  <input type="text" class="form-control" name="name" id="name" value="@if(!empty(old('name'))){{old('name')}}@else{{\Auth::user()->name}}@endif" required=""> 
                </div>
                <div class="form-group">
                  <label for="phone">*{{__('手機號碼')}}</label>
                  <input type="text" class="form-control" name="phone" id="phone" value="@if(!empty(old('phone'))){{old('phone')}}@else{{\Auth::user()->phone}}@endif" maxlength="10"> 
                </div>
                <div class="form-group">
                  <label for="email">*{{__('電子郵件')}}</label>
                  <input type="email" class="form-control" name="email" id="email" value="@if(!empty(old('email'))){{old('email')}}@else{{\Auth::user()->email}}@endif" required=""> 
                </div>
                <div class="form-group text-right mt-26">
                  <button type="button" class="btn btn-outline-white btn-round previous-step" data-step="step-1">{{__('上 一 步')}}</button>
                </div>
            </div>
          </div>
          <div class="col-md-7 cols"> 
              <div class="row w-100">
                <div class="col-md-6">
                  <h3 class="f-light-14">{{__('報名資訊')}}</h3>
                  <div class="w-190">
                    <div class="form-group">
                      <label for="group_name">{{__('公司 / 團體名稱')}}</label>
                      <input type="text" class="form-control" name="group_name" id="group_name" value="{{old('group_name')}}"> 
                    </div>
                    <div class="form-group">
                      <label for="appointment_note">{{__('備註')}}</label>
                      <textarea class="form-control" name="appointment_note" id="appointment_note" maxlength="500">{{old('appointment_note')}}</textarea>
                      <span class="help-text">＊{{__('車輛數、預計到達時間...等')}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <h3 class="f-light-14">{{__('付款提示')}}</h3>
                  <div class="w-190">
                    <p class="info">{{__('請再次確認您的金額是否正確，點擊下方前往付款後，將導往第三方支付平台，並進行線上信用卡刷卡流程。若有任何付款疑問，請洽詢客服人員 (03)888 8888。')}}</p>
                    <div class="checkout-datetime">
                      <div class="d-flex justify-content-between">
                          <div class="period period_date"></div>
                          <div class="period period_time"></div>
                      </div>
                    </div>
                    <div class="subtotal">
                      <div class="d-flex justify-content-between">
                          <div class="total-amount">{{__('總計金額')}}</div>
                          <div class="total"></div>
                      </div>
                    </div>
                    <div class="form-group text-left submit-btn">
                      <button type="submit" class="btn btn-outline-white btn-round">{{__('前 往 付 款')}}</button>
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </form>
  </div>
</div>
</div>
</main> 
<?php $locale = \App::getLocale();
if($locale == 'zh'){
	$calender_info = '<ul class="calender_info"><li class="nav-1">可預約</li><li class="nav-2">不開放</li><li class="nav-3">額滿</li></ul>';
}else{
	$calender_info = '<ul class="calender_info"><li class="nav-1">Open</li><li class="nav-2">Not open</li><li class="nav-3">Full</li></ul>';
}
?>
<script src="{{ asset('public/js/reservations.js') }}"></script>
<script>
$('#calendar').fullCalendar({
    locale: 'zh-tw',
    header: {
        left: 'prev, title, next',
        center: '',
        right: '<ul class="calender-type"><li>可預約</li></ul>'
      },
   eventSources: [ 
      {
        url: "{{ route('appointments_api') }}", 
        //url: 'http://localhost:1234/GBB/php/get-events.php', 
      }
    ],    
    titleFormat: "Y MMMM",
    loading: function( isLoading, view ) {
        if(isLoading) {// isLoading gives boolean value
            //show your loader here 
            $("#loading").show();
        } else {
            $("#loading").hide();
            //hide your loader here
        }
    }, 
    eventRender: function(event, eventElement) {         
        //if (event.title == "Demo") {
        var event_class = event.class;
        $('td[data-date = '+event.start.format('YYYY-MM-DD')+']').addClass(event_class); 
        //}
    },
    dayClick: function(date, jsEvent, view) {        
        var date_format = date.format();        
        $(".reservationsForm").hide();        
        $(".fc-day-top").removeClass('active');
        $('td[data-date = '+date.format()+']').addClass("active");
        if($('td[data-date = '+date.format()+']').hasClass("available")){          
          $(".loading").show();
          var token = $('meta[name="csrf-token"]').attr('content');
          $.ajax({
            type: "GET", 
            dataType: "json", 
            url: "{{ url('api/appointments/form') }}/"+date_format,
            data: { 
              _token: token,
              locale: '<?php echo \App::getLocale();?>'
            },
            success: function(response) {  
              if(response.success == 1){
                $(".loading").hide();
                $(".reservationsForm").html(response.form);
                $(".period_date").text(response.date);
                $(".period_time").text(response.time);
                $(".reservationsForm").show();
              }else{
                alert(response.message);
              }            
            }
          });        
        }        
        /*if($('td[data-date = '+date.format()+']').hasClass("available")){
          $("#appointment_date").val(date.format());  
          $(".reservationsForm").show();
        }else{
          $(".reservationsForm").hide();
        }*/
      },
    viewRender: function(view) {
        var minDate = new Date();
        if (view.start <= minDate) {
            $('.fc-prev-button').addClass("fc-state-disabled");
        }
        else {
            $('.fc-prev-button').removeClass("fc-state-disabled");
        }
        var calender_info = '<ul class="calender_info"><li class="nav-1">可預約</li><li class="nav-2">不開放</li><li class="nav-3">額滿</li></ul>';
        $(".fc-right").html('<?php echo $calender_info;?>');
      }
});
</script>
@endsection