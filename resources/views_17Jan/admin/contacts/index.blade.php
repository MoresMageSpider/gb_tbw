@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">提問表單</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/contacts', 'class' => 'form-inline my-2 my-lg-0', 'role' => 'search'])  !!}
                                    <div class="input-group" style="width: 300px;">
                                        <input type="text" class="form-control" name="search" placeholder="搜尋姓名、手機號碼、電子郵件" value="{{ request('search') }}">
                                        <span class="input-group-append">
                                            <button class="btn btn-secondary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                    {!! Form::close() !!}                              
                            </div>
                            <div class="add_button">
                                <a href="{{ url('/admin/question-types') }}" class="btn btn-outline-primary btn-md" title="編輯問題類型">編輯問題類型</a> 
                            </div>
                        </div>  
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-border">
                                <thead>
                                    <tr>
                                        <th width="140">提問時間</th><th width="110">姓名</th><th width="110">手機號碼</th><th>電子郵件</th><th>聯絡方式</th><th>問題類型</th><th>處理狀態</th><th>編輯</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($contacts as $item)
                                    <tr>
                                        <td>{{ date('Y/m/d h:i', strtotime($item->created_at)) }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->contact_method }}</td>
                                        <td>{{ $item->question_type }}</td>
                                        <td>
                                            @if($item->processing_status == 1)
                                                {{"處理中"}}
                                            @elseif($item->processing_status == 2)
                                                {{"停用"}}
                                            @else
                                                {{"已處理"}}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/contacts/' . $item->id) }}" title="View Contact"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a> 
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/contacts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Contact',
                                                        'onclick'=>'return confirm("是否確認刪除??")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $contacts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
