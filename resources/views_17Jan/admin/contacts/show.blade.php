@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">提問表單</div>
                    <div class="card-body">   
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">預約時間</label>
                            <div class="col-sm-11">2020/01/10 13:21</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">姓      名</label>
                            <div class="col-sm-11">{{ $contact->name }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">手機號碼</label>
                            <div class="col-sm-11">{{ $contact->phone }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">電子郵件</label>
                            <div class="col-sm-11">{{ $contact->email }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">聯絡方式</label>
                            <div class="col-sm-11">{{ $contact->contact_method }}</div>
                        </div> 
                        <hr>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">問題類型</label>
                            <div class="col-sm-11">{{ $contact->question_type }}</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label mb-0 py-0 font-weight-medium">問題內容</label>
                            <div class="col-sm-11">{{ $contact->message }}</div>
                        </div>
                        <hr>
                        {!! Form::model($contact, [
                            'method' => 'PATCH',
                            'url' => ['/admin/contacts', $contact->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}
                        @include ('admin.contacts.form_notes', ['formMode' => 'edit'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
