@extends('layouts.backend')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('admin.sidebar')

            <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="card">
                    <div class="card-header">消息發佈</div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="search">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/news', 'class' => 'form-inline my-2 my-lg-0', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="搜尋標題" value="{{ request('search') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}                                
                            </div>
                            <div class="add_button">
                                <a href="{{ url('/admin/news/create') }}" class="btn btn-success btn-md" title="新增消息">新增消息</a> 
                            </div>
                        </div>                        
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <table class="table table-border db">
                                <thead>
                                    <tr>
                                        <th>拖拉排序</th><th width="w120">封面圖片</th><th width="36%">標題</th><th>發佈時間</th><th>發佈狀態</th><th>編輯</th>
                                    </tr>
                                </thead>
                                <tbody id="sort_table">
                                @foreach($news as $item)
                                    <tr class="ui-state-default row1" data-id="{{ $item->id }}">
                                        <td class="handle"><i class="fa fa-sort move"></i></td>
                                        <td>
                                            @if($item->image != '')
                                                <a class="zoom" href="{{asset('storage/app/news/'.$item->image)}}" target='_blank'>
                                                <img src="{{asset('storage/app/news/'.$item->image)}}" alt="{{ $item->title }}" class="img-shadow" width="105">
                                            </a>
                                            @endif
                                        </td> 
                                        <td>{{ $item->title }}</td> 
                                        <td>{{ date('Y/m/d h:i', strtotime($item->published_at)) }}</td>
                                        <td>{{ ($item->status == 1)?"已發佈":"停用" }}</td>
                                        <td> 
                                            <a href="{{ url('/admin/news/' . $item->id . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/news', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete News',
                                                        'onclick'=>'return confirm("是否確認刪除??")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $news->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div> 
<script type="text/javascript">
  $(function () {
    $( "#sort_table" ).sortable({
      items: "tr",
      cursor: 'move',
      opacity: 0.6,
      update: function() {
          sendOrderToServer();
      }
    });
    function sendOrderToServer() {
      var order = [];
      var token = $('meta[name="csrf-token"]').attr('content');
      $('tr.row1').each(function(index,element) {
        order.push({
          id: $(this).attr('data-id'),
          position: index+1
        });
      });
      $.ajax({
        type: "POST", 
        dataType: "json", 
        url: "{{ route('news_reposition') }}",
            data: {
          order: order,
          _token: token
        },
        success: function(response) {
            if (response.status == "success") {
              console.log(response);
            } else {
              console.log(response);
            }
        }
      });
    }
  });
</script>
@endsection