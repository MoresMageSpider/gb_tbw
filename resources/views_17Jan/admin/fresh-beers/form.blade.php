@if($formMode === 'create')
    <div class="row">
        <div class="form-group{{ $errors->has('title') ? 'has-error' : ''}} col-md-8">
            {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
            {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
<div class="row">
<div class="form-group{{ $errors->has('content') ? 'has-error' : ''}} col-md-8">
    {!! Form::label('content', '頁面內容', ['class' => 'control-label']) !!}
    {!! Form::textarea('content', null, ('required' == 'required') ? ['class' => 'form-control crud-richtext', 'required' => 'required', 'rows' => 5] : ['class' => 'form-control crud-richtext', 'rows' => 5]) !!}
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('image', '頁面圖片', ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($freshbeer) && $freshbeer->image != '')
                    <a class="zoom" href="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/freshbeers/'.$freshbeer->image)}}" alt="{{ $freshbeer->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>建議尺寸：1000*1220 px 之 png or jpg 圖片<br>
                    檔案需小於 2mb。
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit')?'重新上傳圖片' : '上傳圖片'}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>    
@if($formMode === 'create')
<div class="row">
    <div class="form-group{{ $errors->has('sort_order') ? 'has-error' : ''}} col-md-3">
        {!! Form::label('sort_order', 'Sort Order', ['class' => 'control-label']) !!}
        {!! Form::number('sort_order', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required', 'min' => 0] : ['class' => 'form-control', 'min' => 0]) !!}
        {!! $errors->first('sort_order', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? '儲存變更' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
