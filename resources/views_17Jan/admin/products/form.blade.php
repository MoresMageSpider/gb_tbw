<div class="row">
    <div class="form-group required {{ $errors->has('title') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('title', '商品名稱', ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control input-char-count', 'required' => 'required', 'maxlength' => 50] : ['class' => 'form-control input-char-count', 'maxlength' => 50]) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('content', '商品說明', ['class' => 'control-label']) !!}
        {!! Form::textarea('content', null, ('required' == 'required') ? ['class' => 'form-control input-char-count', 'required' => 'required', 'maxlength' => 155, 'rows' => 5] : ['class' => 'form-control input-char-count', 'maxlength' => 155, 'rows' => 5]) !!}
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-6">
        {!! Form::label('image', '商品圖片', ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($product) && $product->image != '')
                    <a class="zoom" href="{{asset('storage/app/products/'.$product->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/products/'.$product->image)}}" alt="{{ $product->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>建議尺寸：320*320 px 之 png or jpg 圖片，<br>
檔案需小於 1mb。
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit')?'重新上傳圖片' : '上傳圖片'}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>  
<div class="row">
    <div class="form-group required {{ $errors->has('price') ? 'has-error' : ''}} col-md-3">
        {!! Form::label('price', '建議售價', ['class' => 'control-label']) !!}
        <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">$</div>
        </div>
        {!! Form::text('price', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
      </div>
        
        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('status') ? 'has-error' : ''}} col-md-3">
        {!! Form::label('status', '發佈狀態', ['class' => 'control-label']) !!}
        <?php
        $status = array(1 => '啟用', 0 => '停用');
        ?>
        {!! Form::select('status',  $status, null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? '儲存變更' : '新增商品', ['class' => 'btn btn-primary']) !!}
</div> 