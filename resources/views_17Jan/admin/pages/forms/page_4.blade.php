<div class="row">
    <div class="form-group required {{ $errors->has('content') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('content', '一般導覽', ['class' => 'control-label']) !!}
        {!! Form::textarea('content', null, ['class' => 'form-control crud-richtext']) !!}
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('excerpt') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('excerpt', '釀酒師晚餐', ['class' => 'control-label']) !!}
        {!! Form::textarea('excerpt', null, ['class' => 'form-control crud-richtext']) !!}
        {!! $errors->first('excerpt', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}} col-md-8">
        {!! Form::label('image', '頁面圖片', ['class' => 'control-label']) !!} 
        <div class="d-flex flex-row bd-highlight mb-3">
            <div class="image align-self-center mr-2">
                @if(isset($page) && $page->image != '')
                    <a class="zoom" href="{{asset('storage/app/pages/'.$page->image)}}" target='_blank'><img id="preview_img" src="{{asset('storage/app/pages/'.$page->image)}}" alt="{{ $page->name }}" width="80"></a>
                @else                    
                    <img id="preview_img" src="https://www.w3adda.com/wp-content/uploads/2019/09/No_Image-128.png" class="" width="80"/>
                @endif        
            </div>
            <div class="image-file align-self-center">
                <p>建議尺寸：1000*1220 px 之 png or jpg 圖片，<br>
檔案需小於 2mb。
                </p>
                <button type="button" class="btn btn-outline-primary" onclick='document.getElementById("image").click();'>{{($formMode === 'edit' || $formMode === 'page_edit')?'重新上傳圖片' : '上傳圖片'}}</button>
                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control d-none" >
            </div>
        </div>
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>    
<div class="form-group">
    {!! Form::submit($formMode === 'create'  ? 'Create' : '儲存變更', ['class' => 'btn btn-primary']) !!}
</div> 