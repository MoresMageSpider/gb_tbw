@extends('layouts.backend')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('admin.sidebar')

        <div role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="card">
                <div class="card-header">編輯問題類型</div>
                <div class="card-body"> 
                    <a href="{{ url('/admin/contacts') }}" class="btn btn-outline-primary btn-md" title="返回 提問表單">
                            返回 提問表單
                        </a>
                        <br><br> 
                        {!! Form::open(['url' => '/admin/question-types', 'class' => 'form-horizontal', 'files' => true]) !!}
                            <div class="row">
                                <div class="col-md-5">
                                    @foreach($questiontypes as $item)
                                    <div class="form-group fieldGroup">
                                        <div class="input-group"> 
                                            <input type="hidden" name="ids[]" value="{{ $item->id }}">
                                            <input type="text" name="questions[]" class="form-control" value="{{ $item->title }}" maxlength="200" required="" @if($loop->iteration == 1)style="max-width: 338px;"@endif  />
                                            @if($loop->iteration != 1)
                                                <div class="input-group-addon ml-2"> 
                                                    <a href="{{ url('/admin/question-types/' . $item->id) }}" onclick='return confirm("是否確認刪除??")' class="btn btn-outline-danger btn-sm mt-1"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 刪除項目</a> 
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="input-group-addon"> 
                                <a href="javascript:void(0)" class="btn btn-outline-primary btn-sm addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> ＋ 新增項目</a>
                            </div>
                            <div class="form-group mt-4">
                                {!! Form::submit('儲存變更', ['class' => 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                        <!-- copy of input fields group -->
                        <div class="form-group fieldGroupCopy" style="display: none;">
                            <div class="input-group">
                                <input type="hidden" name="ids[]" value="0">
                                <input type="text" name="questions[]" class="form-control" maxlength="200"  required=""/> 
                                <div class="input-group-addon ml-2"> 
                                    <a href="javascript:void(0)" class="btn btn-outline-danger remove btn-sm mt-1"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> 刪除項目</a>
                                </div>
                            </div>
                        </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
