<nav class="col-md-2 d-none d-md-block sidebar pt-4">
    <div class="sidebar-sticky pt-5">
    @foreach($laravelAdminMenus->menus as $section)
        @if($section->items) 
            <div class="card-header">
                    {{ $section->section }}
            </div>
            <div class="card-body">
                <ul class="nav flex-column">
                    @foreach($section->items as $menu)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" href="{{ url($menu->url) }}">
                                {{ $menu->title }}
                            </a>
                        </li>
                    @endforeach
                </ul> 
            </div>
        @endif
    @endforeach
    </div>
</nav>
