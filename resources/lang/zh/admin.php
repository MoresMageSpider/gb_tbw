<?php

return [
	'page_title' => '頁面內容',
    'page_content' => '頁面內容',
    'page_image' => '頁面圖片',
    'page_image_size' => '建議尺寸：1000*1220 px 之 png or jpg 圖片',
    'size_2mb' => '檔案需小於 2mb。',
    'image_size_2' => '建議尺寸：2200*770 px 之 png or jpg 圖片，',
];
