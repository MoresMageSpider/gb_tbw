function loadPreview(input, id) {
    id = id || '#preview_img';
    if (input.files && input.files[0]) {
        var reader = new FileReader(); 
        reader.onload = function (e) {
            $(id).attr('src', e.target.result);
        }; 
        reader.readAsDataURL(input.files[0]);
    }
 }
 function loadPreview_2(input, id) {
    id = id || '#preview_img_2';
    if (input.files && input.files[0]) {
        var reader = new FileReader(); 
        reader.onload = function (e) {
            $(id).attr('src', e.target.result);
        }; 
        reader.readAsDataURL(input.files[0]);
    }
 } 
$(document).ready(function(){
    //group add limit
    var maxGroup = 100;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });


    //group add limit
    var maxPeriodGroup = 6;
    
    //add more fields group
    $(".addPeriodMore").on("click", function(){
        if($('body').find('.fieldPeriodGroup').length < maxPeriodGroup){
            var fieldHTML = '<div class="form-group fieldPeriodGroup">'+$(".fieldPeriodGroupCopy").html()+'</div>';
            $('body').find('.fieldPeriodGroup:last').after(fieldHTML);
            //alert($('body').find('.fieldPeriodGroup').length);
            $('.remaining').text($('body').find('.fieldPeriodGroup').length - maxPeriodGroup);
        }else{
            alert('Maximum '+maxPeriodGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldPeriodGroup").remove();
    });
});