$(function() {
  $( "#reservationsForm" ).on( "click", ".next-step.step-1", function(e) {  
      var total_people = 0;
      var available = $('input[name="period_id"]:checked').data('max');
      $( "#reservationsForm .input-number" ).each(function() {          
          total_people += parseInt($(this).val());          
      });
      if(total_people == 0){
        alert("Please enter atlest one people.");
        return true;
      }else if(total_people > available){
        alert("You can select maximum "+available);
        return true;
      }
      $('.subtotal .total').text($('#total_amount_1').val());
      $('.form-step').hide();
      $('.'+$( this ).data('step')).show();
  });
  $( "#reservationsForm" ).on( "click", ".previous-step", function(e) {  
      $('.form-step').hide();
      $('.'+$( this ).data('step')).show();
  });
  $('body').on('click', 'button.fc-prev-button', function() { 
      $(".step-one-form").remove();
  });
  $('body').on('click', 'button.fc-next-button', function() {
      $(".step-one-form").remove();
  });
  $( "#reservationsForm" ).on( "click", ".check-period-time", function(e) {   
      $(".period_time").text($(this).data('time'));
  });
});
$( "#reservationsForm" ).on( "click", ".btn-number", function(e) { 
    e.preventDefault();     
    var total_people = 0;
    var available = $('input[name="period_id"]:checked').data('max');
    $( "#reservationsForm .input-number" ).each(function() {          
        total_people += parseInt($(this).val());          
    });     
    fieldName = $(this).attr('data-field');
    id = $(this).data('id');
    hidden_price = parseInt($('.hidden_price_'+id).val()); 
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());    
    var input_val = 0;  
    if(total_people >= available && type == 'plus'){
      return true;
    }
    if (!isNaN(currentVal)) {
        if(type == 'minus') {            
            if(currentVal > input.attr('min')) {
                var input_val = currentVal - 1;
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }
        } else if(type == 'plus') {
            if(currentVal < input.attr('max')) {
                var input_val = currentVal + 1;
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }
        }
        var price = input_val * hidden_price; 
        $('.price_'+id).text(price);
        $('#total_amount_'+id).val(price);
    } else {
        input.val(0);
    }
});
$( "#reservationsForm" ).on( "focusin", ".input-number", function(e) {   
   $(this).data('oldValue', $(this).val()); 
});
$( "#reservationsForm" ).on( "change", ".input-number", function(e) {   
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val()); 
    name = $(this).attr('name');    
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    } 
});
$( "#reservationsForm" ).on( "keydown", ".input-number", function(e) {    
  // Allow: backspace, delete, tab, escape, enter and .
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
       // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) || 
       // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
           // let it happen, don't do anything
           return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
  }
});