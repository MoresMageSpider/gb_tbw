$(window).scroll(function(){
    if ($(window).scrollTop() >= 50) {
        $('#header').addClass('nav-up'); 
    }
    else {
        $('#header').removeClass('nav-up'); 
    }
});
const $dropdown = $(".dropdown");
const $dropdownToggle = $(".dropdown-toggle");
const $dropdownMenu = $(".dropdown-menu");
const showClass = "show";

$(window).on("load resize", function() {  
  if (this.matchMedia("(min-width: 992px)").matches) {
    $dropdown.hover(
      function() {
        const $this = $(this);
        $this.addClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "true");
        $this.find($dropdownMenu).addClass(showClass);
      },
      function() {
        const $this = $(this);
        $this.removeClass(showClass);
        $this.find($dropdownToggle).attr("aria-expanded", "false");
        $this.find($dropdownMenu).removeClass(showClass);
      }
    );
  } else {
    $dropdown.off("mouseenter mouseleave");
  }
});

// Access Website Page show hide
$(function() { 
    $(".btn-access-website").on("click", function(){
        $('#'+$(this).data("hide")).hide();
        $('#'+$(this).data("show")).show();
    }); 
    jQuery('.scrollbar-inner').scrollbar();
});
$(document).ready(function(){
  var owl = $('#owlSlider');
  owl.owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      dots:false,
      items:1, 
      slideSpeed: 3000, 
      mouseDrag : false,
      animateIn: 'fadeIn', // add this
      animateOut: 'fadeOut' // and this
  });
  // Custom Button
  $('.owl1-next').click(function() {
    owl.trigger('next.owl.carousel');
  });
  $('.owl1-prev').click(function() {
    owl.trigger('prev.owl.carousel');
  });    
  $('#newsModal').on('show.bs.modal', function (event) {  
    $('.description').html('');
    $('.description').addClass('loading');
    var button = $(event.relatedTarget) // Button that triggered the modal
    var title = button.data('title');
    var date = button.data('date');
    var url = button.data('url');
    var token = $('meta[name="csrf-token"]').attr('content');
    var modal = $(this)
    modal.find('.news-title').text(title)
    modal.find('.date').text(date)
    $.ajax({
      type: "POST", 
      dataType: "json", 
      url: url,
      data: { 
        _token: token
      },
      success: function(response) {
        $('.description').removeClass('loading');
        $('.description').html(response.content)      
      }
    });
  });
}); 